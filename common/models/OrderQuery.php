<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * OrderQuery is ActiveQuery with post scopes.
 *
 * @method Order|array|null one($db = null)
 * @method Order[]|array all($db = null)
 */
class OrderQuery extends ActiveQuery
{
    /**
     * @param string $guid
     * @return OrderQuery
     */
    public function byGuid($guid)
    {
        $this->andWhere(['guid' => $guid]);
        return $this;
    }

    /**
     * @param integer $days
     * @return OrderQuery
     */
    public function createdInLastDay($days)
    {
        $this->andWhere('"created" >= NOW() - interval \'' . (int)$days . ' days\'');
        return $this;
    }
}
