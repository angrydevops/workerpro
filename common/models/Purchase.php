<?php

namespace common\models;

use \yii\validators\RegularExpressionValidator;

/**
 * This is the model class for table "purchases".
 *
 * @property integer $id
 * @property integer|null $order_id
 * @property string|null $hash
 * @property string $guid
 * @property string $organization
 * @property string $username
 * @property string $email
 * @property string $licence_type
 * @property string $subscription_type
 * @property integer $computers_count
 * @property string|null $referal
 * @property double $price
 * @property string $summary
 * @property string|null $wmi_order_id
 * @property string|null $wmi_to_user_id
 *
 * @property Order $order
 */
class Purchase extends \yii\db\ActiveRecord
{
	/**
	 * User friendly label.
	 */
	const LABEL_PLURAL = 'Оплата';

	/**
	 * @var array of licence types
	 */
	public static $licenceTypes = [
		'cloud' => 'Облачная',
		'local' => 'Локальная'
	];

	/**
	 * @var array of subscription types
	 */
	public static $subscriptionTypes = [
		'threeMonths' => 'на 3 месяца',
		'sixMonths' => 'на 6 месяцев',
		'year' => 'на год'
	];

	/**
	 * @var array of attributes from Order
	 */
	public static $attributesFromOrder = [
		'guid',
		'organization',
		'username',
		'email',
		'licence_type',
		'subscription_type',
		'computers_count',
		'referal',
		'price',
	];

	/**
	 * @var array of event types with labels
	 */
	public static $events = [
		'opened' => 'Открыта',
		'confirmation-success' => 'Успешное подтверждение',
		'confirmation-fail' => 'Неуспешное подтверждение',
		'pay-success' => 'Успешная оплата',
		'pay-fail' => 'Неуспешная оплата',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'purchases';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['organization', 'username', 'email', 'referal'], 'filter', 'filter' => 'trim'],
			[['organization', 'username', 'email', 'licence_type', 'computers_count', 'summary'], 'required'],
			[['email'], 'filter', 'filter' => 'mb_strtolower'],
			['email', 'email', 'enableIDN' => true],
			[['licence_type', 'subscription_type', 'referal', 'guid', 'wmi_order_id'], 'string'],
			[['computers_count', 'order_id', 'computers_count'], 'integer'],
			[['price'], 'number'],
			[['organization', 'username'], 'string', 'max' => 95],
			[['email'], 'string', 'max' => 255],
			[['hash'], 'string', 'max' => 32],
			[['referal'], 'string', 'max' => 31],
			[['wmi_to_user_id'], 'string', 'max' => 12],
			[['computers_count'], 'default', 'value' => 1],
			[['referal'], RegularExpressionValidator::className(), 'pattern' => '/^[a-zA-Z0-9-]+$/'],
			[['order_id'], 'exist', 'targetClass' => 'common\models\Order', 'targetAttribute' => 'id'],
			[['subscription_type', 'referal', 'wmi_order_id', 'wmi_to_user_id'], 'default', 'value' => null],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'order_id' => 'Заказ',
			'hash' => 'Хеш',
			'guid' => 'GUID',
			'organization' => 'Название организации',
			'username' => 'Контактное лицо',
			'email' => 'E-mail',
			'computers_count' => 'Кол-во компьютеров',
			'licence_type' => 'Тип лицензии',
			'subscription_type' => 'Подписка',
			'referal' => 'Промо-код',
			'price' => 'Стоимость покупки',
			'summary' => 'События',
			'wmi_order_id' => 'Идентификатор заказа',
			'wmi_to_user_id' => 'Кошелек плательщика',
		];
	}

	/**
	 * @inheritdoc
	 * @return PurchaseQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new PurchaseQuery(get_called_class());
	}

	/**
	 * @return OrderQuery
	 */
	public function getOrder()
	{
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}
}
