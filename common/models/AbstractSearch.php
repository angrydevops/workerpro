<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\base\UnknownPropertyException;

/**
 * Base Search Model.
 */
abstract class AbstractSearch extends Model
{

	/**
	 * @return string model class
	 * @throws UnknownPropertyException if the model cannot be found
	 */
	public function getBaseModelClass()
	{
		$modelClass = self::className();
		if ($modelClass == false) {
			throw new UnknownPropertyException('Unknown Model');
		}

		$pos = strrpos($modelClass, '\\');
		if ($pos !== false) {
			$modelClass = substr($modelClass, $pos + 1);
		}

		if (substr($modelClass, -6) == 'Search') {
			$modelClass = substr($modelClass, 0, -6);
		}

		return $modelClass;
	}

	/**
	 * Get dataProvider by search params.
	 * @param array $params
	 * @return \yii\data\ActiveDataProvider
	 */
	public abstract function search($params);

	/**
	 * Add equal condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param string $attribute
	 */
	protected function addConditionEqual($query, $attribute)
	{
		$value = $this->$attribute;
		if (trim($value) == '') {
			return;
		}

		$query->andWhere([$attribute => $value]);
	}

	/**
	 * Add like condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param string $attribute
	 */
	protected function addConditionLike($query, $attribute)
	{
		$value = $this->$attribute;
		if ($value == '') {
			return;
		}

		$param = ':' . $attribute;
		$query->andWhere('UPPER(' . $attribute . ') LIKE ' . $param, [$param => '%' . mb_strtoupper($value, Yii::$app->charset) . '%']);
	}

	/**
	 * Add more condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param array $attributesCondition
	 * @param string $attributeValue
	 */
	protected function addConditionLikeMultiple($query, $attributesCondition, $attributeValue)
	{
		$value = $this->$attributeValue;
		if ($value == '') {
			return;
		}

		$attributeValueLike  = ':' . $attributeValue . '_like';
		$attributeValueEqual = ':' . $attributeValue . '_equal';

		$where = ['OR'];
		$params = [];
		foreach ($attributesCondition as $attribute) {
			$not_ip = strpos($attribute, '_ip') === false;
			if (strpos($attribute, '_id') === false && $not_ip) {
				$where[] = 'UPPER(' . $attribute . ') LIKE ' . $attributeValueLike;
				if (!array_key_exists($attributeValueLike, $params)) {
					$params[$attributeValueLike] = '%' . mb_strtoupper($value, Yii::$app->charset) . '%';
				}
			} else {
				if ($not_ip == false && preg_match('/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/', $value) == 0) {
					continue;
				}
				$where[] = $attribute . ' = ' . $attributeValueEqual;
				if (!array_key_exists($attributeValueEqual, $params)) {
					$params[$attributeValueEqual] = $value;
				}
			}
		}
		$query->andWhere($where, $params);

		// TODO: поиск по связям
		//foreach ($relations as $attribute) {
		//	$query->innerJoinWith($attribute);
		//}
	}

	/**
	 * Add compare condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param string $attributeCondition
	 * @param string $attributeValue
	 * @param string $compare
	 */
	protected function addConditionCompare($query, $attributeCondition, $attributeValue, $compare)
	{
		$value = $this->$attributeValue;
		if ($value == '') {
			return;
		}

		$attributeValue = ':' . $attributeValue;
		$query->andWhere($attributeCondition . ' ' . $compare . ' ' . $attributeValue, [$attributeValue => $value]);
	}

	/**
	 * Add less condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param string $attributeCondition
	 * @param string|null $attributeValue
	 */
	protected function addConditionLess($query, $attributeCondition, $attributeValue = null)
	{
		if ($attributeValue === null) {
			$attributeValue = $attributeCondition . '_max';
		}

		$this->addConditionCompare($query, $attributeCondition, $attributeValue, '<=');
	}

	/**
	 * Add more condition in ActiveQuery.
	 * @param \yii\db\ActiveQuery $query
	 * @param string $attributeCondition
	 * @param string|null $attributeValue
	 */
	protected function addConditionMore($query, $attributeCondition, $attributeValue = null)
	{
		if ($attributeValue === null) {
			$attributeValue = $attributeCondition . '_min';
		}

		$this->addConditionCompare($query, $attributeCondition, $attributeValue, '>=');
	}
}
