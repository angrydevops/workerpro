<?php

namespace common\models;

use Yii;
use common\validators\DatePgValidator;
use yii\validators\RegularExpressionValidator;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $guid
 * @property string $organization
 * @property string $username
 * @property string $email
 * @property string $licence_type
 * @property string $subscription_type
 * @property integer $computers_count
 * @property string $referal
 * @property double $price
 * @property string $created
 *
 * @property Purchase $purchase
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * User friendly label.
     */
    const LABEL_PLURAL = 'Заказы';

    public $user_type;

    /**
     * @var array of user types
     */
    public static $userTypes = [
        'ur' => 'Юридическое лицо',
        'fiz' => 'Частное лицо'
    ];

    /**
     * @var array of licence types
     */
    public static $licenceTypes = [
        'cloud' => 'Облачная',
        'local' => 'Локальная'
    ];

    /**
     * @var array of subscription types
     */
    public static $subscriptionTypes = [
        'threeMonths' => 'на 3 месяца',
        'sixMonths' => 'на 6 месяцев',
        'year' => 'на год'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization', 'username', 'email', 'referal'], 'filter', 'filter' => 'trim'],
            [['organization', 'username', 'email', 'licence_type', 'computers_count'], 'required'],
            [['email'], 'filter', 'filter' => 'mb_strtolower'],
            ['email', 'email', 'enableIDN' => false],
            [['licence_type', 'subscription_type', 'referal', 'guid','user_type'], 'string'],
            [['computers_count'], 'integer'],
            [['price'], 'number'],
            [['organization', 'username'], 'string', 'max' => 95],
            [['email'], 'string', 'max' => 255],
            [['referal'], 'string', 'max' => 31],
            [['computers_count'], 'default', 'value' => 1],
            [['created'], DatePgValidator::className()],
            [['referal'], RegularExpressionValidator::className(), 'pattern' => '/^[a-zA-Z0-9-]+$/'],
            [['subscription_type', 'referal'], 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guid' => 'GUID',
            'organization' => 'Название организации',
            'username' => 'Контактное лицо',
            'email' => 'E-mail',
            'computers_count' => 'Кол-во компьютеров',
            'licence_type' => 'Тип лицензии',
            'subscription_type' => 'Подписка',
            'referal' => 'Промо-код',
            'price' => 'Стоимость покупки',
            'created' => 'Создан',
        ];
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return PurchaseQuery
     */
    public function getPurchase()
    {
        return $this->hasOne(Purchase::className(), ['order_id' => 'id']);
    }

    /**
     * @return null|integer
     */
    public function calculatePrice()
    {
        $programPrices = Yii::$app->params['programPrices'];

        if ($this->licence_type === null) {
            return null;
        }

        $licence = $programPrices[$this->licence_type];
        $prices = [];
        $this->price = $this->computers_count;

        if (array_key_exists('subscribe', $licence)) {
            if (array_key_exists($this->subscription_type, $licence['subscribe'])) {
                $this->price *= $licence['subscribe'][$this->subscription_type]['count'];
                $prices = $licence['subscribe'][$this->subscription_type]['prices'];
            }
        } else {
            $prices = $licence['prices'];
        }

        foreach ($prices as $priceParams) {
            $isCurrent = true;
            $isCurrent = $isCurrent && ($priceParams['min'] === null || $priceParams['min'] <= $this->computers_count);
            $isCurrent = $isCurrent && ($priceParams['max'] === null || $priceParams['max'] >= $this->computers_count);

            if ($isCurrent) {
                $this->price *= $priceParams['price'];
                break;
            }
        }

        return $this->price;
    }

}
