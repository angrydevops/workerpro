<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Purchase]].
 *
 * @see Purchase
 * @method Purchase|array|null one($db = null)
 * @method Purchase[]|array all($db = null)
 */
class PurchaseQuery extends ActiveQuery
{
}