<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\behaviors\TreeBehavior;
use common\helpers\Url;
use common\validators\DatePgValidator;
use common\validators\SlugValidator;

/**
 * This is the model class for table "folders".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $title
 * @property string $title_seo
 * @property string $description
 * @property string $icon
 * @property string $color
 * @property string $updated
 * @property string $path
 * @property integer $post_count
 * @property integer $group_id
 * @property integer $sort
 *
 * @property string $UploadFile
 * @property string $UploadExternalUrl
 * @property string $UploadUrl
 * @property string $PathParent
 *
 * @property Folder $parent
 * @property Post[] $posts
 *
 * @method string getUploadUrl()
 */
class Folder extends ActiveRecord
{
	/**
	 * User friendly label.
	 */
	const LABEL_SINGULAR = 'Директория';

	/**
	 * User friendly label.
	 */
	const LABEL_PLURAL = 'Директории';

	/**
	 * Cache key for cache by id.
	 */
	const CACHE_KEY_BY_ID = 'folder_by_id';

	/**
	 * @var array
	 */
	protected static $_cachedById = [];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%folders}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			[['slug', 'name', 'title', 'title_seo', 'description'], 'filter', 'filter' => 'trim'],
			[['slug', 'name'], 'required'],
			[['slug', 'name'],      'string', 'max' => 31, 'min' => 2],
			[['title'],             'string', 'max' => 95],
			[['title_seo'], 'string', 'max' => 255],
			[['description'],       'string', 'max' => 1023],
			[['slug'],      SlugValidator::className()],
			[['updated'], DatePgValidator::className()],
			[['path'], 'unique'],
			[['title', 'title_seo', 'updated'], 'default', 'value' => null],
			[['sort'], 'default', 'value' => 0],
		];
		if ($this->getBehavior('tree') !== null) {
			$rules[] = [['PathParent'], 'exist', 'targetAttribute' => 'path'];
		}
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		$auto = parent::scenarios();
		return [
			'administrator' => $auto[self::SCENARIO_DEFAULT],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'slug' => 'Для url',
			'name' => 'Текст ссылки',
			'title' => 'Заголовок',
			'title_seo' => 'Подпись браузера',
			'description' => 'Описание',
			'updated' => 'Обновлена',
			'path' => 'Путь',
			'PathParent' => 'Родитель',
			'sort' => 'Номер по порядку',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'tree' => [
				'class' => TreeBehavior::className(),
				'attributeTreeElement' => 'slug',
			],
		];
	}

	/**
	 * @return FolderQuery
	 */
	public static function find()
	{
		return new FolderQuery(get_called_class());
	}

	/**
	 * @return FolderQuery
	 */
	public function getParent()
	{
		return $this->hasOne(Folder::className(), ['path' => 'path_parent']);
	}

	/**
	 * @return PostQuery
	 */
	public function getPosts()
	{
		return $this->hasMany(Post::className(), ['folder_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		Yii::$app->cacheCommon->delete(self::CACHE_KEY_BY_ID);
	}

	/**
	 * Sets the default values on create form.
	 */
	public function defaultOnCreate()
	{
		$this->updated = date('Y-m-d H:i:s');
	}

	/**
	 * @param boolean $scheme
	 * @param array|null $postLast
	 * @return string
	 */
	public function getUrl($scheme = false, $postLast = null)
	{
		$route = ['post/folder', 'folder_path' => str_replace('.', '/', $this->path)];

		return Url::toRouteFrontend($route, $scheme);
	}

	/**
	 * @param string|null $id
	 * @return array|null
	 */
	public static function getById($id = null) {
		$cachedById = self::$_cachedById;
		if ($cachedById == []) {
			$cachedById = Yii::$app->cacheCommon->get(self::CACHE_KEY_BY_ID);
			if ($cachedById == false) {
				$cachedByIdData = self::find()->select([
					'id',
					'slug',
					'name',
					'path' => 'REPLACE(ltree2text(path), \'.\', \'/\')',
					'path_parent' => 'subltree(path, 0, nlevel(path) - 1)',
				])->with([
					'parent' => function ($query) {
						/* @var FolderQuery $query */
						$query->select(['id', 'path']);
					},
				])->asArray()->all();

				$cachedById = [];
				if (!empty($cachedByIdData)) {
					$cachedById = [];
					foreach ($cachedByIdData as $item) {
						$cachedById[$item['id']] = [
							'slug'      => $item['slug'],
							'name'      => $item['name'],
							'path'      => $item['path'],
							'parent_id' => $item['parent']['id'],
						];
					}

					Yii::$app->cacheCommon->set(self::CACHE_KEY_BY_ID, $cachedById);
				}
			}
			if (!empty($cachedById)) {
				self::$_cachedById = $cachedById;
			}
		}

		if (is_numeric($id)) {
			if (array_key_exists($id, $cachedById)) {
				return $cachedById[$id];
			} else {
				return null;
			}
		}
		return $cachedById;
	}
}
