<?php
namespace common\models;

use yii\db\ActiveQuery;

/**
 * PostQuery is ActiveQuery with post scopes.
 *
 * @method Post|array|null one($db = null)
 * @method Post[]|array all($db = null)
 */
class PostQuery extends ActiveQuery
{
	/**
	 * @param string $column
	 * @return string
	 */
	public static function getAsTimestamp($column) {
		return 'EXTRACT(EPOCH FROM ' . $column . ' AT TIME ZONE current_setting(\'TIMEZONE\'))';
	}

	/**
	 * @return PostQuery
	 */
	public function notPublished()
	{
		$this->andWhere(['published' => null]);
		return $this;
	}

	/**
	 * @return PostQuery
	 */
	public function notPublishedOrInFuture()
	{
		$this->andWhere(['or', ['published' => null], 'published > CURRENT_TIMESTAMP']);
		return $this;
	}

	/**
	 * @return PostQuery
	 */
	public function isPublished()
	{
		$this->andWhere('published IS NOT NULL')->andWhere('published <= CURRENT_TIMESTAMP');
		return $this;
	}

	/**
	 * @param integer $days
	 * @return PostQuery
	 */
	public function publishedInLastDay($days)
	{
		$this->andWhere('"published" >= NOW() - interval \'' . (int) $days . ' days\'');
		return $this;
	}

	/**
	 * @param string $startDate
	 * @param string $endDate
	 * @return PostQuery
	 */
	public function publishedBetween($startDate, $endDate)
	{
		$this->isPublished();
		$this->where(['between', 'published', $startDate, $endDate]);
		return $this;
	}

	/**
	 * @param string $startDate
	 * @param string $endDate
	 * @return PostQuery
	 */
	public function publishedBetweenByWeek($startDate, $endDate) {
		$this->publishedBetween($startDate, $endDate);
		$this->groupBy(['week' => 'date_trunc(\'week\', published)']);
		$this->orderBy(['date_trunc(\'week\', published)' => SORT_DESC]);

		return $this;
	}

	/**
	 * @param int|string $folder_id
	 * @return PostQuery
	 */
	public function byFolder($folder_id)
	{
		$this->andWhere('id IN (SELECT post_id FROM post_folder WHERE folder_id = :folder_id)', [':folder_id' => (int) $folder_id]);
		return $this;
	}

	/**
	 * Минимально необходимый набор для списков.
	 * @return PostQuery
	 */
	public function forList()
	{
		$this->select([
			Post::tableName() . '.id',
			Post::tableName() . '.slug',
			Post::tableName() . '.name',
			Post::tableName() . '.folder_id_main',
			Post::tableName() . '.ad_group_id',
		])->with([
			'folderMain' => function ($query) {
				/* @var FolderQuery $query */
				$query->forPostList();
			},
		])->isPublished()->orderBy([Post::tableName() . '.published' => SORT_DESC]);
		return $this;
	}

	/**
	 * @param $folderIds
	 * @return PostQuery
	 */
	public function forFolderView($folderIds)
	{
		$this->andWhere(['IN', 'folder_id', $folderIds]);
		$this->orderBy(['idx(array[' . implode(',', $folderIds) . '], folder_id)' => SORT_ASC, 'sort' => SORT_ASC, 'name' => SORT_ASC]);
		$this->forView();

		return $this;
	}

	/**
	 * @return PostQuery
	 */
	public function forView()
	{
		$this->select([
			'id',
			'slug',
			'name',
			'title_seo',
			'description',
			'content',
			'icon',
			'published' => self::getAsTimestamp('published'),
			'folder_id',
		])->with([
			'folderMain' => function ($query) {
				/* @var FolderQuery $query */
				$query->forPostList();
			},
		])->andWhere('published IS NOT NULL');
		return $this;
	}

	/**
	 * @param boolean $with_future
	 * @return bool|string
	 */
	public function getLastPublished($with_future = false) {
		if ($with_future) {
			$this->andWhere('published IS NOT NULL');
		} else {
			$this->isPublished();
		}
		return $this->select([
			'published' => self::getAsTimestamp('published'),
		])->orderBy([Post::tableName() . '.published' => SORT_DESC])->limit(1)->scalar();
	}
}
