<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\helpers\StringHelper;
use common\helpers\Url;
use common\behaviors\HtmlImageBehavior;
use common\behaviors\SpacelessBehavior;
use common\validators\DatePgValidator;
use common\validators\SlugSiteValidator;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $title_seo
 * @property string $description
 * @property string $icon
 * @property string $content
 * @property string $published
 * @property string $updated
 * @property integer $folder_id
 * @property integer $sort
 *
 * @property Folder $folderMain
 */
class Post extends ActiveRecord
{
	/**
	 * User friendly label.
	 */
	const LABEL_SINGULAR = 'Материал';

	/**
	 * User friendly label.
	 */
	const LABEL_PLURAL = 'Материалы';

	/**
	 * Default name value.
	 */
	const DEFAULT_NAME = 'Название';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%posts}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			[['slug', 'name', 'title_seo', 'description', 'content'], 'filter', 'filter' => 'trim'],
			[['name', 'description'],                                 'filter', 'filter' => function ($value) {
				return preg_replace('/[ ]{2,}/', ' ', str_replace(["\n", "\r"], ' ', $value));
			}],
			[['slug', 'name', 'description', 'folder_id'], 'required'],
			[['slug'],                           'string', 'max' => 31, 'min' => 2],
			[['name'],                           'string', 'max' => 95],
			[['title_seo', 'icon'], 'string', 'max' => 255],
			[['description'],                    'string', 'max' => 1023],
			[['content'],                        'string'],
			[['folder_id'], 'integer'],
			[['published'], 'date', 'format' => 'php:Y-m-d| H:i'],
			[['slug'],       SlugSiteValidator::className()],
			[['updated'],      DatePgValidator::className()],
			[['published'], 'unique', 'targetAttribute' => 'date_trunc(\'minute\', published)', 'message' => 'На эту минуту уже назначена другая публикация.'],
			[['folder_id'], 'exist', 'targetClass' => 'common\models\Folder', 'targetAttribute' => 'id'],
			[['title_seo', 'icon', 'content', 'published', 'updated'], 'default', 'value' => null],
			[['sort'], 'default', 'value' => 0],
		];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		$auto = parent::scenarios();
		return [
			'administrator' => $auto[self::SCENARIO_DEFAULT],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'slug' => 'Для url',
			'name' => 'Название',
			'title_seo' => 'Заголовок',
			'description' => 'Описание',
			'icon' => 'Иконка',
			'content' => 'Содержимое',
			'published' => 'Опубликован',
			'updated' => 'Обновлён',
			'folder_id' => 'Директория',
			'sort' => 'Номер по порядку',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'htmlImage' => [
				'class' => HtmlImageBehavior::className(),
			],
			'spaceless' => [
				'class' => SpacelessBehavior::className(),
			],
		];
	}

	/**
	 * @return PostQuery
	 */
	public static function find()
	{
		return new PostQuery(get_called_class());
	}

	/**
	 * @return FolderQuery
	 */
	public function getFolderMain()
	{
		return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
	}

	/**
	 * Sets the default values on create form.
	 */
	public function defaultOnCreate()
	{
		$this->slug = 'nazvanie';
		$this->name = self::DEFAULT_NAME;
		$this->description = 'Описание';
		$this->published = date('Y-m-d H:i', max(
			strtotime('+1 year'),
			self::find()->getLastPublished(true) + 300
		));
		$this->updated = date('Y-m-d H:i:s');
		$this->folder_id = 1;
	}

	/**
	 * @param array|Post $post
	 * @return array
	 */
	public static function getUrlParams($post) {
		// TODO: ссылки через админку

		$postsUrlReplace = Yii::$app->params['postsUrlReplace'];
		if (array_key_exists($post['id'], $postsUrlReplace)) {
			$params = $postsUrlReplace[$post['id']];

			if ($params[0] === 'post/view-by-slug') {
				$params['slug'] = $post['slug'];
			}
		} else {
			$params = [
				'post/view',
				'folder_path' => StringHelper::mb_str_replace('.', '/', $post['folderMain']['path']),
				'post_slug'   => $post['slug'],
				'post_id'     => $post['id'],
			];
		}

		return $params;
	}

	/**
	 * @param boolean $scheme
	 * @return string
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\base\InvalidParamException
	 */
	public function getUrl($scheme = false)
	{
		return Url::toRouteFrontend(self::getUrlParams($this), $scheme);
	}

	/**
	 * @param integer $id
	 * @return bool|string
	 */
	public static function getContentById($id)
	{
		return self::find()->select('content')->where(['id' => $id])->scalar();
	}
}