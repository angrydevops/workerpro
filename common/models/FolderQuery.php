<?php
namespace common\models;

use yii\db\ActiveQuery;
use common\behaviors\TreeQueryTrait;

/**
 * FolderQuery is ActiveQuery with folder scopes.
 *
 * @method Folder|array|null one($db = null)
 * @method Folder[]|array all($db = null)
 */
class FolderQuery extends ActiveQuery
{
	use TreeQueryTrait;

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function orderTree()
	{
		/** @var ActiveQuery $this */
		$this->orderBy([
			'ltree2text(subpath(path, 0, 1))' => SORT_ASC,
			'sort' => SORT_ASC,
		]);
		return $this;
	}

	/**
	 * @param string $path
	 * @return FolderQuery
	 */
	public function withChildren($path)
	{
		$this->andWhere('path ~ (\'' . $path . '\' || \'.*\')::lquery');

		return $this;
	}

	/**
	 * @param string $path
	 * @return FolderQuery
	 */
	public function sameParent($path)
	{
		$this->andWhere('path ~ ((subltree(\'' . $path . '\', 0, 1))::text || \'.*\')::lquery');

		return $this;
	}

	/**
	 * @return FolderQuery
	 */
	public function forPostList()
	{
		$this->select(['id', 'slug', 'name', 'path']);
		return $this;
	}

	/**
	 * @return FolderQuery
	 */
	public function forSitemap()
	{
		$this->select([
			'path',
			'updated' => 'EXTRACT(EPOCH FROM updated AT TIME ZONE current_setting(\'TIMEZONE\'))',
		]);
		return $this;
	}

	/**
	 * @return FolderQuery
	 */
	public function forTree() {
		$this->select(['id', 'name', 'slug', 'path'])->orderTree();
		return $this;
	}

	/**
	 * @param integer $id
	 * @return FolderQuery
	 */
	public function forView($id)
	{
		$this->select([
			'id',
			'slug',
			'name',
			'title',
			'title_seo',
			'description',
			'icon',
			'path' => 'REPLACE(ltree2text(path), \'.\', \'/\')',
		])->where(['id' => $id]);
		return $this;
	}
}
