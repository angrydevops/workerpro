<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\behaviors\UploadBehavior;
use common\validators\DatePgValidator;

/**
 * This is the model class for table "user".
 * @package common\models
 *
 * @property integer $id
 * @property string $role
 * @property string $title
 * @property string $avatar
 * @property string $created
 * @property string $updated
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 *
 * @property string $password write-only password
 * @property string $passwordNew
 * @property string $UploadFile
 * @property string $UploadExternalUrl
 * @property string $UploadUrl
 *
 * @property Post[] $posts
 *
 * @method string getUploadUrl()
 */
class User extends ActiveRecord implements IdentityInterface
{
	/**
	 * User friendly label.
	 */
	const LABEL_SINGULAR = 'Пользователь';

	/**
	 * User friendly label.
	 */
	const LABEL_PLURAL = 'Пользователи';

	const COLUMN_TOKEN = 'blowfish(id::TEXT || date_part(\'epoch\', updated) || \'_\' || COALESCE(password_hash, \'\'))';

	/**
	 * @var string|null
	 */
	public $token;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%users}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			[['title', 'first_name', 'last_name', 'email',], 'filter', 'filter' => 'trim'],
			[['email'], 'filter', 'filter' => 'mb_strtolower'],
			[['role', 'title'], 'required'],
			[['auth_key'],                   'string', 'max' => 32],
			[['first_name', 'last_name'],    'string', 'max' => 63],
			['passwordNew',                  'string', 'max' => 72, 'min' => 6],
			[['title'],                      'string', 'max' => 95],
			[['avatar', 'email', 'password_hash'], 'string', 'max' => 255],
			[['role'],      'in', 'range' => array_keys(self::dictionaryRole())],
			[['email'], 'email', 'enableIDN' => true],
			[['created', 'updated'],                           DatePgValidator::className()],
			[['first_name', 'last_name', 'email', 'auth_key', 'password_hash', 'created', 'updated'], 'default', 'value' => null],
		];
		if ($this->getBehavior('upload') !== null) {
			$rules[] = [['UploadFile'], 'image', 'minWidth' => 50, 'minHeight' => 50, 'maxWidth' => 3840, 'maxHeight' => 3840];
			$rules[] = [['UploadExternalUrl'], 'url', 'enableIDN' => true];
		}
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		$auto = parent::scenarios();
		$rules = [
			'administrator' => $auto[self::SCENARIO_DEFAULT],
			'requestPassword'  => ['password_hash'],
		];

		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'role' => 'Роль',
			'title' => 'Подпись',
			'avatar' => 'Аватар',
			'created' => 'Зарегистрирован',
			'updated' => 'Обновлён',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'email' => 'Email',
			'UploadFile' => 'локальную',
			'UploadExternalUrl' => 'из интернета',
			'passwordNew' => 'Новый пароль',
			'password_hash' => 'Хеш пароля',
			'auth_key' => 'Ключ авторизации',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'upload' => [
				'class' => UploadBehavior::className(),
				'attribute' => 'avatar',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if ($this->getScenario() == 'administrator') {
				$this->setPassword(\Yii::$app->getSecurity()->generateRandomString());
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert, $changedAttributes) {
		if ($insert && $this->getScenario() == 'administrator') {
			$user = User::find()
				->tokenByEmail($this->email)
				->addSelect('title')
				->one();

			\Yii::$app->getMailer()
				->compose(
					[
						'html' => 'authInvite-html',
						'text' => 'authInvite-text',
					],
					[
						'username' => $user->title,
						'token'    => $user->token,
					]
				)
				->setTo($this->email)
				->setSubject('Приглашение')
				->send();
		}

		return parent::afterSave($insert, $changedAttributes);
	}

	/**
	 * @return UserQuery
	 */
	public static function find()
	{
		return new UserQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return static::find()
			->select([
				'id',
				'role',
				'email',
				'title',
				'avatar',

				'password_hash',
				'auth_key',
			])
			->where(['id' => $id])
			->one();
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @return string
	 */
	public function getPasswordNew()
	{
		return '';
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
	}

	/**
	 * @param string $password
	 */
	public function setPasswordNew($password)
	{
		if ($password) {
			// записываем только если не пуст
			$this->setPassword($password);
		}
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->getSecurity()->generateRandomString();
	}

	/**
	 * @return array
	 */
	public static function dictionaryRole() {
		$roles = Yii::$app->db->createCommand('SELECT unnest(enum_range(NULL::user_roles))')->queryColumn();
		return array_combine($roles, $roles);
	}
}
