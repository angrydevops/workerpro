<?php
namespace common\models;

use yii\db\ActiveQuery;

/**
 * UserQuery is ActiveQuery with user scopes.
 *
 * @method User|array|null one($db = null)
 * @method User[]|array all($db = null)
 */
class UserQuery extends ActiveQuery
{

	/**
	 * @return UserQuery
	 */
	public function emailVerified()
	{
		$this->andWhere(['role' => self::getEmailVerifiedRoles()]);

		return $this;
	}

	/**
	 * @return array
	 */
	public static function getEmailVerifiedRoles()
	{
		return ['verified', 'moderator', 'administrator'];
	}

	/**
	 * @return UserQuery
	 */
	public function emailNotVerified()
	{
		$this->andWhere(['not in', 'role', self::getEmailVerifiedRoles()]);

		return $this;
	}

	/**
	 * @param string $email
	 * @return $this
	 */
	public function tokenByEmail($email)
	{
		$this
			->select([
				'token' => User::COLUMN_TOKEN,
			])
			->where(['email' => $email]);

		return $this;
	}
}