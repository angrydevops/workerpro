<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);
return [
	'language' => 'ru',
	'sourceLanguage' => 'ru',
	'timeZone' => 'Europe/Moscow',
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		'cacheCommon' => [
			'keyPrefix' => '',
		],
		'frontendUrlManager' => [
			'class' => 'frontend\components\FrontendUrlManager',
			'hostInfo' => 'http://' . $params['frontendDomain']
		],
		'db' => [
			'class' => 'yii\db\Connection',
			'enableSchemaCache' => true,
			'schemaCacheDuration' => 0,
			'schemaCache' => 'cacheCommon',
			'on afterOpen' => function($event) {
				$event->sender->createCommand('SET timezone = \'' . \Yii::$app->timeZone . '\';')->execute();
			},
		],
		'license'=> [
			'class'=>'common\components\License',
			'host'=>'lic.worker.pro',
			'dbname'=>'license',
			'username'=>'WebApp@worker-lic',
			'password'=>'rt33b9!dfd0E!E2Eed5mww1'
		],
		'log' => [
			'targets' => [
				'errorCommon' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/error.log',
					'rotateByCopy' => strtoupper(substr(PHP_OS, 0, 3)) === 'WIN',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			'messageConfig' => [
				'charset' => 'UTF-8',
				'from' => ['info@worker.pro' => 'Worker'],
			],
		],
	],
];
