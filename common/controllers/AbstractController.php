<?php
namespace common\controllers;

use yii\web\Controller;
use yii\base\UnknownPropertyException;

/**
 * Base Controller.
 */
abstract class AbstractController extends Controller
{
	/**
	 * @return string model class
	 * @throws UnknownPropertyException if the model cannot be found
	 */
	public function getBaseModelClass()
	{
		$modelClass = self::className();
		if ($modelClass == false) {
			throw new UnknownPropertyException('Unknown Model');
		}

		$pos = strrpos($modelClass, '\\');
		if ($pos !== false) {
			$modelClass = substr($modelClass, $pos + 1);
		}

		if (substr($modelClass, -10) == 'Controller') {
			$modelClass = substr($modelClass, 0, -10);
		}

		return $modelClass;
	}

	/**
	 * @param null|string $model
	 * @param null|string $action
	 * @param null|boolean $own
	 * @return string
	 * @throws UnknownPropertyException
	 */
	public function getPermissionName($model = null, $action = null, $own = null) {
		if (empty($model)) {
			$model = $this->getBaseModelClass();
		}
		if (empty($action)) {
			$action = $this->action->id;
		}

		$pos = strrpos($model, '\\');
		if ($pos !== false) {
			$model = substr($model, $pos + 1);
		}

		if ($action == 'create') {
			$action = 'indexAndCreate';
		} else if ($action == 'index') {
			$action = ($own === false ? 'indexNotOwn' : 'indexAndCreate');
		} else if ($action == 'update' || $action == 'delete') {
			$action = ($own === true ? 'updateAndDeleteOwn' : 'updateAndDelete');
		} else {
			if ($own === true) {
				$action .= 'Own';
			} elseif ($own === false) {
				$action .= 'NotOwn';
			}
		}

		return strtolower($model[0]) . substr($model, 1) . strtoupper($action[0]) . substr($action, 1);
	}
}
