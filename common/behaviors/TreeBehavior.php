<?php
namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * TreeBehavior is behavior for materialized path tree.
 *
 * @author Kostya M <primat@list.ru>
 *
 * @property ActiveRecord $owner
 */
class TreeBehavior extends Behavior
{
	/**
	 * @var string Attribute name for materialized path storage.
	 */
	public $attributeTree = 'path';

	/**
	 * @var string Attribute name of which are path.
	 */
	public $attributeTreeElement = 'id';

	/**
	 * @inheritdoc
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
		];
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	function beforeValidate($event)
	{
		$value = $this->owner->getDirtyAttributes([$this->attributeTreeElement]);
		if (empty($value)) {
			return;
		}

		// если изменился, то меняем path
		$value = $value[$this->attributeTreeElement];
		$parent = $this->getPathParent();
		$this->owner->{$this->attributeTree} = ($parent == '') ? $value : ($parent . '.' . $value);
	}

	/**
	 * Getter for virtual attribute.
	 * @return string
	 */
	public function getPathParent()
	{
		$value = $this->owner->{$this->attributeTree};

		$pos = strrpos($value, '.');
		if ($pos === false) {
			$value = '';
		} else {
			$value = substr($value, 0, $pos);
		}

		return $value;
	}

	/**
	 * Setter for virtual attribute.
	 * @param string $value
	 */
	public function setPathParent($value)
	{
		$this->owner->{$this->attributeTree} = ($value == '') ? $this->owner->{$this->attributeTreeElement} : ($value . '.' . $this->owner->{$this->attributeTreeElement});
	}

	/**
	 * Check that it is answer.
	 * @return boolean
	 */
	public function getIsAnswer()
	{
		return strpos($this->owner->{$this->attributeTree}, '.') !== false;
	}

	/**
	 * Return parent key.
	 * @return null|string
	 */
	public function getParentKey()
	{
		$path = $this->owner->{$this->attributeTree};
		$last_pos = strrpos($path, '.');
		if ($last_pos === null) {
			return null;
		}

		$path = substr($path, 0, $last_pos);
		$last_pos = strrpos($path, '.');
		if ($last_pos === null) {
			return $path;
		}

		return substr($path, $last_pos);
	}
}
