<?php
namespace common\behaviors;

use yii\db\ActiveQuery;

/**
 * TreeQueryTrait is trait with tree scopes for ActiveQuery.
 *
 * @author Kostya M <primat@list.ru>
 */
trait TreeQueryTrait
{
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function orderTree()
	{
		/** @var ActiveQuery $this */
		$this->orderBy('path');
		return $this;
	}

	/**
	 * @param int|string $level
	 * @return \yii\db\ActiveQuery
	 */
	public function whereLevel($level) {
		/** @var ActiveQuery $this */
		$this->andWhere(['nlevel(path)' => $level]);
		return $this;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function parents()
	{
		return $this->whereLevel(1);
	}
}
