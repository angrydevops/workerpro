<?php
namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\helpers\File;
use common\helpers\Url;

/**
 * HtmlImageBehavior is behavior for images
 *
 * @property ActiveRecord $owner
 */
class HtmlImageBehavior extends Behavior
{
	/**
	 * @var string
	 */
	public $attribute = 'content';

	/**
	 * @inheritdoc
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
		];
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function beforeSave($event)
	{
		$attributeChanged = $this->owner->isAttributeChanged($this->attribute);
		$pathMedia = Yii::getAlias('@media');

		if ($attributeChanged) {
			$imageDomain = \common\helpers\Url::imageDomain();
			$pattern = '/<img[^>]+src="((?!((http\:)?\/\/' . str_replace('.', '\.', Yii::$app->params['frontendDomain']) . ')?\/)[^"]+)/iu';
			preg_match_all($pattern, $this->owner->{$this->attribute}, $matches, PREG_SET_ORDER);
			foreach ($matches as $attributes) {
				$src = $attributes[1];
				$request = new \BCA\CURL\CURL(html_entity_decode($src));
				$request->option(CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
				$request->option(CURLOPT_SSL_VERIFYPEER, false);
				$request->option(CURLOPT_SSL_VERIFYHOST, false);
				$request = $request->get();
				if ($request->success()) {
					$subUrl = $this->owner->slug;
					$fullPath = preg_match('/[^\/]+$/', $src, $url) ? $url[0] : $src;
					$fullPath = mb_strrpos($fullPath, '.');
					$subUrl .= ($fullPath === false ? '.jpg' : mb_substr($src, $fullPath));
					$questionMarkPos = mb_strpos($subUrl, '?');
					if ($questionMarkPos !== false) {
						$subUrl = mb_substr($subUrl, 0, $questionMarkPos);
					}

					list($fullPath, $subUrl) = UploadBehavior::getFullPath(date('m'), $subUrl);
					if (file_put_contents($fullPath, $request)) {
						if (function_exists('chgrp') && function_exists('chown')) {
							@chgrp($fullPath, Yii::$app->params['ownerGroup']);
							@chown($fullPath, Yii::$app->params['ownerUser']);
						}

						$this->owner->{$this->attribute} = str_replace($src, $imageDomain . \common\helpers\Url::thumbnail($subUrl), $this->owner->{$this->attribute});
					}
				}
			}

			if (preg_match_all('/(<a[^>]+>)?(<img[^>]+>)(<\/a>)?/iu', $this->owner->{$this->attribute}, $images, PREG_SET_ORDER)) {
				foreach ($images as $imageTags) {
					if (!preg_match_all('/(src|width|height)="([^"]+)"/iu', $imageTags[2], $imageTagAttributes, PREG_SET_ORDER)) {
						continue;
					}
					$imageTagAttributes = \yii\helpers\ArrayHelper::map($imageTagAttributes, 1, 2);

					$src = \common\helpers\Url::getSubUrlFromFull($imageTagAttributes['src']);
					if ($src == false) {
						continue;
					}

					if (preg_match('/\-(\d+)x(\d+)\.(jpe?g|png|gif)$/i', $imageTagAttributes['src'], $sizePreset)) {
						$src = str_replace($sizePreset[0], '.' . $sizePreset[3], $src);
					}

					$fullPath = $pathMedia . $src;
					if (!file_exists($fullPath)) {
						continue;
					}

					$sizeImageWidth = getimagesize($fullPath);
					if ($sizeImageWidth === false) {
						continue;
					}
					$sizeImageHeight = $sizeImageWidth[1];
					$sizeImageWidth  = $sizeImageWidth[0];

					if ($sizeImageWidth > 838) {
						// не обернуто ссылкой на большое изображение
						if ($imageTags[1] == '' && empty($imageTags[3])) {
							if ($sizeImageWidth > 1920 || $sizeImageHeight > 1920) {
								if ($sizeImageWidth > $sizeImageHeight) {
									$fullPath = \common\helpers\Url::thumbnail($src, 1920, round($sizeImageHeight * 1920 / $sizeImageWidth));
								} else {
									$fullPath = \common\helpers\Url::thumbnail($src, round($sizeImageWidth * 1920 / $sizeImageHeight), 1920);
								}
							} else {
								$fullPath = \common\helpers\Url::thumbnail($src);
							}

							$this->owner->{$this->attribute} = str_replace(
									$imageTags[2],
									'<a class="image-big" href="' . $imageDomain . $fullPath . '" target="_blank">' . $imageTags[2] . '</a>',
									$this->owner->{$this->attribute}
							);
						}
					} else {
						// обернуто ссылкой на маленькое изображение
						if (!($imageTags[1] == '' || empty($imageTags[3]))) {
							$this->owner->{$this->attribute} = str_replace(
									$imageTags[0],
									$imageTags[2],
									$this->owner->{$this->attribute}
							);
						}
					}

					// размер исходного изображения совпадает с указанными размерами
					if ($sizeImageWidth == $imageTagAttributes['width'] && $sizeImageHeight == $imageTagAttributes['height']) {
						// если при этом была пресетная дописка - её надо очистить
						if ($sizePreset) {
							$this->owner->{$this->attribute} = str_replace(
									$imageTags[2],
									str_replace($sizePreset[0], '.' . $sizePreset[3], $imageTags[2]),
									$this->owner->{$this->attribute}
							);
						}

						continue;
					}

					// текущий пресет совпадает с указанными размерами
					if ($sizePreset && $sizePreset[1] == $imageTagAttributes['width'] && $sizePreset[2] == $imageTagAttributes['height']) {
						continue;
					}
					$this->owner->{$this->attribute} = str_replace(
							$imageTags[2],
							str_replace($imageTagAttributes['src'], $imageDomain . \common\helpers\Url::thumbnail($src, $imageTagAttributes['width'], $imageTagAttributes['height']), $imageTags[2]),
							$this->owner->{$this->attribute}
					);
				}
			}
		}
	}
}
