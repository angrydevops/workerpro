<?php
namespace common\behaviors;

use yii\db\ActiveQuery;

/**
 * TreeQuery is ActiveQuery with tree scopes.
 *
 * @author Kostya M <primat@list.ru>
 */
class TreeQuery extends ActiveQuery
{
	use TreeQueryTrait;
}
