<?php
namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * SpacelessBehavior removes whitespace characters between HTML tags. Whitespaces within HTML tags
 * or in a plain text are always left untouched.
 *
 * @property ActiveRecord $owner
 */
class SpacelessBehavior extends Behavior
{
	/**
	 * @var string
	 */
	public $attribute = 'content';

	/**
	 * @var array
	 */
	public $blockTags = [
		'h[1-6]', 'blockquote', 'div', 'p',
		'ol', 'ul', 'li',
		'table', 'tbody', 'tr', 'th', 'td',
	];

	/**
	 * @var array
	 */
	public $inlineTags = [
		'a', 'abbr', 'em', 's', 'span', 'strong', 'sub', 'sup',
		'mark',
	];

	/**
	 * @var array
	 */
	public $allowEmptyTags = [
		'td', 'th',
	];

	/**
	 * @inheritdoc
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
		];
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function beforeSave($event)
	{
		if (!$this->owner->isAttributeChanged($this->attribute)) {
			return;
		}

		$whitespaces = preg_quote(\common\helpers\StringHelper::WHITESPACES);

		$blockTags  = implode('|', $this->blockTags);
		$inlineTags = implode('|', $this->inlineTags);

		$content = $this->owner->{$this->attribute};

		$content = str_replace("\r", '', $content);
		$content = str_replace("\t", ' ', $content);

		$content = preg_replace('/<\/(' . $blockTags . ')>[' . $whitespaces . ']++<(' . $blockTags . ')/iu', '</$1><$2', $content);

		$whitespaces = '&nbsp;|[' . $whitespaces . ']';

		$content = preg_replace('/(<(?:' . $inlineTags . ')[^>]++>)(' . $whitespaces . ')++/iu', '$2$1', $content);
		$content = preg_replace('/(' . $whitespaces . ')++(<\/(?:' . $inlineTags . ')>)/iu', '$2$1', $content);

		$content = preg_replace('/(<(?:' . $blockTags . ')[^>]++>)(?:' . $whitespaces . ')++/iu', '$1', $content);
		$content = preg_replace('/(?:' . $whitespaces . ')++(<\/(?:' . $blockTags . ')>)/iu', '$1', $content);

		$content = preg_replace('/(?:' . $whitespaces . ')++(<[bh]r[ ]*\/?>)/iu', '$1', $content);
		$content = preg_replace('/(<[bh]r[ ]*\/?>)(?:' . $whitespaces . ')++/iu', '$1', $content);

		$blockTags = implode('|', array_diff($this->blockTags, $this->allowEmptyTags));
		$content = preg_replace('/<(?:' . $blockTags . '|' . $inlineTags . ')[^>]*><\/(?:' . $blockTags . '|' . $inlineTags . ')>/iu', '', $content);

		$content = preg_replace('/ {2,}/', ' ', $content);

		$this->owner->{$this->attribute} = $content;
	}
}