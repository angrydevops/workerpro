<?php
namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use Imagine\Image\Point;

/**
 * UploadBehavior is behavior for easy upload.
 *
 * @author Kostya M <primat@list.ru>
 *
 * @property ActiveRecord $owner
 */
class UploadBehavior extends Behavior
{
	/**
	 * @var string Attribute name for storage internal url.
	 */
	public $attribute = 'image';

	/**
	 * @var string Virtual attribute for upload file from POST.
	 */
	public $UploadFile;

	/**
	 * @var string Virtual attribute for upload file from external url.
	 */
	public $UploadExternalUrl;

	/**
	 * @var string where to get suburl from
	 */
	public $subUrlResource = 'slug';

	/**
	 * @var string|null If remove or update icon is successful, then there is the old upload.
	 */
	protected $_oldUpload;

	/**
	 * @inheritdoc
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
			ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
			ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
			ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterDelete',
			ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
		];
	}

	/**
	 * @return string Getter for internal url.
	 */
	public function getUploadUrl() {
		if ($this->owner->{$this->attribute}) {
			return \common\helpers\Url::imageDomain() . \common\helpers\Url::thumbnail($this->owner->{$this->attribute});
		}

		return '';
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function beforeValidate($event)
	{
		$this->UploadFile = \yii\web\UploadedFile::getInstance($this->owner, 'UploadFile');
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function beforeSave($event)
	{
		$value = $this->UploadFile;
		if ($value instanceof \yii\web\UploadedFile && $value->error == UPLOAD_ERR_OK) {
			list($fullPath, $subUrl) = self::getFullPath($this->getSubDir(), $value->name);
			if (move_uploaded_file($value->tempName, $fullPath)) {
				$this->_oldUpload = $this->owner->{$this->attribute};

				$this->owner->{$this->attribute} = $subUrl;
			}
		}

		$value = $this->UploadExternalUrl;
		if (!empty($value)) {
			$request = new \BCA\CURL\CURL($value);
			$request->option(CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
			$request->option(CURLOPT_SSL_VERIFYPEER, false);
			$request->option(CURLOPT_SSL_VERIFYHOST, false);
			$request = $request->get();

			// TODO: эмулировать валидацию картинки

			if ($request->success()) {
				$subUrl = $this->owner->{$this->subUrlResource};
				$subUrl = mb_substr($subUrl, 0, 32);

				if (strpos($value, 'youtube') !== false) {
					$subUrl .= '-noshift';
				}

				if (preg_match('/([^\/?]+)(\?|$)/', $value, $url)) {
					$value = $url[1];
				}

				$extPos = mb_strrpos($value, '.');
				$subUrl .= ($extPos === false ? '.jpg' : mb_substr($value, $extPos));

				list($fullPath, $subUrl) = self::getFullPath($this->getSubDir(), $subUrl);
				if (file_put_contents($fullPath, $request)) {
					if (function_exists('chgrp') && function_exists('chown')) {
						@chgrp($fullPath, Yii::$app->params['ownerGroup']);
						@chown($fullPath, Yii::$app->params['ownerUser']);
					}

					$this->_oldUpload = $this->owner->{$this->attribute};

					$this->owner->{$this->attribute} = $subUrl;
				}
			}
		}
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function beforeDelete($event)
	{
		$this->_oldUpload = $this->owner->{$this->attribute};
	}

	/**
	 * @param \yii\base\ModelEvent $event
	 */
	public function afterDelete($event)
	{
		if (!empty($this->_oldUpload)) {
			$path =  Yii::getAlias('@media') . $this->_oldUpload;
			if (is_file($path)) {
				unlink($path);
			}
		}
	}

	/**
	 * @return string
	 */
	public function getSubDir() {
		return strtolower($this->owner->formName()) . '-' . $this->attribute;
	}

	/**
	 * @param string $subDir
	 * @param string $filename
	 * @return array
	 */
	public static function getFullPath($subDir, $filename) {
		$subDir = '/' . $subDir .'/';

		$dir = Yii::getAlias('@media') . $subDir;
		if (!is_dir($dir)) {
			mkdir($dir, 0775);

			if (function_exists('chgrp') && function_exists('chown')) {
				@chgrp($dir, Yii::$app->params['ownerGroup']);
				@chown($dir, Yii::$app->params['ownerUser']);
			}
		}

		$path = mb_strrpos($filename, '.');
		if ($path === false) {
			$ext = '.jpg';
			$filename = \common\helpers\Inflector::slug($filename);
		} else {
			$ext = mb_strtolower(mb_substr($filename, $path));
			$filename = \common\helpers\Inflector::slug(mb_substr($filename, 0, $path));
		}
		if (empty($filename)) {
			$filename = 'x3';
		}

		while (file_exists($path = $dir . $filename . $ext)) {
			$filename = $filename . rand(0, 9);
		}

		return [$path, $subDir . $filename . $ext];
	}
}
