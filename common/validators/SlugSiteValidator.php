<?php
namespace common\validators;

use yii\validators\RegularExpressionValidator;

class SlugSiteValidator extends RegularExpressionValidator
{
	/**
	 * @inheritdoc
	 */
	public $pattern = '/^[a-z0-9][a-z0-9\-]*[a-z0-9]$/';

	/**
	 * @inheritdoc
	 */
	public $message = '{attribute} должен содержать только маленькие буквы английского алфавита, цифры или дефис.';
}
