<?php
namespace common\validators;

use yii\validators\RegularExpressionValidator;

/**
 * SlugValidator validates that the attribute value is a valid slug.
 *
 * @author Kostya M <primat@list.ru>
 */
class SlugValidator extends RegularExpressionValidator
{
	/**
	 * @inheritdoc
	 */
	public $pattern = '/^[A-Za-z0-9\._-]+$/';

	/**
	 * @inheritdoc
	 */
	public $message = '{attribute} должен содержать только буквы английского алфавита, цифры или дефис.';
}
