<?php

namespace common\components;

use yii\base\Component;

class License extends Component{


    public $host;
    public $dbname;
    public $username;
    public $password;

    protected static $periods = [
        '13E76132-1D35-4C58-839D-3308F76FC64F'=>'threeMonths',
        '6967FE61-4615-4AC0-9D2A-F8722605B810'=>'sixMonths',
        '9DFE3C77-B0F8-432C-A584-A74FE268EA04'=>'year',
        '236F486F-29FF-42F5-87F5-1CB42A4B9F5A'=>0
    ];

    /** @var \yii\db\Connection */
    private $db;

    public function init(){
        parent::init();
        $this->db = \Yii::createObject([
            'class' => 'yii\db\Connection',
            'dsn' => 'dblib:host='.$this->host.';dbname='.$this->dbname,
            'username' => $this->username,
            'password' => $this->password,
            'charset'=>'UTF-8'
        ]);
    }


    public function getAll($hash){
        $res = $this->db->createCommand('exec dbo.LicWebClientSelect :hash',['hash'=>$hash]);
        $items = [];

        foreach($res->queryAll() as $item){
            //$item['client_name'] = iconv('cp1251','utf-8',$item['client_name']);// mb_convert_encoding($item['client_name'], 'utf-8',mb_detect_encoding($item['client_name']));
            $item['subscription_type'] = $this->getSubscriptionType($item['subscribe_type_guid']);
            $items [] = $item;
        }
        return $items;
    }

    public function getSubscriptionType($guid){
        foreach(self::$periods as $key=>$val){
            if($key === $guid){
                return $val;
            }
        }
        return null;
    }
}