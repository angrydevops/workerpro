<?php

namespace common\components;

use Yii;

/**
 * @inheritdoc
 *
 * @property \common\models\User|\yii\web\IdentityInterface|null $identity
 * @method   \common\models\User|\yii\web\IdentityInterface|null getIdentity() getIdentity(boolean $autoRenew = true)
 */
class User extends \yii\web\User
{
    /**
     * Gets user data from session
     * @param string|null $param
     * @return mixed
     */
    public function getGuest($param = null) {
        $session = Yii::$app->getSession();
        $autocomplete = $session->get('autocomplete', []);
        $referal = $session->get('referal', null);

        $userInfo = $autocomplete;

        if ($referal !== null) {
            if ($referal['created'] > strtotime('3 months ago')) {
                $userInfo['referal'] = $referal['key'];
            } else {
                $session->remove('referal');
            }
        }

        if ($param !== null) {
            return $userInfo[$param];
        }

        return $userInfo;
    }

    /**
     * Writes autocomplete data for forms to session
     * @param array $data
     */
    public function setAutocomplete($data = []) {
        $session = Yii::$app->getSession();
        $autocomplete = $session->get('autocomplete', []);

        $session->set('autocomplete', array_merge($autocomplete, $data));
    }
}
