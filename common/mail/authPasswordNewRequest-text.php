<?php

/* @var $this yii\web\View */
/* @var $username string */
/* @var $token    string */

$app = Yii::$app;
?>
Здравствуйте, <?= $username; ?>! Кто-то, возможно Вы, воспользовался запросом нового пароля в <?= $app->params['name_abl']; ?>.

Чтобы получить пароль, перейдите по этой ссылке: <?= $app->getUrlManager()->createAbsoluteUrl(['auth/confirm', 'token' => $token]); ?>

Если Вы не запрашивали сброс пароля, то просто проигнорируйте это письмо.
