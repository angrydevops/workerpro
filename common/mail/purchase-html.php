<?php

use common\models\Order;

/**
 * @var \common\models\Purchase $model
 */

?>
<div>
	<p>Лицензия оплачена. Информация о заказе: </p>
	<dl>
		<dt><?= $model->getAttributeLabel('guid'); ?>: </dt>
		<dd><?= $model->guid; ?></dd>

		<dt><?= $model->getAttributeLabel('organization'); ?>: </dt>
		<dd><?= $model->organization; ?></dd>

		<dt><?= $model->getAttributeLabel('username'); ?>: </dt>
		<dd><?= $model->username; ?></dd>

		<dt><?= $model->getAttributeLabel('email'); ?>: </dt>
		<dd><?= $model->email; ?></dd>

		<dt><?= $model->getAttributeLabel('computers_count'); ?>: </dt>
		<dd><?= $model->computers_count; ?></dd>

		<dt><?= $model->getAttributeLabel('licence_type'); ?>: </dt>
		<dd><?= Order::$licenceTypes[$model->licence_type]; ?></dd>

		<?php if ($model->licence_type === 'cloud') { ?>
			<dt><?= $model->getAttributeLabel('subscription_type'); ?>: </dt>
			<dd><?= Order::$subscriptionTypes[$model->subscription_type]; ?></dd>
		<?php } ?>

		<dt><?= $model->getAttributeLabel('referal'); ?>: </dt>
		<dd><?= $model->referal; ?></dd>

		<dt><?= $model->getAttributeLabel('price'); ?>: </dt>
		<dd><?= $model->price; ?></dd>
	</dl>
</div>