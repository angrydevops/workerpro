<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $username string */
/* @var $token    string */

$app = Yii::$app;
?>
<div class="password-reset">
	<p>
		Здравствуйте, <?= Html::encode($username); ?>!
		Вас пригласили получить доступ в <a href="<?= Url::base(true); ?>"><?= $app->params['name_acc']; ?></a>.
	</p>

	<p>
		Чтобы получить пароль, <strong>перейдите по <a href="<?= $app->getUrlManager()->createAbsoluteUrl(['auth/confirm', 'token' => $token]); ?>">этой ссылке</a></strong>.<br/>
	</p>

	<p>Если Вы не имеете отношение к данному сайту, то просто проигнорируйте это письмо.</p>
</div>
