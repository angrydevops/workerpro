<?php

/**
 * @var \frontend\models\SupportForm $model
 */
?>

<?= $model->getAttributeLabel('username'); ?>: <?= $model->username; ?>

<?= $model->getAttributeLabel('email'); ?>: <?= $model->email; ?>

<?= $model->getAttributeLabel('type'); ?>: <?= $model->typesLabels[$model->type]; ?>

<?= $model->getAttributeLabel('text'); ?>:<?= $model->text; ?>

