<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $username string */
/* @var $password string */

$app = Yii::$app;
?>
Здравствуйте, <?= Html::encode($username); ?>!

Ваш пароль на <?= $app->params['brandName']; ?>: <?= $password; ?>
