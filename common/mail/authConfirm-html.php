<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $username string */
/* @var $password string */

$app = Yii::$app;
?>
<div class="password-reset">
	<p>Здравствуйте, <?= Html::encode($username); ?>!</p>

	<p>Ваш пароль на <a href="<?= Url::base(true); ?>"><?= $app->params['brandName']; ?></a>: <strong><?= $password; ?></strong></p>
</div>
