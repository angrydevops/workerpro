<?php

/**
 * @var \frontend\models\SupportForm $model
 */
?>
<div>
	<dl>
		<dt><?= $model->getAttributeLabel('username'); ?>: </dt>
		<dd><?= $model->username; ?></dd>

		<dt><?= $model->getAttributeLabel('email'); ?>: </dt>
		<dd><?= $model->email; ?></dd>

		<dt><?= $model->getAttributeLabel('type'); ?>:</dt>
		<dd><?= $model->typesLabels[$model->type]; ?></dd>

		<dt><?= $model->getAttributeLabel('text'); ?>:</dt>
		<dd><?= $model->text; ?></dd>
	</dl>
</div>
