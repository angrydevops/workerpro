<?php

/**
 * @var \common\models\Purchase $model
 */
use common\models\Order;

?>
Лицензия оплачена. Информация о заказе:

<?= $model->getAttributeLabel('guid'); ?>: <?= $model->guid; ?>

<?= $model->getAttributeLabel('organization'); ?>: <?= $model->organization; ?>

<?= $model->getAttributeLabel('username'); ?>: <?= $model->username; ?>

<?= $model->getAttributeLabel('email'); ?>: <?= $model->email; ?>

<?= $model->getAttributeLabel('computers_count'); ?>: <?= $model->computers_count; ?>

<?= $model->getAttributeLabel('licence_type'); ?>: <?= Order::$licenceTypes[$model->licence_type]; ?>

<?php if ($model->licence_type === 'cloud') { ?>
	<?= $model->getAttributeLabel('subscription_type'); ?>: <?= Order::$subscriptionTypes[$model->subscription_type]; ?>
<?php } ?>

<?= $model->getAttributeLabel('referal'); ?>: <?= $model->referal; ?>

<?= $model->getAttributeLabel('price'); ?>: <?= $model->price; ?>
