<?php

/**
 * @var \common\models\Order $model
 */
use common\helpers\Url;

?>

<?= $model->getAttributeLabel('organization'); ?>: <?= $model->organization; ?>

<?= $model->getAttributeLabel('username'); ?>: <?= $model->username; ?>

<?= $model->getAttributeLabel('email'); ?>: <?= $model->email; ?>

<?= $model->getAttributeLabel('computers_count'); ?>:<?= $model->computers_count; ?>

<?= $model->getAttributeLabel('licence_type'); ?>:<?= $model::$licenceTypes[$model->licence_type]; ?>

<?php if ($model->licence_type == 'cloud') { ?>
	<?= $model->getAttributeLabel('subscription_type'); ?>: <?= $model::$subscriptionTypes[$model->subscription_type]; ?>
<?php } ?>

<?= $model->getAttributeLabel('referal'); ?>:<?= $model->referal; ?>

<?= $model->getAttributeLabel('price'); ?>:<?= $model->price; ?>

Ссылка для оплаты: <?= Yii::$app->frontendUrlManager->createAbsoluteUrl([
	'purchase/index',
	'order' => $model->guid,
]); ?>
