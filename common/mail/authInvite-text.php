<?php

/* @var $this yii\web\View */
/* @var $username string */
/* @var $token    string */

$app = Yii::$app;
?>
Здравствуйте, <?= $username; ?>! Вас пригласили получить доступ в <?= $app->params['name_acc']; ?>.

Чтобы получить пароль, перейдите по этой ссылке: <?= $app->getUrlManager()->createAbsoluteUrl(['auth/confirm', 'token' => $token]); ?>

Если Вы не имеете отношение к данному сайту, то просто проигнорируйте это письмо.
