<?php

namespace common\helpers;

use yii\helpers\BaseHtml;

/**
 * Html is helper extends BaseHtml.
 *
 * @author Kostya M <primat@list.ru>
 */
class Html extends BaseHtml
{
	/**
	 * @param string $url
	 * @param integer|boolean $width
	 * @param integer|boolean $height
	 * @return string
	 */
	public static function thumbnail($url, $width = false, $height = false)
	{
		$options = [];
		if ($width) {
			$options['width'] = $width;
		}
		if ($height) {
			$options['height'] = $height;
		}
		return self::img(Url::thumbnail($url, $width, $height), $options);
	}

	/**
	 * @param array $items
	 * @param array $options
	 * @return string
	 */
	public static function itemsValue($items, $options = []) {
		if (empty($items)) {
			return '';
		}

		$attribute = isset($options['attribute']) ? $options['attribute'] : 'name';
		$tag = isset($options['tag']) ? $options['tag'] : 'li';
		$encode = !isset($options['encode']) || $options['encode'];
		$without_url = isset($options['without_url']) ? $options['without_url'] : null;
		$exclude = isset($options['exclude']) ? $options['exclude'] : array();

		$exists = [];
		$results = '';
		foreach ($items as $item) {
			$isBreadcrumb = !empty($item['isBreadcrumb']);
			$position = !empty($item['position']) ? $item['position'] : 1;
			$html = $item[$attribute];
			if (in_array($html, $exclude)) {
				continue;
			}

			if ($encode) {
				$html = static::encode($html);
			}

			$options = [];
			if (!empty($item['title']) && $item['title'] != $html) {
				$options['title'] = $item['title'];
			}

			if ($isBreadcrumb) {
				$html = Html::tag('span', $html, ['itemprop' => 'name']);
			}

			$without_url = !is_null($without_url) ? $without_url : empty($item['url']);
			if (in_array($html, $exists)) {
				if ($without_url) {
					continue;
				}
			} else {
				$exists[] = $html;
			}
			if (!$without_url) {
				if ($isBreadcrumb) {
					$options['itemprop'] = 'item';
				}
				$html = Html::a($html, $item['url'], $options);

				$options = [];
			}

			if ($isBreadcrumb) {
				$html .= '<meta itemprop="position" content="' . $position . '" />';

				$options['itemtype'] = 'http://schema.org/ListItem';
				$options['itemscope'] = '';
				$options['itemprop'] = 'itemListElement';
			}

			$results .= static::tag($tag, $html, $options);
		}
		return $results;
	}

	/**
	 * @param \yii\web\View $view
	 * @param string $content
	 * @param string $section
	 * @param string|null $iconFirst
	 * @param boolean $schemaVideo
	 * @param boolean $showAd
	 * @return string
	 */
	public static function replaceYoutube(&$view, $content, $section, $iconFirst = null, $schemaVideo = false, $showAd = true) {
		if (preg_match_all('/(<object[^>]+>)?(<param[^>]+>)*<(iframe|embed)[^>]+src="?(https?\:)?\/\/www\.youtube\.com\/[^\/]+\/([^?&#" ]+)[^>]+>[^<>]*(<\/embed>)?<\/(iframe|object)>/ui', $content, $matches, PREG_SET_ORDER) == 0) {
			return $content;
		}

		$section = mb_strtoupper($section, \Yii::$app->charset);

		$view->registerJs(<<<JS
			window.youtubeVideoType = '{$section}';
			window.isAllowPreroll = '{$showAd}';
JS
			, $view::POS_END);

		foreach ($matches as $i => $player) {
			$youtube_id = $player[5];
			$html_id = 'player' . $youtube_id;
			$thumbnail = empty($iconFirst) ? 'http://img.youtube.com/vi/' . $youtube_id . '/mqdefault.jpg' : $iconFirst;

			$content = str_replace(
				$player[0],
				'<img id="' . $html_id . '" class="video-js vjs-default-skin" width="600" height="338" alt="' .
					($schemaVideo ? $view->title : '') .
					'" src="' .
					$thumbnail .
					($schemaVideo ? '" itemprop="thumbnailUrl"/><span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject"><meta itemprop="contentUrl" content="' . $thumbnail . '"/><meta itemprop="width" content="600"/><meta itemprop="height" content="338"/><meta itemprop="name" content="' . $view->title . '"/></span><meta itemprop="embedUrl" content="http://www.youtube.com/embed/' . $youtube_id : '') .
					'"/>',
				$content
			);
			// шема и спец. иконка могут быть только у первого видео
			$iconFirst = null;
			$schemaVideo = false;
		}

		return $content;
	}

	/**
	 * @param string $content
	 * @param string $name
	 * @param boolean $with_itemprop
	 * @return string
	 */
	public static function schemaImage($content, $name, $with_itemprop = false) {
		if (preg_match_all('/(<a)([^>]+class="[^"]*image-big[^"]*"[^>]+href="([^"]+?(\-(\d{3,4})x(\d{3,4}))?+(\.jpe?g|\.png|\.gif)?+)"[^>]*>[ ]*<img)(([^>]+style="([^"]*)")?[^>]+alt="([^"]*)"[^>]+width="([^"]*)"[^>]*>[ ]*<\/a>)/ui', $content, $matches, PREG_SET_ORDER) == 0) {
			return $content;
		}

		foreach ($matches as $image) {
			$imageSchema = $image[1] . ' itemprop="contentUrl"' . $image[2] . ' itemprop="thumbnail"' . $image[8] . '<meta itemprop="name" content="' . static::encode($name) . '"/>';
			if (!empty($image[11])) {
				$imageSchema .= '<meta itemprop="description" content="' . $image[11] . '"/>';
			}
			if (!empty($image[5])) {
				$imageSchema .= '<meta itemprop="width" content="' . $image[5] . '"/>';
			}
			if (!empty($image[6])) {
				$imageSchema .= '<meta itemprop="height" content="' . $image[6] . '"/>';
			}

			$imageSchema = ' itemscope itemtype="http://schema.org/ImageObject"' . ($with_itemprop ? ' itemprop="image"' : '') . '>' . $imageSchema . '</figure>';
			$imageAttributeValue = $image[12];
			if (!empty($image[12])) {
				if ($imageAttributeValue >= 195 && $imageAttributeValue <= 200) {
					$imageAttributeValue = 'image33p';
				} else if ($imageAttributeValue >= 290 && $imageAttributeValue <= 300) {
					$imageAttributeValue = 'image49p';
				} else if ($imageAttributeValue == 484) {
					$imageAttributeValue = 'image100pm';
				} else if ($imageAttributeValue == 400) {
					$imageAttributeValue = 'image75p';
				} else if ($imageAttributeValue >= 595 && $imageAttributeValue <= 600) {
					$imageAttributeValue = 'image100p';
				} else {
					$imageAttributeValue = false;
				}
				if ($imageAttributeValue) {
					$imageSchema = ' class="' . $imageAttributeValue . '"' . $imageSchema;
				}
			}
			if (!empty($image[9])) {
				$imageSchema = $image[9] . $imageSchema;
			}
			$imageSchema = '<figure' . $imageSchema;

			$content = str_replace(
				$image[0],
				$imageSchema,
				$content
			);
		}
		return $content;
	}

	/**
	 * @param string $content
	 * @param string $itemprop
	 * @return string
	 */
	public static function schemaAddItempropToImages($content, $itemprop = 'image') {
		if (preg_match_all('/<img(?! itemprop="thumbnail")([^>]+)>/ui', $content, $matches, PREG_SET_ORDER) == 0) {
			return $content;
		}

		foreach ($matches as $image) {
			$content = str_replace(
				$image[1],
				' itemprop="' . $itemprop . '"' . $image[1],
				$content
			);
		}
		return $content;
	}

	/**
	 * @inheritdoc
	 */
	public static function activeListInput($type, $model, $attribute, $items, $options = [])
	{
		return parent::activeListInput($type, $model, $attribute, $items, $options);
	}

	/**
	 * @param string $name
	 * @param string|array $selection
	 * @param array $items
	 * @param array $options
	 * @return string
	 */
	public static function checkboxListWithHeading($name, $selection, $items = [], $options = [])
	{
		if (substr($name, -2) !== '[]') {
			$name .= '[]';
		}

		$formatter = isset($options['item']) ? $options['item'] : null;
		$headingTag = isset($options['headingTag']) ? $options['headingTag'] : 'legend';
		$separator = isset($options['separator']) ? $options['separator'] : '';
		$itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
		$encode = !isset($options['encode']) || $options['encode'];

		$lines = [];
		$index = 0;
		foreach ($items as $groupName => $item) {
			$lines[] = Html::tag($headingTag,  $groupName);
			foreach ($item as $value => $label) {
				$checked = $selection !== null &&
					(!is_array($selection) && !strcmp($value, $selection)
						|| is_array($selection) && in_array($value, $selection));
				if ($formatter !== null) {
					$lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
				} else {
					$lines[] = static::checkbox($name, $checked, array_merge($itemOptions, [
						'value' => $value,
						'label' => $encode ? static::encode($label) : $label,
					]));
				}
				$lines[] = $separator;
				$index++;
			}
		}

		if (isset($options['unselect'])) {
			$name2 = substr($name, -2) === '[]' ? substr($name, 0, -2) : $name;
			$hidden = static::hiddenInput($name2, $options['unselect']);
		} else {
			$hidden = '';
		}

		$tag = isset($options['tag']) ? $options['tag'] : 'div';
		unset($options['tag'], $options['unselect'], $options['encode'], $options['separator'], $options['item'], $options['itemOptions'], $options['headingTag']);

		return $hidden . static::tag($tag, implode("\n", $lines), $options);
	}

	/**
	 * @return string
	 */
	public static function getFeedStyle() {
		if (array_key_exists('feed_style', $_COOKIE)) {
			setcookie('feed_style', null, time() - 300);
		}

		if (!array_key_exists('feedStyle', $_COOKIE)) {
			return 'tiles';
		}
		if ($_COOKIE['feedStyle'] == 'list' || $_COOKIE['feedStyle'] == 'tiles') {
			return $_COOKIE['feedStyle'];
		}
		return 'tiles';
	}

	/**
	 * @return string
	 */
	public static function getFeedStyleTitle() {
		return array_key_exists('feedStyle', $_COOKIE) ? (self::getFeedStyle() == 'list' ? 'Лента' : 'Плитка') : 'Автоматический';
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public static function toText($string) {
		$string = preg_replace('/<\/(h[1-6]|blockquote|div|p|ol|ul|li|table|tbody|tr|th|td)>/iu', '</$1> ', $string);
		$string = strip_tags($string);
		$string = preg_replace('/ {2,}/', ' ', $string);
		return html_entity_decode($string, ENT_QUOTES, \Yii::$app->charset);
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public static function encodeYandex($string) {
		return htmlspecialchars(str_replace("\r", '', $string), ENT_QUOTES | ENT_XML1, \Yii::$app->charset, false);
	}

	/**
	 * @param array|null $event
	 * @return string
	 */
	public static function eventElement($event)
	{
		if (empty($event)) {
			return '';
		}

		$content = $event['content'];

		$resourceType = $event['resource_type'];
		if ($resourceType == 'ForumThread' || $resourceType == 'Post') {
			$svgId = 'event-post';
		} else if ($resourceType == 'ForumPost' || $resourceType == 'Comment') {
			$svgId = 'event-comment';
		} else if ($resourceType == 'EventText' || $resourceType == 'EventImage' || $resourceType == 'EventYoutube') {
			$svgId = 'event-for-stream';
		} else if ($resourceType == 'ForumPostLike') {
			$svgId = 'event-like';
		} else if ($resourceType == 'PostPopular') {
			$svgId = 'event-popular';
		} else {
			$svgId = 'event-profile';
		}
		if (isset($svgId)) {
			$content = <<<HTML
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
					<use xlink:href="#{$svgId}"/>
				</svg>
HTML
				. $content;
		}

		$options = [];
		if (!empty($event['title'])) {
			$options['title'] = $event['title'];
		}
		if ($resourceType == 'EventYoutube' || $resourceType == 'EventImage') {
			$options['target'] = '_blank';
			$options['class'] = 'event-narrow';

			if ($resourceType == 'EventYoutube') {
				$contentWide = '<iframe class="event-wide" width="600" height="338" src="http://www.youtube.com/embed/' . Url::getYoutubeId($event['url']) . '" frameborder="0" scrolling="no"></iframe>';
			} else if ($resourceType == 'EventImage') {
				$contentWide = $options;
				$contentWide['class'] = 'event-wide';

				$contentWide = Html::a(
					'<img src="' . $event['url'] . '" alt="' . Html::encode($event['content']) . '"/>',
					$event['url'],
					$contentWide
				);
			}
		}
		$options['id'] = 'event-' . $event['id'];

		if (empty($event['url'])) {
			$content = Html::tag('span', $content, $options);
		} else {
			$content = Html::a($content, $event['url'], $options);
		}

		if (isset($contentWide)) {
			return $content . $contentWide;
		} else {
			return $content;
		}
	}

	/**
	 * @param \common\models\Folder[]|\common\models\Tag[]|\common\models\Post[]|\common\models\Link[]$items
	 * @return array
	 */
	public static function prepareDataForSelectize($items) {
		$result = [];
		foreach ($items as $item) {
			$result[] = [
				'id'   => $item->id,
				'url'  => $item->hasMethod('getUrl') ? $item->getUrl() : $item->url,
				'name' => $item->name,
				'icon' => $item->hasAttribute('icon') ? $item->icon : '',
			];
		}

		return $result;
	}

	/**
	 * @param array $tree
	 * @param string $value
	 * @param string $label
	 * @return array
	 */
	public static function treeForDropDownList($tree, $value = 'id', $label = 'name') {
		$result = [];

		foreach ($tree as $item) {
			$path_pos = strpos($item['path'], '.');

			if ($path_pos !== false) {
				$level = mb_substr_count($item['path'], '.', \Yii::$app->charset);
				$item[$label] = str_repeat('—', $level) . $item[$label];
			}

			$result[$item[$value]] = $item[$label];
		}

		return $result;
	}
}
