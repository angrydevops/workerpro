<?php

namespace common\helpers;

use Yii;
use yii\helpers\BaseUrl;

/**
 * Url is helper extends BaseUrl.
 *
 * @author Kostya M <primat@list.ru>
 */
class Url extends BaseUrl
{
	/**
	 * @param string $url
	 * @param integer|boolean $width
	 * @param integer|boolean $height
	 * @return string
	 */
	public static function thumbnail($url, $width = false, $height = false)
	{
		if ($width || $height) {
			$suffix = '-' . ($width ? $width : '0') . 'x' . ($height ? $height : '0');

			$pos = strrpos($url, '.');
			if ($pos === false) {
				$url .= $suffix;
			} else {
				$url = substr($url, 0, $pos) . $suffix . substr($url, $pos);
			}
		}
		return Yii::$app->params['urlMedia'] . $url;
	}

	/**
	 * @param string|boolean|null $url true if check is not needed
	 * @return string|boolean
	 */
	public static function imageDomain($url = true)
	{
		if (empty($url)) {
			return false;
		}

		$url = \Yii::$app->params['urlMedia'];
		if (substr($url, 0, 4) == 'http') {
			return '';
		}

		$domain = 'http:';
		if (substr($url, 0, 2) != '//') {
			$domain .= '//' . \Yii::$app->params['frontendDomain'];
		}
		return $domain;
	}

	/**
	 * @param string $urlFull
	 * @return string|boolean
	 */
	public static function getSubUrlFromFull($urlFull) {
		$urlMedia = \Yii::$app->params['urlMedia'];
		if (strpos($urlFull, $urlMedia) === 0) {
			return substr($urlFull, strlen($urlMedia));
		}

		if (substr($urlMedia, 0, 5) == 'http:') {
			$urlMedia = substr($urlMedia, 5);

			if (strpos($urlFull, $urlMedia) === 0) {
				return substr($urlFull, strlen($urlMedia));
			}
		}

		if (substr($urlFull, 0, 5) == 'http:') {
			$urlFull = substr($urlFull, 5);

			if (strpos($urlFull, $urlMedia) === 0) {
				return substr($urlFull, strlen($urlMedia));
			}
		}

		if (substr($urlFull, 0, 2) == '//') {
			$pos = strpos($urlFull, '/', 2);
			if ($pos === false) {
				return false;
			}
			$urlFull = substr($urlFull, $pos);
		}

		if (strpos($urlFull, $urlMedia) === 0) {
			return substr($urlFull, strlen($urlMedia));
		}

		return false;
	}

	/**
	 * @param array $route
	 * @param boolean $scheme
	 * @return string
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\base\InvalidParamException
	 */
	public static function toRouteFrontend($route, $scheme = false) {
		if (Yii::$app->has('frontendUrlManager')) {
			$frontendUrlManager = Yii::$app->get('frontendUrlManager');
			return $scheme ?
				$frontendUrlManager->createAbsoluteUrl($route) :
				$frontendUrlManager->createUrl($route);
		}

		return self::toRoute($route, $scheme);
	}

	/**
	 * @param string $url
	 * @return string|boolean
	 */
	public static function getYoutubeId($url) {
		$charset = Yii::$app->charset;

		$pos = mb_strpos($url, 'v=', null, $charset);
		if ($pos === false) {
			$pos = mb_strpos($url, 'v/', null, $charset);
		}
		if ($pos === false) {
			return false;
		}

		$url = mb_substr($url, $pos + 2);
		if (preg_match('/^[0-9a-zA-Z_-]++/', $url, $url) == 0) {
			return false;
		}

		return $url[0];
	}
}
