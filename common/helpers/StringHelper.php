<?php

namespace common\helpers;

use Yii;
use yii\helpers\BaseStringHelper;

/**
 * StringHelper is helper extends BaseStringHelper.
 *
 * @author Kostya M <primat@list.ru>
 */
class StringHelper extends BaseStringHelper
{

	const PUNCTUATION_MARKS = '-‑—.…,:;!?"\'<>()«»„“';

	const WHITESPACES = "  \t\n\r\0";

	/**
	 * Multibyte trim.
	 *
	 * @param string $string
	 * @param string $char_list
	 * @return string
	 */
	public static function mb_trim($string, $char_list = "  \t\n\r\0"){
		$char_list = preg_quote($char_list);
		return preg_replace('/^[' . $char_list . ']*(?U)(.*)[' . $char_list . ']*$/u', '\\1', $string);
	}

	/**
	 * @param string|array $search
	 * @param string|array $replace
	 * @param string|array $subject
	 * @param int $count
	 * @return string|boolean
	 */
	public static function mb_str_replace($search, $replace, $subject, &$count = 0) {
		if (!is_array($search) && is_array($replace)) {
			return false;
		}
		if (is_array($subject)) {
			// call mb_replace for each single string in $subject
			foreach ($subject as &$string) {
				$string = self::mb_str_replace($search, $replace, $string, $c);
				$count += $c;
			}
		} elseif (is_array($search)) {
			if (!is_array($replace)) {
				foreach ($search as &$string) {
					$subject = self::mb_str_replace($string, $replace, $subject, $c);
					$count += $c;
				}
			} else {
				$n = max(count($search), count($replace));
				while ($n--) {
					$subject = self::mb_str_replace(current($search), current($replace), $subject, $c);
					$count += $c;
					next($search);
					next($replace);
				}
			}
		} else {
			$parts = mb_split(preg_quote($search), $subject);
			$count = count($parts) - 1;
			$subject = implode($replace, $parts);
		}
		return $subject;
	}

	/**
	 * Truncates a string to the number of characters specified.
	 *
	 * @param string $string The string to truncate.
	 * @param integer $length How many characters from original string to include into truncated string.
	 * @param string $suffix String to append to the end of truncated string.
	 * @param string $encoding The charset to use, defaults to charset currently used by application.
	 * @return string the truncated string.
	 */
	public static function truncateAtWordEnd($string, $length, $suffix = '...', $encoding = null)
	{
		if (mb_strlen($string, $encoding ?: Yii::$app->charset) > $length) {
			$string = mb_substr($string, 0, $length, $encoding ?: Yii::$app->charset);

			$marksWordEnd = self::WHITESPACES . self::PUNCTUATION_MARKS;
			$marksWordEndQuoted = preg_quote($marksWordEnd);
			$string = preg_replace('/[' . $marksWordEndQuoted . '][^' . $marksWordEndQuoted . ']*?$/u', '', $string, 1);

			return self::mb_trim($string, $marksWordEnd) . $suffix;
		} else {
			return $string;
		}
	}

	/**
	 * @param int $minLength
	 * @param int $maxLength
	 * @param bool $useNumbers
	 * @return string
	 */
	public static function generateRememberedString($minLength = 6, $maxLength = 8, $useNumbers = true) {
		$consonant = ['b', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'y', 'z'];
		$vowel     = ['a', 'e', 'i', 'o', 'u'];
		$consonantLast = count($consonant) - 1;
		$vowelLast     = count($vowel) - 1;

		$password = '';
		for ($i = 0, $count = rand($minLength, $maxLength); $i < $count; ++$i) {
			if ($i % 2 == 0) {
				$password .= $consonant[rand(0, $consonantLast)];
			} else {
				$password .=     $vowel[rand(0, $vowelLast)];
			}
		}

		if ($useNumbers) {
			$number = range(0, 9);
			$numberLast = count($number) - 1;

			$minLength = round($minLength / 3);
			$maxLength = round($maxLength / 2);

			for ($i = 0, $count = rand($minLength, $maxLength); $i < $count; ++$i) {
				$password .= $number[rand(0, $numberLast)];
			}
		}

		return $password;
	}

	/**
	 * @param string $str
	 * @return string
	 */
	public static function mb_lcfirst($str) {
		$fc = mb_strtolower(mb_substr($str, 0, 1));
		return $fc.mb_substr($str, 1);
	}
}
