<?php

namespace common\log;

use Yii;
use \yii\caching\Cache;

class EmailTarget extends \yii\log\EmailTarget
{
	/**
	 * @var integer $frequency frequency in seconds
	 * default: 1 day
	 */
	public $frequency = 86400;

	/**
	 * @var Cache|string the cache object or the ID of the cache application component that
	 * is used to cache the table metadata.
	 */
	public $schemaCache = 'cache';

	/**
	 * @var array of error categories that won't be sent if last success try was less than hour ago
	 */
	public $skipCategoriesByCacheTimestamp = [];

	/**
	 * @var mixed
	 */
	protected $_cacheKey;

	/**
	 * Returns the cache key for the specified log target.
	 * @return mixed the cache key
	 */
	protected function getCacheKey()
	{
		if (empty($this->_cacheKey)) {
			$this->_cacheKey = [
				__CLASS__,
				$this->getLevels(),
				implode('_', $this->categories),
				implode('_', $this->except),
			];
		}

		return $this->_cacheKey;
	}

	/**
	 * @param mixed $value
	 */
	public function setCacheKey($value) {
		$this->_cacheKey = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function export()
	{
		/* @var $cache Cache */
		$cache = is_string($this->schemaCache) ? Yii::$app->get($this->schemaCache, false) : $this->schemaCache;
		if ($cache instanceof Cache) {
			if (!empty($this->skipCategoriesByCacheTimestamp)) {
				foreach ($this->messages as $i => $message) {
					$category = $message[2];
					if (array_search($category, $this->skipCategoriesByCacheTimestamp) !== false) {
						$timestamp = $cache->get($category);
						if (!empty($timestamp) && $message[3] - $timestamp < 3600) {
							unset($this->messages[$i]);
						}
					}
				}
			}

			if (empty($this->messages)) {
				return;
			}

			$cacheKey = $this->getCacheKey();

			$time = time();
			$lastSent = $cache->get($cacheKey);
			if (!empty($lastSent) && $time - $lastSent < $this->frequency) {
				return;
			}
			$cache->set($cacheKey, $time);
		}

		parent::export();
	}
}