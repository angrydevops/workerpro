<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);
$isWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
return [
	'id' => 'worker-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'console\controllers',
	'components' => [
		'log' => [
			'targets' => [
				'sql' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/sql.log',
					'rotateByCopy' => $isWin,
					'levels' => ['info'],
					'categories' => ['yii\db\Command*'],
					'logVars' => [],
				],
			],
		],
	],
	'params' => $params,
];
