<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\console\Exception;

class DbController extends Controller
{

	public static function executeSqlConsole($command)
	{
		$dsn = Yii::$app->getDb()->dsn;
		$dsn = explode(';', substr($dsn, strpos($dsn, ':') + 1));

		$dsnByKey = [];
		foreach ($dsn as $item) {
			$item = explode('=', $item);

			$dsnByKey[$item[0]] = $item[1];
		}

		$command = ' -c "' . str_replace('"', '\"', $command) . '"';
		if (!empty($dsnByKey['host'])) {
			$command = ' -h ' . $dsnByKey['host'] . $command;
		}
		$command ='psql -U postgres -d ' . $dsnByKey['dbname'] . $command;
		$streams = [
			['pipe', 'r'], //stdin
			['pipe', 'w'], //stdout
			['pipe', 'w']  //stderr
		];
		$process = proc_open($command, $streams, $pipes);
		if (!is_resource($process)) {
			throw new Exception('Cannot open process: ' . $command);
		} else {
			list(, $stdout, $stderr) = $pipes;
			$error = stream_get_contents($stderr);
			fclose($stderr);
			if (strlen($error) > 0) {
				throw new Exception('Process error: ' . $error);
			} else {
				$output = stream_get_contents($stdout);
				fclose($stdout);
				$returnCode = proc_close($process);
				if ($returnCode === -1) {
					throw new Exception('Process was completed incorrectly: ' . $output);
				} else {
					return [
						$returnCode,
						$output
					];
				}
			}
		}
	}
}
