<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\ArrayHelper;

class MediaController extends Controller
{
    /**
     * Deletes unused images
     */
    public function actionDeleteUnused()
    {
        $usedInGallery = ArrayHelper::getColumn(\Yii::$app->params['galleryPhotosFromMedia'], 'image');
    }
}
