<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\console\Exception;

class OptimizeController extends Controller
{
	/**
	 * @param string $code
	 * @return string
	 */
	protected function _clearCodeComment($code) {
		if (strpos($code, '/*') === 0) {
			$pos = mb_strpos($code, '*/');
			if ($pos > 0) {
				$code = ltrim(mb_substr($code, $pos + 2));
			}
		}
		return $code;
	}

	public function actionCss($inputFile, $outputFile = null)
	{
		if (!is_string($outputFile)) {
			$outputFile = substr($inputFile, 0, -3) . 'min.css';
		}

		$scss = new \Leafo\ScssPhp\Compiler();
		$scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');
		$result = $scss->compile(file_get_contents($inputFile), $inputFile);

		$result = $this->_clearCodeComment($result);

		file_put_contents($outputFile, $result);
		echo 'completed.';
	}

	public function actionJs($inputFile, $outputFile = null)
	{
		if (!is_string($outputFile)) {
			$outputFile = substr($inputFile, 0, -2) . 'min.js';
		}
		try {
			$options = [
				'js_code' => file_get_contents($inputFile),
				'compilation_level' => 'SIMPLE_OPTIMIZATIONS',
				'output_format' => 'text',
				'output_info' => 'compiled_code',
				'language' => 'ECMASCRIPT5'
			];
			$options = [
				'http' => [
					'header' => "Content-type: application/x-www-form-urlencoded\r\n",
					'method' => 'POST',
					'content' => http_build_query($options),
				],
			];

			$context = stream_context_create($options);
			$result = file_get_contents('http://closure-compiler.appspot.com/compile', false, $context);
			if (is_int($result) || empty($result)) {
				throw new Exception('Closure-compiler failed.');
			}

			$result = $this->_clearCodeComment($result);

			file_put_contents($outputFile, $result);
		} catch (Exception $e) {
			echo $e->getMessage(), PHP_EOL;
			if (copy($inputFile, $outputFile) === false) {
				throw new Exception('File to destination path is not saved.');
			}
		}

		echo 'completed.';
	}
}
