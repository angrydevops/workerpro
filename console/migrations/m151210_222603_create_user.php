<?php

use yii\db\Migration;
use common\models\User;

class m151210_222603_create_user extends Migration
{
    public function up()
    {
        $user = new User();
        $user->role = 'writer';
        $user->title = 'Константин Мутугуллин';
        $user->email = 'iamf13@ya.ru';
        $user->setScenario('administrator');
        return $user->save(false);
    }

    public function down()
    {
        $user = User::findOne(['email' => 'iamf13@ya.ru']);
        return $user->delete();
    }
}
