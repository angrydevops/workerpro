<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_193234_add_purchase_indexes extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'purchases';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createIndex(
            $this->itemName . '_idx_guid',
            $tableName,
            'guid',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_email',
            $tableName,
            'email',
            'hash'
        );

        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_referal
  ON {$this->itemName}
  USING hash
  (referal)
  WHERE referal IS NOT NULL;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_order_id
  ON {$this->itemName}
  USING hash
  (order_id)
  WHERE order_id IS NOT NULL;
SQL
        )->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $columns = [
            'guid',
            'email',
            'referal',
            'order_id'
        ];

        foreach ($columns as $column) {
            $this->dropIndex($this->itemName . '_idx_' . $column, '{{%' . $this->itemName . '}}');
        }
    }
}
