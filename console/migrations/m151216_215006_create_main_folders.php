<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_215006_create_main_folders extends Migration
{
    public function safeUp()
    {
        $folders = [
            [
                'slug' => 'help',
                'name' => 'Помощь',
                'title_seo' => 'Как пользоваться программой учета рабочего времени',
            ],
            [
                'slug' => 'articles',
                'name' => 'Статьи',
                'title_seo' => 'Как повысить продуктивность работы сотрудников',
            ],
        ];

        echo 'Creating folders...';

        foreach ($folders as $data) {
            $folder = new \common\models\Folder();
            $folder->setScenario('administrator');

            $folder->slug = $data['slug'];
            $folder->name = $data['name'];
            $folder->title = $data['name'];
            $folder->title_seo = $data['title_seo'];

            if (!$folder->save()) {
                echo 'Error: folder not saved: ', \yii\helpers\VarDumper::dumpAsString($folder->getErrors()), PHP_EOL;
                return false;
            }
        }

        echo 'done.', PHP_EOL;

        Yii::$app->cacheCommon->flush();
    }

    public function down()
    {
        echo "m151216_215006_create_main_folders cannot be reverted.\n";

        return false;
    }
}
