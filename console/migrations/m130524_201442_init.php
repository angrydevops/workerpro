<?php

use yii\db\Migration;
use console\controllers\DbController;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        DbController::executeSqlConsole('CREATE EXTENSION ltree');
        DbController::executeSqlConsole('CREATE EXTENSION pgcrypto');


        $this->db->createCommand(<<<SQL
CREATE TYPE user_roles AS ENUM ('client', 'writer');
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
ALTER TYPE user_roles OWNER TO worker;
SQL
        )->execute();


        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION blowfish(text)
  RETURNS text AS
\$BODY\$
	SELECT substr(crypt($1, '$2a$08$.t/QsvjgquCzOieDCybNHe'), 30);
\$BODY\$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
ALTER FUNCTION blowfish(text)
  OWNER TO worker;
SQL
        )->execute();


        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'role' => 'user_roles NOT NULL DEFAULT \'client\'',
            'title' => $this->string(95)->notNull(),
            'avatar' => $this->string(255),
            'created' => 'timestamp without time zone NOT NULL',
            'updated' => 'timestamp without time zone NOT NULL',
            'first_name' => $this->string(63),
            'last_name' => $this->string(63),
            'email' => $this->string(255),
            'password_hash' => $this->string(255),
            'auth_key' => $this->string(32),
        ]);

        $this->db->createCommand(<<<SQL
ALTER TABLE users
  OWNER TO worker;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX users_idx_blowfish_id_and_updated_and_password_hash
  ON users
  USING btree
  (blowfish(((id::text || date_part('epoch'::text, updated)) || '_'::text) || COALESCE(password_hash, ''::character varying)::text))
  WHERE password_hash IS NOT NULL;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX users_idx_email
  ON users
  USING btree
  (email)
  WHERE email IS NOT NULL;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX users_idx_role
  ON users
  USING hash
  (role);
SQL
        )->execute();


        $this->createTable('{{%folders}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(31)->notNull(),
            'name' => $this->string(31)->notNull(),
            'title' => $this->string(95),
            'title_seo' => $this->string(255),
            'description' => $this->string(1023),
            'updated' => 'timestamp without time zone NOT NULL',
            'path' => 'ltree NOT NULL',
        ]);

        $this->db->createCommand(<<<SQL
ALTER TABLE folders
  OWNER TO worker;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX folders_idx_name
  ON folders
  USING hash
  (name);
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX folders_idx_path
  ON folders
  USING gist
  (path);
SQL
        )->execute();


        $this->createTable('{{%posts}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(31)->notNull(),
            'name' => $this->string(31)->notNull(),
            'title_seo' => $this->string(255),
            'description' => $this->string(1023)->notNull(),
            'icon' => $this->string(255),
            'content' => $this->text(),
            'published' => 'timestamp without time zone NOT NULL',
            'updated' => 'timestamp without time zone NOT NULL',
            'folder_id' => $this->integer(),
        ]);

        $this->addForeignKey('posts_fk_folder', 'posts', 'folder_id', 'folders', 'id', 'RESTRICT', 'RESTRICT' );

        $this->db->createCommand(<<<SQL
ALTER TABLE posts
  OWNER TO worker;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX posts_idx_folder
  ON posts
  USING hash
  (folder_id);
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE INDEX posts_idx_published
  ON posts
  USING btree
  (published)
  WHERE published IS NOT NULL;
SQL
        )->execute();
    }

    public function safeDown()
    {
        $this->dropTable('{{%posts}}');
        $this->dropTable('{{%folders}}');
        $this->dropTable('{{%users}}');

        $this->db->createCommand(<<<SQL
DROP FUNCTION IF EXISTS blowfish(text, text);
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
DROP TYPE IF EXISTS user_roles;
SQL
        )->execute();

        DbController::executeSqlConsole('DROP EXTENSION IF EXISTS ltree');
        DbController::executeSqlConsole('DROP EXTENSION IF EXISTS pgcrypto');
    }
}
