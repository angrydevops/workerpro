<?php

use yii\db\Schema;
use yii\db\Migration;

class m160107_230259_change_uploaded_files_mode extends Migration
{
    public function up()
    {
        $this->chmodInDir(Yii::getAlias('@media'));
    }

    protected function chmodInDir($dir) {
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if (mb_strpos($file, '.') === 0) {
                    continue;
                }

                $fullPath = $dir . DIRECTORY_SEPARATOR . $file;

                if (!is_dir($fullPath)) {
                    chmod($fullPath, 0644);
                } else {
                    $this->chmodInDir($fullPath);
                }
            }

            closedir($handle);
        }
    }

    public function down()
    {
        echo "m160107_230259_change_uploaded_files_mode cannot be reverted.\n";

        return false;
    }
}
