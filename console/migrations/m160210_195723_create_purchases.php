<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_195723_create_purchases extends Migration
{
    public $itemName = 'purchases';

    public function safeUp()
    {
        $this->createTable('{{%' . $this->itemName . '}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'hash' => $this->string(32),
            'guid' => 'uuid DEFAULT gen_random_uuid()',
            'organization' => $this->string(95)->notNull(),
            'username' => $this->string(95)->notNull(),
            'email' => $this->string(255)->notNull(),
            'licence_type' => 'licence_types NOT NULL',
            'subscription_type' => 'subscription_types',
            'computers_count' => $this->integer()->notNull(),
            'referal' => $this->string(31),
            'price' => $this->float()->notNull(),
            'summary' => 'json NOT NULL DEFAULT \'{}\''
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
