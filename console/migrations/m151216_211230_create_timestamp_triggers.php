<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_211230_create_timestamp_triggers extends Migration
{
    public function safeUp()
    {
        echo 'Creating triggers and functions', PHP_EOL;
        $sql = <<<SQL
CREATE OR REPLACE FUNCTION posts_updated_timestamp()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'UPDATE') THEN
        IF (NEW.slug              = OLD.slug AND
            NEW.name              = OLD.name AND
            COALESCE(NEW.title_seo, '')              = COALESCE(OLD.title_seo, '') AND
            NEW.description       = OLD.description AND
            COALESCE(NEW.icon,    '')                = COALESCE(OLD.icon,    '') AND
            COALESCE(NEW.content, '')                = COALESCE(OLD.content, '') AND
            COALESCE(NEW.published, '1991-12-26')    = COALESCE(OLD.published, '1991-12-26') AND
            NEW.folder_id    = OLD.folder_id
        ) THEN
            RETURN NEW;
        END IF;
    END IF;

    NEW.updated := NOW();
    RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE TRIGGER "posts_timestamp_before_insert_or_update"
BEFORE INSERT OR UPDATE ON "posts"
FOR EACH ROW
EXECUTE PROCEDURE posts_updated_timestamp();
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE OR REPLACE FUNCTION users_created_updated_timestamp()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.created := NOW();
    ELSE
        IF (NEW.role   = OLD.role AND
            NEW.title  = OLD.title AND
            NEW.avatar = OLD.avatar AND
            NEW.first_name = OLD.first_name AND
            NEW.last_name = OLD.last_name AND
            NEW.email = OLD.email
        ) THEN
            RETURN NEW;
        END IF;
    END IF;

    NEW.updated := NOW();
    RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE TRIGGER "users_timestamp_before_insert"
BEFORE INSERT OR UPDATE ON "users"
FOR EACH ROW
EXECUTE PROCEDURE users_created_updated_timestamp();
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE OR REPLACE FUNCTION folders_updated_timestamp()
RETURNS TRIGGER AS
$$
BEGIN
	IF (TG_OP = 'UPDATE') THEN
		IF (NEW.slug        = OLD.slug AND
			NEW.name        = OLD.name AND
			NEW.description = OLD.description AND
			NEW.path        = OLD.path AND
			COALESCE(NEW.title,     '') = COALESCE(OLD.title,     '') AND
			COALESCE(NEW.title_seo, '') = COALESCE(OLD.title_seo, '')
		) THEN
			RETURN NEW;
		END IF;
	END IF;

    NEW.updated := NOW();
    RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE TRIGGER "folders_timestamp_before_insert_or_update"
BEFORE INSERT OR UPDATE ON "folders"
FOR EACH ROW
EXECUTE PROCEDURE folders_updated_timestamp();
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function safeDown()
    {
        echo 'Deleting triggers and functions', PHP_EOL;
        $this->db->createCommand('DROP TRIGGER posts_timestamp_before_insert_or_update ON posts')->execute();
        $this->db->createCommand('DROP TRIGGER users_timestamp_before_insert ON "users"')->execute();
        $this->db->createCommand('DROP TRIGGER folders_timestamp_before_insert_or_update ON folders')->execute();
        $this->db->createCommand('DROP FUNCTION posts_updated_timestamp()')->execute();
        $this->db->createCommand('DROP FUNCTION users_created_updated_timestamp()')->execute();
        $this->db->createCommand('DROP FUNCTION folders_updated_timestamp()')->execute();
    }
}
