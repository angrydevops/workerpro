<?php

use console\controllers\DbController;
use yii\db\Migration;

class m151229_205929_create_intarray_extention extends Migration
{
    public function up()
    {
        DbController::executeSqlConsole('CREATE EXTENSION intarray');
    }

    public function down()
    {
        DbController::executeSqlConsole('DROP EXTENSION IF EXISTS intarray');
    }
}
