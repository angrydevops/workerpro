<?php

use yii\db\Migration;

class m160204_200250_create_orders extends Migration
{
    public function safeUp()
    {
        $owner = Yii::$app->db->username;
        $this->db->createCommand(<<<SQL
CREATE TYPE licence_types AS ENUM ('cloud', 'local');
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
ALTER TYPE licence_types OWNER TO {$owner};
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE TYPE subscription_types AS ENUM ('threeMonths', 'sixMonths', 'year');
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
ALTER TYPE subscription_types OWNER TO {$owner};
SQL
        )->execute();

        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'guid' => 'uuid DEFAULT gen_random_uuid()',
            'organization' => $this->string(95)->notNull(),
            'username' => $this->string(95)->notNull(),
            'email' => $this->string(255)->notNull(),
            'licence_type' => 'licence_types NOT NULL',
            'subscription_type' => 'subscription_types',
            'computers_count' => $this->integer()->notNull(),
            'referal' => $this->string(31),
            'price' => $this->float()->notNull(),
            'created' => 'timestamp without time zone NOT NULL',
        ]);

        $this->db->createCommand(<<<SQL
ALTER TABLE orders
  OWNER TO {$owner};
SQL
        )->execute();

        $sql = <<<SQL
CREATE OR REPLACE FUNCTION orders_created_timestamp()
RETURNS TRIGGER AS
$$
BEGIN
    NEW.created := NOW();
    RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE TRIGGER "orders_timestamp_before_insert"
BEFORE INSERT ON "orders"
FOR EACH ROW
EXECUTE PROCEDURE orders_created_timestamp();
SQL;
        $this->db->createCommand($sql)->execute();

        $this->createIndex(
            'orders_idx_guid',
            '{{%orders}}',
            'guid',
            'hash'
        );

        $this->createIndex(
            'orders_idx_created',
            '{{%orders}}',
            'created',
            'btree'
        );

        $this->createIndex(
            'orders_idx_email',
            '{{%orders}}',
            'email',
            'hash'
        );

        $this->db->createCommand(<<<SQL
CREATE INDEX orders_idx_referal
  ON orders
  USING hash
  (referal)
  WHERE referal IS NOT NULL;
SQL
        )->execute();
    }

    public function safeDown()
    {
        $this->db->createCommand('DROP TRIGGER orders_timestamp_before_insert ON orders')->execute();
        $this->db->createCommand('DROP FUNCTION orders_created_timestamp()')->execute();

        $this->dropTable('{{%orders}}');

        $this->db->createCommand(<<<SQL
DROP TYPE IF EXISTS licence_types;
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
DROP TYPE IF EXISTS subscription_types;
SQL
        )->execute();
    }
}
