<?php

use yii\db\Schema;
use yii\db\Migration;

class m151219_171214_posts_add_sort extends Migration
{
    public function safeUp()
    {
        $this->db->createCommand()->addColumn('posts', 'sort', \yii\db\Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0')->execute();
    }

    public function safeDown()
    {
        $this->db->createCommand()->dropColumn('posts', 'sort')->execute();
    }
}
