<?php

use yii\db\Schema;
use yii\db\Migration;

class m151219_171205_folders_add_sort extends Migration
{
    public function safeUp()
    {
        $this->db->createCommand()->addColumn('folders', 'sort', \yii\db\Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0')->execute();
    }

    public function safeDown()
    {
        $this->db->createCommand()->dropColumn('folders', 'sort')->execute();
    }
}
