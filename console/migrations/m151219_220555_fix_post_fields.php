<?php

use yii\db\Schema;
use yii\db\Migration;

class m151219_220555_fix_post_fields extends Migration
{
    public function safeUp()
    {
        $this->db->createCommand()->alterColumn('posts', 'published', 'DROP NOT NULL')->execute();
        $this->db->createCommand()->alterColumn('posts', 'name', Schema::TYPE_STRING . '(95)')->execute();
    }

    public function down()
    {
        $this->db->createCommand()->alterColumn('posts', 'published', 'SET NOT NULL')->execute();
        $this->db->createCommand()->alterColumn('posts', 'name', Schema::TYPE_STRING . '(31)')->execute();
    }
}
