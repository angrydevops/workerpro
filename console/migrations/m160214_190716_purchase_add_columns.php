<?php

use yii\db\Schema;
use yii\db\Migration;

class m160214_190716_purchase_add_columns extends Migration
{
    public $itemName = 'purchases';

    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->addColumn($tableName, 'wmi_order_id', $this->string(255));
        $this->addColumn($tableName, 'wmi_to_user_id', $this->string(12));
    }

    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->dropColumn($tableName, 'wmi_order_id');
        $this->dropColumn($tableName, 'wmi_to_user_id');
    }
}
