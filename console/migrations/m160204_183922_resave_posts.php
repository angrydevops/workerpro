<?php

use yii\db\Migration;
use common\models\Post;

class m160204_183922_resave_posts extends Migration
{
    public function safeUp()
    {
        foreach (Post::find()->select(['id', 'content'])->orderBy('id')->each(50) as $index => $post) {
            $post->setScenario('administrator');
            $post->markAttributeDirty('content');
            $post->update(false, ['content']);
        }
    }

    public function down()
    {
        echo "m160204_183922_resave_posts cannot be reverted.\n";

        return false;
    }
}
