<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_192837_add_purchase_fk_order extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'purchases';

    /**
     * @var string
     */
    protected $attributeName = 'order_id';

    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->addForeignKey(
            $this->itemName . '_fk_order',
            $tableName,
            $this->attributeName,
            '{{%orders}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->dropForeignKey($this->itemName . '_fk_order', $tableName);
    }
}
