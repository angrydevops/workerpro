<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use backend\actions\DeleteAction;
use backend\actions\ListAction;
use backend\actions\UpdateAction;
use backend\widgets\RangeColumn;
use common\controllers\AbstractController;
use common\helpers\Html;
use common\models\Folder;
use common\models\Post;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends AbstractController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		$indexGridViewColumns = [
			[
				'attribute' => 'name',
				'format' => 'html',
				'value' => function ($model) {
					$htmlParams = empty($model->title_seo) ? [] : ['title' => $model->title_seo];
					return Html::tag('span', $model->name, $htmlParams);
				}
			],
			[
				'class' => RangeColumn::className(),
				'attribute' => 'published',
				'filterInputType' => 'datetime-local',
				'format' => 'raw',
				'value' => function ($model) {
					return str_replace(' ', '<br />', $model->published);
				}
			],
			[
				'attribute' => 'folder_id',
				'format' => 'html',
				'filter' => Html::treeForDropDownList(Folder::find()->forTree()->asArray()->all()),
				'value' => function ($model) {
					return Html::a($model->folderMain->name, [
						'folder/update',
						'id' => $model->folderMain->id,
					]);
				}
			],
			'sort',
			[
				'class' => ActionColumn::className(),
				'template' => '{in_frontend}{update}',
				'buttons' => [
					'in_frontend' => function ($url, $model) {
						/** @var Post $model */
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $model->getUrl(true), [
							'target' => '_blank',
							'data-pjax' => '0',
						]);
					},
					'update' => function ($url, $model, $key) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
							'title'      => Yii::t('yii', 'Update'),
							'aria-label' => Yii::t('yii', 'Update'),
							'data-pjax' => '0',
						]);
					},
				],
			],
		];
		return [
			'update' => UpdateAction::classname(),
			'delete' => DeleteAction::classname(),
			'index'  => [
				'class' => ListAction::classname(),
				'configGridView' => [
					'columns' => $indexGridViewColumns,
				],
			],
		];
	}

	/**
	 * @return \yii\web\Response
	 * @throws \yii\base\UnknownPropertyException
	 * @throws \yii\web\ForbiddenHttpException
	 */
	public function actionCreate()
	{
		$modelClass = 'common\\models\\' . $this->getBaseModelClass();

		/**
		 * @var \common\models\Post $model
		 */
		$model = new $modelClass;
		$model->setScenario('administrator');
		$model->defaultOnCreate();
		$url = ['index'];

		if ($model->validate()) {
			if ($model->save()) {
				Yii::$app->getSession()->setFlash('success', 'Материал добавлен');
				$url = ['update', 'id' => $model->id];
			} else {
				Yii::$app->getSession()->setFlash('error', 'Не удалось добавить материал');
			}
		} else {
			Yii::$app->getSession()->setFlash('error', 'Не удалось добавить материал');

			$logger = Yii::getLogger();
			$logger->log($model->getErrors(), $logger::LEVEL_ERROR);
		}

		return $this->redirect($url);
	}
}
