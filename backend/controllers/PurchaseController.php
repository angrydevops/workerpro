<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use common\controllers\AbstractController;
use common\helpers\Html;
use common\models\Purchase;
use common\models\Order;
use backend\actions\ViewAction;
use backend\actions\ListAction;
use backend\widgets\RangeColumn;
use yii\helpers\Json;

/**
 * PurchaseController implements the CRUD actions for Purchase model.
 */
class PurchaseController extends AbstractController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'index' => [
				'class' => ListAction::className(),
				'configGridView' => [
					'columns' => [
						[
							'attribute' => 'guid',
							'format' => 'raw',
							'value' => function ($model) {
								/** @var Purchase $model */
								if (!$model->order_id) {
									return $model->guid;
								}

								return Html::a($model->order->guid, ['order/view', 'id' => $model->order_id]);
							}
						],
						'email',
						'username',
						[
							'attribute' => 'organization',
							'label' => 'Организация',
						],
						[
							'attribute' => 'licence_type',
							'filter' => Order::$licenceTypes,
							'value' => function ($model) {
								/** @var Purchase $model */
								return Order::$licenceTypes[$model->licence_type];
							}
						],
						[
							'attribute' => 'subscription_type',
							'filter' => Order::$subscriptionTypes,
							'value' => function ($model) {
								/** @var Purchase $model */
								if (!empty($model->subscription_type) && array_key_exists($model->subscription_type, Order::$subscriptionTypes)) {
									return Order::$subscriptionTypes[$model->subscription_type];
								}

								return null;
							}
						],
						[
							'class' => RangeColumn::className(),
							'attribute' => 'computers_count',
							'label' => 'Компьютеров',
						],
						'referal',
						[
							'class' => RangeColumn::className(),
							'attribute' => 'price',
							'label' => 'Стоимость',
						],
						[
							'attribute' => 'summary',
							'label' => 'Последнее событие',
							'value' => function ($model) {
								/** @var Purchase $model */
								if (!$model->summary) {
									return null;
								}

								$summary = Json::decode($model->summary);
								$lastEvent = array_pop($summary);
								if ($lastEvent) {
									return Purchase::$events[$lastEvent];
								}

								return null;
							}
						],
						[
							'class' => ActionColumn::className(),
							'template' => '{view}',
						],
					],
				],
			],
			'view' => ViewAction::className(),
		];
	}
}
