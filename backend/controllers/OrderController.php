<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use common\controllers\AbstractController;
use common\models\Order;
use backend\actions\ListAction;
use backend\actions\ViewAction;
use backend\widgets\RangeColumn;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends AbstractController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'index' => [
				'class' => ListAction::className(),
				'configGridView' => [
					'columns' => [
						'guid',
						[
							'class' => RangeColumn::className(),
							'attribute' => 'created',
							'filterInputType' => 'datetime-local',
						],
						'email',
						'username',
						[
							'attribute' => 'organization',
							'label' => 'Организация',
						],
						[
							'attribute' => 'licence_type',
							'filter' => Order::$licenceTypes,
							'value' => function ($model) {
								/** @var Order $model */
								return Order::$licenceTypes[$model->licence_type];
							}
						],
						[
							'attribute' => 'subscription_type',
							'filter' => Order::$subscriptionTypes,
							'value' => function ($model) {
								/** @var Order $model */
								if (!empty($model->subscription_type) && array_key_exists($model->subscription_type, Order::$subscriptionTypes)) {
									return Order::$subscriptionTypes[$model->subscription_type];
								}

								return null;
							}
						],
						[
							'class' => RangeColumn::className(),
							'attribute' => 'computers_count',
							'label' => 'Компьютеров',
						],
						'referal',
						[
							'class' => RangeColumn::className(),
							'attribute' => 'price',
							'label' => 'Стоимость',
						],
						[
							'class' => ActionColumn::className(),
							'template' => '{view}{purchase}',
							'buttons' => [
								'purchase' => function ($url, $model) {
									/** @var Order $model */
									if (!$model->purchase) {
										return '';
									}

									return Html::a('<span class="glyphicon glyphicon-credit-card"></span>', ['purchase/view', 'id' => $model->purchase->id], [
										'target' => '_blank',
										'data-pjax' => '0',
									]);
								},
							],
						],
					],
				],
			],
			'view' => ViewAction::className(),
		];
	}
}
