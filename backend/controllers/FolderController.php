<?php
namespace backend\controllers;

use yii\filters\AccessControl;
use backend\actions\CreateAction;
use backend\actions\DeleteAction;
use backend\actions\SelectNamesAction;
use backend\actions\TreeAction;
use backend\actions\UpdateAction;
use common\controllers\AbstractController;

/**
 * FolderController implements the CRUD actions for Folder model.
 */
class FolderController extends AbstractController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'update' =>      UpdateAction::classname(),
			'delete' =>      DeleteAction::classname(),
			'index'  =>        TreeAction::classname(),
			'create' =>      CreateAction::classname(),
		];
	}
}
