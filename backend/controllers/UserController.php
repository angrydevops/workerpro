<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use common\controllers\AbstractController;
use common\models\User;
use backend\actions\CreateAction;
use backend\actions\DeleteAction;
use backend\actions\ListAction;
use backend\actions\UpdateAction;
use backend\widgets\RangeColumn;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AbstractController
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'update' => UpdateAction::classname(),
			'delete' => DeleteAction::classname(),
			'index' => [
				'class' => ListAction::classname(),
				'configGridView' => [
					'columns' => [
						[
							'attribute' => 'role',
							'filter' => User::dictionaryRole(),
							'value' => function ($model) {
								/** @var User $model */
								return User::dictionaryRole()[$model->role];
							}
						],
						'email',
						'title',
						[
							'class' => RangeColumn::className(),
							'attribute' => 'created',
							'filterInputType' => 'datetime-local',
						],
						[
							'class' => ActionColumn::className(),
							'template' => '{update}',
						],
					],
				],
			],
			'create' => CreateAction::classname(),
		];
	}
}
