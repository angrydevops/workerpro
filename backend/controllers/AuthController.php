<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\helpers\StringHelper;
use common\models\User;
use backend\models\LoginForm;
use backend\models\PasswordNewRequestForm;

class AuthController extends Controller
{
	/**
	 * @return mixed
	 */
	public function actionLogin()
	{
		$model = new LoginForm();
		if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	/**
	 * @param null|string $redirect
	 * @return mixed
	 */
	public function actionLogout($redirect = null)
	{
		$user = Yii::$app->getUser();
		if (!$user->isGuest) {
			$user->logout();
		}

		return $this->goBack($redirect);
	}

	/**
	 * @param string $token
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
	public function actionConfirm($token)
	{
		/** @var User $user */
		$user = User::find()->where([User::COLUMN_TOKEN => $token])->andWhere('password_hash IS NOT NULL')->one();
		if ($user === null) {
			throw new NotFoundHttpException('Пользователь с таким токеном не найден.');
		}

		$password = StringHelper::generateRememberedString();
		$user->setPassword($password);
		$user->generateAuthKey();
		if ($user->role == 'user') {
			$user->role = 'verified';
		}
		$user->save(false);

		$app = Yii::$app;
		$app->getUser()->login($user);

		if ($app->getMailer()
			->compose(
				[
					'html' => 'authConfirm-html',
					'text' => 'authConfirm-text',
				],
				[
					'username' => $user->title,
					'password' => $password,
				]
			)
			->setTo($user->email)
			->setSubject('Пароль')
			->send()
		) {
			Yii::$app->getSession()->setFlash('success', 'На указанный email отправлен Ваш пароль.');
		} else {
			Yii::$app->getSession()->setFlash('warning', 'Письмо с паролем отправить не удалось. Можете установить новый или попробовать запросить новый.');
		}

		return $this->redirect('/');
	}

	/**
	 * @return mixed
	 */
	public function actionRequestPasswordNew()
	{
		$model = new PasswordNewRequestForm();
		if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', 'На указанный email отправлены дальнейшие инструкции.');

				return $this->redirect(['auth/login']);
			} else {
				Yii::$app->getSession()->setFlash('error', 'Письмо отправить не удалось. Пожалуйста, повторите попытку через некоторое время.');
			}
		}

		return $this->render('requestPasswordNew', [
			'model' => $model,
		]);
	}
}