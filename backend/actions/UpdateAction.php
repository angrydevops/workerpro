<?php
namespace backend\actions;

use Yii;
use yii\base\Action;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * UpdateAction represents an action for update.
 *
 * @author Kostya M <primat@list.ru>
 */
class UpdateAction extends Action
{
	/**
	 * @var \common\controllers\AbstractController the controller that owns this action
	 */
	public $controller;

	/**
	 * @var string view
	 */
	public $view = 'update';

	/**
	 * @var string model class
	 */
	public $modelClass;

	/**
	 * @var string model scenario
	 */
	public $modelScenario = 'administrator';

	/**
	 * @var string|array url for return after success update
	 */
	public $returnUrl;

	/**
	 * @var callable a condition to deny action
	 */
	public $denyCondition;

	/**
	 * @param string|int|array $id model id
	 * @return mixed the result of the action
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \yii\base\UnknownPropertyException
	 * @throws \yii\web\ForbiddenHttpException
	 */
	public function run($id)
	{
		$modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

		/**
		 * @var \yii\db\ActiveRecord|null $model
		 */
		$model = $modelClass::findOne($id);
		if($model === null) {
			throw new NotFoundHttpException('The requested item does not exist.');
		}

		if (!empty($this->denyCondition) && call_user_func($this->denyCondition, $model)) {
			throw new ForbiddenHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
		}

		$model->setScenario($this->modelScenario);
		if ($model->load($_POST)) {
			if ($model->validate()) {
				set_time_limit(180);
				if ($model->save(false)) {
					\Yii::$app->getSession()->setFlash('success', 'Обновлено');

					if ($this->returnUrl) {
						return $this->controller->redirect($this->returnUrl);
					} else {
						return $this->controller->refresh();
					}
				} else {
					Yii::$app->getResponse()->setStatusCode(400);
				}
			} else {
				Yii::$app->getResponse()->setStatusCode(422, 'Validation Failed');
			}
		}

		return $this->controller->render('/actions/' . $this->view, [
			'modelLabel' => $modelClass::LABEL_PLURAL,
			'model' => $model,
		]);
	}
}
