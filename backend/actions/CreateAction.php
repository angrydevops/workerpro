<?php
namespace backend\actions;

use Yii;
use yii\base\Action;

/**
 * CreateAction represents an action for create.
 *
 * @author Kostya M <primat@list.ru>
 */
class CreateAction extends Action
{
	/**
	 * @var \common\controllers\AbstractController the controller that owns this action
	 */
	public $controller;

	/**
	 * @var string view
	 */
	public $view = 'create';

	/**
	 * @var string model class
	 */
	public $modelClass;

	/**
	 * @var string model scenario
	 */
	public $modelScenario = 'administrator';

	/**
	 * @var string|array url for return after success update
	 */
	public $returnUrl;

	/**
	 * @return mixed the result of the action
	 * @throws \yii\base\UnknownPropertyException
	 * @throws \yii\web\ForbiddenHttpException
	 */
	public function run()
	{
		$modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

		/**
		 * @var \yii\db\ActiveRecord $model
		 */
		$model = new $modelClass;
		$model->setScenario($this->modelScenario);
		if ($model->load($_POST)) {
			if ($model->validate()) {
				if ($model->save(false)) {
					\Yii::$app->getSession()->setFlash('success', 'Добавлено');
					if ($this->returnUrl === null) {
						$this->returnUrl = ['update', 'id' => $model->id];
					}
					return $this->controller->redirect($this->returnUrl);
				} else {
					Yii::$app->getResponse()->setStatusCode(400);
				}
			} else {
				Yii::$app->getResponse()->setStatusCode(422, 'Validation Failed');
			}
		} else if (method_exists($model, 'defaultOnCreate')) {
			$model->defaultOnCreate();
		}

		return $this->controller->render('/actions/' . $this->view, [
			'modelLabel' => $modelClass::LABEL_PLURAL,
			'model' => $model,
		]);
	}
}
