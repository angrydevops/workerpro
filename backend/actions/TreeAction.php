<?php
namespace backend\actions;

use yii\base\Action;

/**
 * TreeAction represents an action for list as tree.
 *
 * @author Kostya M <primat@list.ru>
 */
class TreeAction extends Action
{
	/**
	 * @var \common\controllers\AbstractController the controller that owns this action
	 */
	public $controller;

	/**
	 * @var string view
	 */
	public $view = 'tree';

	/**
	 * @var string model class
	 */
	public $modelClass;

	/**
	 * @var string model scope
	 */
	public $modelScope;

	/**
	 * @return mixed the result of the action
	 * @throws \yii\base\UnknownPropertyException
	 * @throws \yii\web\ForbiddenHttpException
	 */
	public function run()
	{
		$modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

		return $this->controller->render('/actions/' . $this->view, [
			'modelLabel' => $modelClass::LABEL_PLURAL,
			'items' => $modelClass::find()->forTree()->all(),
			'withCreate' => method_exists($this->controller, 'actionCreate') || array_key_exists('create', $this->controller->actions()),
		]);
	}
}
