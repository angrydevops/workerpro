<?php
namespace backend\widgets;

use Yii;
use yii\bootstrap\Nav;

/**
 * Nav renders a nav HTML component with active element by controller.
 *
 * @author Kostya M <primat@list.ru>
 */
class NavActiveByController extends Nav
{
	/**
	 * @inheritdoc
	 */
	protected function isItemActive($item)
	{
		if (!isset($item['url'])) {
			return false;
		}

		if (is_array($item['url']) && isset($item['url'][0])) {
			$route = $item['url'][0];

			$pos = strpos($route, '/');
			if ($pos !== false) {
				$route = substr($route, 0, $pos);
			}

			if ($route == Yii::$app->controller->getUniqueId()) {
				return true;
			}
		} else {
			$pos = strpos($item['url'], '/');
			if ($pos !== false) {
				$item['url'] = substr($item['url'], 0, $pos);
			}
			if ($item['url'] === Yii::$app->getRequest()->getPathInfo()) {
				return true;
			}
		}

		return false;
	}
}
