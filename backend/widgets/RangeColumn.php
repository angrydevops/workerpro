<?php
namespace backend\widgets;

use yii\grid\DataColumn;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\Html;

/**
 * DataColumn is the column type for the [[GridView]] widget.
 *
 * It is used to display data columns and sorting ranges.
 *
 * @author Kostya M <primat@list.ru>
 */
class RangeColumn extends DataColumn
{
	/**
	 * @var string
	 */
	public $attribute_min;

	/**
	 * @var string
	 */
	public $attribute_max;

	/**
	 * @var string
	 */
	public $filterInputType = 'text';

	/**
	 * @return string
	 * @throws InvalidConfigException
	 */
	protected function renderFilterCellContent()
	{
		if ($this->filter === false) {
			throw new InvalidConfigException('The "filter" property must be true or default.');
		}
		if (!($this->grid->filterModel instanceof Model)) {
			throw new InvalidConfigException('The "filterModel" property must be Model.');
		}
		if ($this->attribute === null) {
			throw new InvalidConfigException('The "attribute" property must be set.');
		}
		if ($this->attribute_min === false && $this->attribute_max === false) {
			throw new InvalidConfigException('The "attribute_min" or "attribute_max" property must be not false.');
		}

		if ($this->attribute_min === null) {
			$this->attribute_min = $this->attribute . '_min';
		}
		if ($this->attribute_max === null) {
			$this->attribute_max = $this->attribute . '_max';
		}

		if (!($this->attribute_min === false || $this->grid->filterModel->isAttributeActive($this->attribute_min))) {
			throw new InvalidConfigException('The "attribute_min" property must be active.');
		}
		if (!($this->attribute_max === false || $this->grid->filterModel->isAttributeActive($this->attribute_max))) {
			throw new InvalidConfigException('The "attribute_max" property must be active.');
		}

		if ($this->attribute_min === false) {
			$out = '';
		} else {
			$inputOptions = $this->filterInputOptions;
			$inputOptions['class'] .= ' left50p';

			$out = Html::activeInput($this->filterInputType, $this->grid->filterModel, $this->attribute_min, $inputOptions);
		}
		if ($this->attribute_max !== false) {
			$inputOptions = $this->filterInputOptions;
			$inputOptions['class'] .= ' right50p';

			$out .= Html::activeInput($this->filterInputType, $this->grid->filterModel, $this->attribute_max, $inputOptions);
		}

		return $out;
	}
}
