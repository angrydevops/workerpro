<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);
$isWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
return [
	'id' => 'worker-backend',
	'name' => 'Административная часть Worker.Pro',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'backend\controllers',
	'defaultRoute' => 'dashboard',
	'bootstrap' => ['log'],
	'components' => [
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
		],
		'frontendUrlManager' => [
			'class' => 'frontend\components\FrontendUrlManager',
			'hostInfo' => 'http://' . $params['frontendDomain']
		],
		'log' => [
			'targets' => [
				'access' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/403.log',
					'rotateByCopy' => $isWin,
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:403'],
				],
				'notFound' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/404.log',
					'rotateByCopy' => $isWin,
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:404'],
					'logVars' => [],
				],
				'sql' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/sql.log',
					'rotateByCopy' => $isWin,
					'levels' => ['info'],
					'categories' => ['yii\db\Command*'],
					'logVars' => [],
				],
				'errorCommon' => [
					'except' => [
						'yii\web\HttpException:403',
						'yii\web\HttpException:404',
					],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'session' => [
			'class' => 'common\components\Session',
			'name' => 'WORKER',
			'useTransparentSessionID' => false,
			'useCookies' => true,
			'cookieParams' => [
				'lifetime' => 31536000, // 60*60*24*365
				'path' => '/',
				'domain' => $_SERVER['HTTP_HOST'],
				'secure' => false,
			],
			'gCProbability' => 0,
			'savePath' => '@runtime/session',
		],
		'user' => [
			'identityClass' => 'common\models\User',
			'authTimeout' => 237600, // 60 * 60 * ((24 - 18) + 48 + 9 + 3)
			'loginUrl' => 'auth/login',
		],
	],
	'params' => $params,
];
