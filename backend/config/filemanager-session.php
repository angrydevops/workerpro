<?php
ini_set('session.use_trans_sid', 0);
ini_set('session.use_cookies', 1);
ini_set('session.hash_bits_per_character', 5);
ini_set('session.gc_probability', 0);
ini_set('session.name', 'filemanager');
ini_set('session.cookie_lifetime', 2592000); // 30 days

ini_set('session.save_handler', 'files');
$dir = dirname(__DIR__) . '/runtime/filemanager_session';
if (!is_dir($dir)) {
	mkdir($dir, 0744);
}
ini_set('session.save_path', $dir);
session_start();