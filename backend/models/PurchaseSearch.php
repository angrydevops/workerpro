<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\AbstractSearch;
use common\models\Purchase;
use common\validators\DatePgValidator;

/**
 * PurchaseSearch represents the model behind the search form about Purchase.
 */
class PurchaseSearch extends AbstractSearch
{

	public $guid;
	public $created_min;
	public $created_max;
	public $email;
	public $username;
	public $organization;
	public $licence_type;
	public $subscription_type;
	public $computers_count_min;
	public $computers_count_max;
	public $price_min;
	public $price_max;
	public $referal;
	public $search;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['guid', 'email', 'username', 'organization', 'search', 'referal'], 'filter', 'filter' => 'trim'],
			[['guid','email', 'username', 'organization', 'licence_type', 'subscription_type', 'search', 'referal'], 'string'],
			[['computers_count_min', 'computers_count_max'], 'integer'],
			[['price_min', 'price_max'], 'number'],
			[['created_min', 'created_max'], DatePgValidator::className()],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getBaseModelClass()
	{
		return 'common\\models\\' . parent::getBaseModelClass();
	}

	/**
	 * @return array
	 */
	public function searchLabels()
	{
		return [];
	}

	/**
	 * @inheritdoc
	 */
	public function search($params)
	{
		/** @var Purchase $baseModelClass */
		$baseModelClass = $this->getBaseModelClass();

		$query = $baseModelClass::find()->with(['order']);
		if (!empty($params['filter'])) {
			$query->{$params['filter']}();
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			]
		]);

		$dataProvider->getSort()->attributes['created']['default'] = SORT_DESC;

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		foreach (['guid', 'licence_type', 'subscription_type'] as $attribute) {
			$this->addConditionEqual($query, $attribute);
		}
		foreach (['email', 'username', 'organization', 'referal'] as $attribute) {
			$this->addConditionLike($query, $attribute);
		}
		foreach (['created', 'computers_count', 'price'] as $attribute) {
			$this->addConditionLess($query, $attribute, $attribute . '_max');
			$this->addConditionMore($query, $attribute, $attribute . '_min');
		}
		$this->addConditionLikeMultiple($query, [
			'email',
			'username',
			'organization',
		], 'search');

		return $dataProvider;
	}
}
