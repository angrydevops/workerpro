<?php

namespace backend\models;

use common\models\AbstractSearch;
use common\models\User;
use common\validators\DatePgValidator;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends AbstractSearch
{
	public $id_min;
	public $id_max;
	public $role;
	public $email;
	public $title;
	public $created_min;
	public $created_max;
	public $search;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['role', 'email', 'title', 'search'], 'filter', 'filter' => 'trim'],
			[['role', 'email', 'title', 'search'], 'string'],
			[['id_min', 'id_max'], 'integer'],
			[['created_min', 'created_max'], DatePgValidator::className()],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getBaseModelClass()
	{
		return 'common\\models\\' . parent::getBaseModelClass();
	}

	/**
	 * @return array
	 */
	public function searchLabels()
	{
		return [];
	}

	/**
	 * @inheritdoc
	 */
	public function search($params)
	{
		/** @var User $baseModelClass */
		$baseModelClass = $this->getBaseModelClass();

		$query = $baseModelClass::find();
		if (!empty($params['filter'])) {
			$query->{$params['filter']}();
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			]
		]);

		$dataProvider->getSort()->attributes['created']['default'] = SORT_DESC;

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addConditionLess($query, 'id', 'id_max');
		$this->addConditionMore($query, 'id', 'id_min');
		$this->addConditionEqual($query, 'role');
		foreach (['email', 'title'] as $attribute) {
			$this->addConditionLike($query, $attribute);
		}
		$this->addConditionLess($query, 'created', 'created_max');
		$this->addConditionMore($query, 'created', 'created_min');
		$this->addConditionLikeMultiple($query, [
			'email',
			'title',
			'first_name',
			'last_name',
		], 'search');

		return $dataProvider;
	}
}
