<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\AbstractSearch;
use common\models\Folder;
use common\models\PostFolder;
use common\validators\DatePgValidator;

/**
 * PostSearch represents the model behind the search form about Post.
 */
class PostSearch extends AbstractSearch
{
	public $name;
	public $sort;
	public $search;
	public $folder_id;
	public $published_min;
	public $published_max;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'string'],
			[['folder_id', 'sort'], 'integer'],
			[['published_min', 'published_max'], DatePgValidator::className()],
			[['search'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getBaseModelClass()
	{
		return 'common\\models\\' . parent::getBaseModelClass();
	}

	/**
	 * @return array
	 */
	public function searchLabels() {
		return [];
	}

	/**
	 * @inheritdoc
	 */
	public function search($params)
	{
		/** @var \common\models\Post $baseModelClass */
		$baseModelClass = $this->getBaseModelClass();

		/** @var \common\models\PostQuery $query */
		$query = $baseModelClass::find()->with(['folderMain']);
		if (!empty($params['filter'])) {
			$query->{$params['filter']}();
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			]
		]);

		$dataProvider->getSort()->attributes['published']['default'] = SORT_DESC;

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addConditionEqual($query, 'folder_id');
		$this->addConditionEqual($query, 'sort');
		$this->addConditionLike($query, 'name');
		$this->addConditionMore($query, 'published', 'published_min');
		$this->addConditionLess($query, 'published', 'published_max');

		$this->addConditionLikeMultiple($query, ['slug', 'name', 'title_seo', 'description', 'content'], 'search');

		return $dataProvider;
	}
}
