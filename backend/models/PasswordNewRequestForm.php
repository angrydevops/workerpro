<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordNewRequestForm extends Model
{
	public $email;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['email', 'filter', 'filter' => 'trim'],
			['email', 'filter', 'filter' => 'mb_strtolower'],
			['email', 'required'],
			['email', 'email', 'enableIDN' => true],
			['email', 'string', 'max' => 255],
			[
				'email',
				'exist',
				'targetClass' => '\common\models\User',
				'message' => 'Пользователь с таким email не найден, пройдите регистрацию.'
			],
		];
	}

	/**
	 * Sends an email with a link, for resetting the password.
	 *
	 * @return boolean whether the email was send
	 */
	public function sendEmail()
	{
		$user = User::find()
			->select([
				'id',
				'password_hash',
			])
			->where(['email' => $this->email])
			->one();
		if (empty($user->password_hash)) {
			$user->setPassword(\Yii::$app->getSecurity()->generateRandomString());
			$user->setScenario('requestPassword');
			$user->save();
		}

		$user = User::find()
			->tokenByEmail($this->email)
			->addSelect('title')
			->one();

		$app = \Yii::$app;
		return $app->getMailer()
			->compose(
				[
					'html' => 'authPasswordNewRequest-html',
					'text' => 'authPasswordNewRequest-text',
				],
				[
					'username' => $user->title,
					'token'    => $user->token,
				]
			)
			->setTo($this->email)
			->setSubject('Запрос нового пароля')
			->send();
	}
}
