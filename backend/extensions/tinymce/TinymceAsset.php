<?php
namespace backend\extensions\tinymce;

use yii\web\AssetBundle;

/**
 * Менеджер ресурсов
 */
class TinymceAsset extends AssetBundle
{
	public $sourcePath = '@backend/extensions/tinymce/assets/vendor/tinymce';
	public $js = [
		'tinymce.min.js'
	];
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\web\YiiAsset'
	];
}
