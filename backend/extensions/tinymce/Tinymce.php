<?php
namespace backend\extensions\tinymce;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * Tinymce Widget Class
 * Wysiwyg редактор на основе плагина {@link http://www.tinymce.com Tinymce}.
 * 
 * Пример использования редактора с привязкой к модели:
 * ```php
 * ...
 * echo $form->field($model, 'text')->widget(Tinymce::className(), [
 *     'settings' => [
 *         'language' => 'ru'
 *     ]
 * ]);
 * ...
 * ```
 * 
 * Пример использования редактора независимо от модели:
 * ```php
 * ...
 * echo Tinymce::widget([
 *     'name' => 'redactor'
 * ]);
 * ...
 * ```
 * 
 * Пример использования редактора с привязкой к уже существующему элементу:
 * ```php
 * ...
 * <textarea id="textarea"></textarea>
 * ...
 * 
 * echo Tinymce::widget([
 *     'name' => 'redactor',
 *     'settings' => [
 *         'selector' => '#textarea'
 *     ]
 * ]);
 * ...
 * ```
 *
 */
class Tinymce extends InputWidget
{
	/**
	 * Настройки редактора
	 * @var array {@link http://www.tinymce.com/wiki.php/Configuration redactor options}.
	 */
	public $settings = [];

	/**
	 * @var boolean Определяем настройки по умолчанию для редактора в зависимости от типа пользователя.
	 */
	public $admin = false;

	/**
	 * @var array|null Настройки редактора по умолчанию.
	 */
	protected $_defaultSettings;

	/**
	 * @var array Настройки редактора по умолчанию для простых пользователей.
	 */
	protected $_defaultStandartSettings = [
		'language' => 'ru',
		'relative_urls' => false,
		'height' => '100px',
		'menubar' => false,
		'statusbar' => false,
		'plugins' => ['link paste'],
		'toolbar' => 'blockquote bullist numlist | bold italic strikethrough | link',
	];

	/**
	 * @var array Настройки редактора по умолчанию для простых пользователей.
	 */
	protected $_defaultAdvancedSettings = [
		'language' => 'ru',
		'relative_urls' => false,
		'height' => '700px',
		'menubar' => false,
		'statusbar' => true,
		'plugins' => ['autosave charmap code fullscreen hr image link nonbreaking paste searchreplace table template textcolor youtube responsivefilemanager em_dash euro_sign left_pointing_guillemet right_pointing_guillemet three_dot_leader non_breaking_hyphen mark'],
		'toolbar1' => 'alignleft aligncenter alignright alignjustify | outdent indent | formatselect blockquote bullist numlist table hr | searchreplace code fullscreen',
		'toolbar2' => 'bold italic strikethrough mark forecolor | euro_sign nonbreaking non_breaking_hyphen em_dash left_pointing_guillemet right_pointing_guillemet three_dot_leader | youtube link image | undo restoredraft redo',
		'external_filemanager_path' => '/filemanager/',
		'filemanager_title' => 'Менеджер файлов',
		'external_plugins' => ['filemanager' => '/filemanager/plugin.min.js'],
		'block_formats'=> 'Параграф=p;Заголовок 3=h3;Заголовок 4=h4;Блок=div',
		'formats' => [
			'strikethrough' => ['inline' => 's', 'remove' => 'all'],
		],
		'force_p_newlines' => true,
		'keep_styles'=> false,
		'browser_spellcheck' => true,
		'paste_as_text' => true,
		'filemanager_descending' => true,
	];

	/**
	 * @var string textarea
	 */
	private $_textarea;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		// if (!isset($this->options['selector']) || !isset($this->htmlOptions['id'])) {
		// Определяем идентификатор поля редактора.
		if (!isset($this->options['id'])) {
			$this->options['id'] = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->getId();
		}
		// Добавляем поле для редактора, и определяем нужный селектор.
		if (!isset($this->settings['selector'])) {
			$this->settings['selector'] = '#' . $this->options['id'];

			if ($this->hasModel()) {
				$this->_textarea = Html::activeTextarea($this->model, $this->attribute, $this->options);
			} else {
				$this->_textarea = Html::textarea($this->name, $this->value, $this->options);
			}
		}
		/* Если [[options['selector']]] указан как false удаляем селектор из настроек.
		   Это обычно нужно для динамической инициализации виджета */
		if (isset($this->settings['selector']) && $this->settings['selector'] === false) {
			unset($this->settings['selector']);
		}
		// }
		if ($this->admin !== false) {
			$this->_defaultSettings = $this->_defaultAdvancedSettings;
		} else {
			$this->_defaultSettings = $this->_defaultStandartSettings;
		}
		$this->settings = array_merge($this->_defaultSettings, $this->settings);
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		if ($this->_textarea !== null) {
			echo $this->_textarea;
		}
		$this->registerClientScript();
	}

	/**
	 * Регистрируем AssetBundle-ы виджета.
	 */
	public function registerClientScript()
	{
		$view = $this->getView();
		$settings = Json::encode($this->settings);
		TinymceAsset::register($view);
		$view->registerJs('tinymce.init(' . $settings . ');');
	}
}
