<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\widgets\Alert;
use backend\widgets\NavActiveByController;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
	<meta charset="<?= Yii::$app->charset; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags(); ?>
	<title><?= Html::encode($this->title); ?></title>
	<?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="wrap">
	<?php
	NavBar::begin([
		'brandLabel' => 'На сайт',
		'brandUrl' => Yii::$app->frontendUrlManager->getHostInfo(),
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);

	$user = Yii::$app->getUser();
	$isGuest = $user->getIsGuest();

	$menuItems = [];
	if ($isGuest) {
		$menuItems[] = ['label' => 'Войти', 'url' => ['auth/login']];
	} else {
		$menuItems[] = [
			'label' => $user->getIdentity()->title,
			'items' => [
				['label' => 'Выйти', 'url' => ['auth/logout']],
			],
		];
	}
	echo NavActiveByController::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => $menuItems,
	]);
	if (!$isGuest) {
		echo NavActiveByController::widget([
			'options' => ['class' => 'navbar-nav navbar-left'],
			'items' => [
				['label' => 'Сводка',       'url' => ['dashboard/index']],
				['label' => 'Посты',        'url' => ['post/index']],
				['label' => 'Директории',   'url' => ['folder/index']],
				['label' => 'Пользователи', 'url' => ['user/index']],
				['label' => 'Заказы',       'url' => ['order/index']],
				['label' => 'Оплата',       'url' => ['purchase/index']],
			],
		]);
	}
	NavBar::end();
	?>

	<div class="container">
		<?= Alert::widget(); ?>
		<?= $content; ?>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<p class="pull-left">© <?= Yii::$app->params['brandName']; ?>, 2014–<?= date('Y'); ?></p>

		<p class="pull-right"><?= Html::mailto(Yii::$app->params['supportEmail']); ?></p>
	</div>
</footer>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
