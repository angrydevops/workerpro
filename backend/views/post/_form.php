<?php

use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use common\models\Folder;
use common\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Post $model
 */
$form = ActiveForm::begin([
	'options' => ['enctype' => 'multipart/form-data'],
	'id' => 'post-form',
]);

$html = Html::getInputId($model, 'content');
$this->registerJs(<<<JS
\$('#post-form').on('beforeValidateAttribute', function (event, attribute, messages, deferreds) {
	if (attribute.name === '{$html}') {
		tinymce.triggerSave();
	}
	return true;
});
JS
,
	$this::POS_READY,
	'post-form'
);

$user = Yii::$app->getUser();
?>
<div class="col-lg-10">
	<div class="row">
		<div class="col-sm-12">
		<?= $form->field($model, 'name')->textarea(['maxlength' => 95, 'rows' => 3, 'class' => 'form-control post-title tile']) ?>
		<?php $this->registerJs(<<<JS
function elementOnResize(element, eventOnResize) {
	element.data('x', element.outerWidth());
	element.data('y', element.outerHeight());

	element.mouseup(function () {
		if ($(this).outerWidth() != $(this).data('x') || $(this).outerHeight() != $(this).data('y')) {
			eventOnResize();
		}
		$(this).data('x', $(this).outerWidth());
		$(this).data('y', $(this).outerHeight());
	});
}
JS
			, $this::POS_READY, 'resize-function');
		?>
		<?php $this->registerJs(<<<JS
var nameField = $('#post-name'),
	nameFieldPadding = parseInt(nameField.css('padding-bottom')) + parseInt(nameField.css('padding-top')),
	nameFieldHeight = nameField.height();

nameField.on('keyup', function() {
	nameField.height(nameFieldHeight);
	nameField.height(this.scrollHeight - nameFieldPadding);
});
JS
			, $this::POS_READY, 'name-view');
		?>
		<?php
		$this->registerJs(<<<JS
				jQuery.fn.extend({
					insertAtCaret: function(text){
						return this.each(function(i) {
							if (document.selection) {
								this.focus();
								var sel = document.selection.createRange();
								sel.text = text;
								this.focus();
							}
							else if (this.selectionStart || this.selectionStart == '0') {
								var startPos = this.selectionStart;
								var endPos = this.selectionEnd;
								var scrollTop = this.scrollTop;
								this.value = this.value.substring(0, startPos) + text + this.value.substring(endPos, this.value.length);
								this.focus();
								this.selectionStart = startPos + text.length;
								this.selectionEnd = startPos + text.length;
								this.scrollTop = scrollTop;
							} else {
								this.value += text;
								this.focus();
							}
						})
					}
				});

				updateDescriptionSymbolsCount = function() {
					var count = jQuery('#post-description').val().length;
					var counter = jQuery('#descriptionSymbolsCount');
					if (count < 200) {
						counter.removeClass().addClass('text-danger');
					} else if (count < 250) {
						counter.removeClass().addClass('text-warning');
					} else if (count < 300) {
						counter.removeClass().addClass('text-success');
					} else if (count < 400){
						counter.removeClass().addClass('text-warning');
					} else {
						counter.removeClass().addClass('text-danger');
					}
					counter.text('(' + count+')');
				};

				var symbols = '<span style="margin-left: 5px" class="symbols"><a href="#" title="Евро">€</a><a href="#" title="Неразрывный пробел">⎵</a><a href="#" title="Неразрывный дефис">&#8209;</a><a href="#" title="Длинное тире">—</a><a href="#" title="Левая кавычка">«</a><a href="#" title="Правая кавычка">»</a><a href="#" title="Многоточие">…</a></span>';
				jQuery('<span id="descriptionSymbolsCount" title="Количество символов в описании"></span>').appendTo('label[for="post-description"]');
				jQuery(symbols).appendTo('label[for="post-name"]').clone(true).appendTo('label[for="post-description"]');

				jQuery('.symbols>a').on('click', function(e) {
					e.preventDefault();
					var link = jQuery(this);
					var input = link.closest('label').next();
					if (link.text() == '⎵') {
						input.insertAtCaret(' ');
					} else {
						input.insertAtCaret(link.text());
					}
					input.focus();
				});

				jQuery('#post-description').on('focus', updateDescriptionSymbolsCount).on('keyup', updateDescriptionSymbolsCount);
				updateDescriptionSymbolsCount();
JS
			,
			$this::POS_READY,
			'labels'
		);
		?>
		</div>
		<div class="col-sm-6 zi6">
			<?= $form->field($model, 'description')->textarea(['maxlength' => 1023, 'rows' => 7, 'class' => 'form-control post-description tile']) ?>
			<?php
			$this->registerJs(<<<JS
var descriptionField = $('#post-description'),
	descriptionFieldPadding = parseInt(descriptionField.css('padding-bottom')) + parseInt(descriptionField.css('padding-top')),
	descriptionFieldHeight = descriptionField.height();

descriptionField.on('keyup', function() {
	descriptionField.height(descriptionFieldHeight);
	descriptionField.height(this.scrollHeight - descriptionFieldPadding);
});
JS
				, $this::POS_READY, 'description-view');
			?>
			<?php
			$publishHtml = '. ' . Html::a('Опубликовать', '#post-published', ['id' => 'publish-post', 'title' => 'Внести в поле +3 минуты от текущего времени', 'class' => 'dashed']);
			if (empty($model->published)) {
				$html = 'Невидимый' . $publishHtml;
			} else {
				$html = '<a href="' . $model->getUrl(true) . '" target="_blank">';
				if (time() < strtotime($model->published)) {
					$html = 'Отложенная публикация, доступен по ' . $html . 'прямой ссылке</a>' . $publishHtml;
				} else {
					$html .= 'Видимый</a>';
				}
			}
			echo $form->field($model, 'published', [
				'template' => "{label}\n" . '<div class="input-group date" id="post-published-wrapper">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div>' . "\n{hint}\n{error}",
				'options' => [
					'class' => date('Y-m-d', strtotime($model->published)) == date('Y-m-d') ? 'has-success' : '',
				],
			])->textInput(['maxlength' => 29])->hint($html);

			$this->registerJsFile('/js/moment.js',                      ['depends' =>    JqueryAsset::className()]);
			$this->registerJsFile('/js/bootstrap-datetimepicker.js',    ['depends' =>    JqueryAsset::className()]);
			$this->registerCssFile('/css/bootstrap-datetimepicker.css', ['depends' => BootstrapAsset::className()]);
			$this->registerJs(<<<JS
$("#post-published-wrapper").datetimepicker({
	language: "ru",
	format:"YYYY-MM-DD HH:mm"
});
$("#post-published").change();

$("#post-published-wrapper").on("dp.change", function (e) {
	var fieldPostPublished = $('.field-post-published'),
		published = $("#post-published").val();
	if ($.trim(published) == '') {
		fieldPostPublished.removeClass('has-success');
		return;
	}

	if (published.split(' ')[0] == moment().format('YYYY-MM-DD')) {
		fieldPostPublished.addClass('has-success');
	} else {
		fieldPostPublished.removeClass('has-success');
	}
});

$('#publish-post').on('click', function(e) {
	e.preventDefault();

	$("#post-published-wrapper").data('DateTimePicker').setDate(moment().add(3, 'minutes'));
});
JS
				, $this::POS_READY, 'published-datetimepicker');
			?>
		</div>
		<div class="col-sm-6 zi5">
			<?= $form->field($model, 'title_seo')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'slug')->textInput(['maxlength' => 31]) ?>
			<?= $form->field($model, 'folder_id')->dropDownList(Html::treeForDropDownList(Folder::find()->forTree()->all())); ?>
			<?= $form->field($model, 'sort'); ?>
		</div>
	</div>
	<?= $form->field($model, 'content', ['template' => "{input}\n{hint}\n{error}"])->widget('backend\extensions\tinymce\Tinymce', ['admin' => true]); ?>
	<?= $form->errorSummary($model); ?>
	<div class="text-center">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Добавить' : 'Обновить',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']
		); ?>
	</div>
</div>
<div class="col-lg-2">
	<div class="panel panel-warning">
		<div class="panel-heading"><?= $model->getAttributeLabel('icon') ?></div>
		<div class="panel-body">
			<?php
			if ($model->icon) { ?>
				<div class="icon-current-wrapper list" id="icon-current-wrapper"><?= Html::img($model->icon, ['id' => 'icon-current', 'class' => 'icon-current']); ?></div>
			<?php } ?>
				<?= $form->field($model, 'icon', ['template' => '{input}'])->textInput(['maxlength' => 255]) ?>
				<?php $this->registerJs(<<<JS
					jQuery('<a href="#">из текста</a>').click(function(e) {
						e.preventDefault();
						var children = jQuery(tinyMCE.get('post-content').dom.select('body')).children();
						function getUrl(id) {
							var thischildren = jQuery(children[id]).children();
							if (thischildren.length == 0) {
								return false;
							}
							var images = jQuery(children[id]).html().match(/<img([\w\W]+?)>/);
							var firstchildname = jQuery(thischildren[0]).get(0).tagName;
							if (firstchildname == 'IFRAME') {
								var iframevideourl = jQuery(thischildren[0]).attr('src');
								iframevideourl = iframevideourl.substr(iframevideourl.lastIndexOf('/') + 1);
								return 'http://img.youtube.com/vi/' + iframevideourl + '/0.jpg';
							} else if (images) {
								var imgUrl = jQuery(images[0]).attr('src');
								if (imgUrl&&imgUrl.slice(0,2) == '//') {
										imgUrl = 'http:' + imgUrl;
									}
								return imgUrl;
							} else {
								return false;
							}
						}
						var iconUrl = getUrl(0);
						if (!iconUrl) {
							iconUrl = getUrl(1);
						}
						if(iconUrl) {
							jQuery('#post-icon').val(iconUrl).trigger('blur');
						}
					}).appendTo($('.field-post-icon'));
JS
					,
					$this::POS_READY,
					'post_icon'
				); ?>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">Информация</div>
		<div class="panel-body">
			<dl>
				<dt>Посмотреть</dt>
				<dd><?= Html::a('на сайте', $model->getUrl(true), ['target' => '_blank']); ?></dd>
				<dt><?= $model->getAttributeLabel('updated'); ?></dt>
				<dd><?= date('Y-m-d H:i:s', strtotime($model->updated)); ?></dd>
			</dl>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>

