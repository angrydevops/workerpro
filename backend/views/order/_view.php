<?php

use common\models\Order;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\models\Order
 */

?>
<div class="row">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'guid',
            'created',
            'email',
            'username',
            'organization',
            [
                'attribute' => 'licence_type',
                'value' => Order::$licenceTypes[$model->licence_type],
            ],
            [
                'attribute' => 'subscription_type',
                'value' => !empty($model->subscription_type) && array_key_exists($model->subscription_type, Order::$subscriptionTypes) ? Order::$subscriptionTypes[$model->subscription_type] : null,
            ],
            'computers_count',
            'referal',
            'price',
        ],
    ]); ?>
</div>
