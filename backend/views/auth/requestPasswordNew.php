<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \backend\models\PasswordNewRequestForm $model
 */

$this->title = 'Запрос нового пароля';
?>
<div class="site-request-password">
	<h1><?= Html::encode($this->title); ?></h1>
	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
	<?= $form->field($model, 'email')->textInput(['maxlength' => true]); ?>
	<div class="form-group">
		<?= Html::submitButton('Запросить', ['class' => 'btn btn-info']); ?>
	</div>
	<?php ActiveForm::end(); ?>
	<p class="hint-block">
		Уже есть аккаунт — <a href="<?= Url::toRoute('login'); ?>">войдите</a>.
	</p>
</div>
