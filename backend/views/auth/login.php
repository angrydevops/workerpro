<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \backend\models\LoginForm $model
 */

$this->title = 'Вход на сайт';
?>
<div class="site-login">
	<h1><?= Html::encode($this->title); ?></h1>
	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
	<?= $form->field($model, 'email')->textInput(['maxlength' => true]); ?>
	<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]); ?>
	<div class="form-group">
		<?= Html::submitButton('Войти', ['class' => 'btn btn-info']); ?>
	</div>
	<?php ActiveForm::end(); ?>
	<p class="hint-block">
		Забыли свой пароль — <a href="<?= Url::toRoute('request-password-new'); ?>">запросите</a> новый. <br/>
	</p>
</div>
