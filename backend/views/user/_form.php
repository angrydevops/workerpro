<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Url;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form ActiveForm */
?>
<div class="row site-user">
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class="col-lg-8">
		<?= $form->field($model, 'title') ?>
		<?= $form->field($model, 'role')->dropDownList(User::dictionaryRole()); ?>
		<?= $form->field($model, 'email'); ?>
		<?= $form->field($model, 'first_name'); ?>
		<?= $form->field($model, 'last_name'); ?>
		<?= $form->errorSummary($model); ?>
		<div class="text-center">
			<?= Html::submitButton(
				$model->isNewRecord ? 'Добавить' : 'Обновить',
				['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']
			); ?>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-warning">
			<div class="panel-heading"><?= $model->getAttributeLabel('avatar') ?></div>
			<div class="panel-body">
				<?php
				$url = $model->getUploadUrl();
				if ($url) {
					echo Html::img($url, ['width' => '100%']);
				}
				?>
				<fieldset>
					<legend>Загрузить новую</legend>
					<?= $form->field($model, 'UploadFile')->fileInput() ?>
					<?= $form->field($model, 'UploadExternalUrl')->textInput(['maxlength' => 255]) ?>
				</fieldset>
			</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">Информация</div>
			<div class="panel-body">
				<dl class="dl-horizontal">
					<dt><?= $model->getAttributeLabel('created'); ?></dt>
					<dd><?= $model->created; ?></dd>
					<dt><?= $model->getAttributeLabel('updated'); ?></dt>
					<dd><?= $model->updated; ?></dd>
				</dl>
			</div>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>
