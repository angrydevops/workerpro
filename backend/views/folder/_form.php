<?php

use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use common\helpers\Html;
use common\models\Folder;

/**
 * @var yii\web\View $this
 * @var common\models\Folder $model
 */
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

$paths = Folder::find()->forTree();
if (!$model->isNewRecord) {
	$paths = $paths->where('id != :id', [':id' => $model->id]);
}
?>
<div class="col-lg-8">
	<?= $form->field($model, 'name')->textInput(['maxlength' => 31])->hint('Максимально краткое название для меню и списков') ?>
	<?= $form->field($model, 'title')->textInput(['maxlength' => 95])->hint('Выводится на странице в заголовке') ?>
	<?= $form->field($model, 'description')->textarea(['maxlength' => 1023, 'rows' => 3]) ?>
	<?= $form->field($model, 'PathParent')->dropDownList(Html::treeForDropDownList($paths->all(), 'path'), ['prompt' => 'Нет']) ?>
	<?= $form->field($model, 'sort'); ?>
	<?= $form->errorSummary($model); ?>
	<div class="text-center">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Добавить' : 'Обновить',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']
		); ?>
	</div>
</div>
<div class="col-lg-4">
	<div class="panel panel-success">
		<div class="panel-heading">СЕО</div>
		<div class="panel-body">
			<?= $form->field($model, 'slug')->textInput(['maxlength' => 31]) ?>
			<?php
			if ($model->isNewRecord) {
				$this->registerJsFile('/js/jquery.slugit.js', ['depends' => JqueryAsset::className()]);
				$this->registerJs('jQuery("#folder-name").slugIt();', $this::POS_READY, 'slug-auto');
			}
			?>
			<?= $form->field($model, 'path', ['template' => '{error}'])->textInput() ?>
			<?= $form->field($model, 'title_seo')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'updated')->textInput(['maxlength' => 29]) ?>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">Информация</div>
		<div class="panel-body">
			<dl class="dl-horizontal">
				<dt>Посмотреть</dt>
				<dd><?= Html::a('на сайте', $model->getUrl(true), ['target' => '_blank']); ?></dd>
			</dl>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>
