<?php

use yii\helpers\Json;
use yii\widgets\DetailView;
use common\helpers\Html;
use common\models\Order;
use common\models\Purchase;

/**
 * @var $this yii\web\View
 * @var $model common\models\Purchase
 */

$summary = Json::decode($model->summary);
$summaryText = '';

foreach ($summary as $time => $event) {
    $summaryText .= date('Y-m-d H:i:s', $time) . ': ' . Purchase::$events[$event] . '<br>';
}
?>
<div class="row">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'guid',
                'format' => 'raw',
                'value' => !$model->order_id ? $model->guid : Html::a($model->order->guid, ['order/view', 'id' => $model->order_id]),
            ],
            'email',
            'username',
            'organization',
            [
                'attribute' => 'licence_type',
                'value' => Order::$licenceTypes[$model->licence_type],
            ],
            [
                'attribute' => 'subscription_type',
                'value' => !empty($model->subscription_type) && array_key_exists($model->subscription_type, Order::$subscriptionTypes) ? Order::$subscriptionTypes[$model->subscription_type] : null,
            ],
            'computers_count',
            'referal',
            'price',
            'wmi_order_id',
            'wmi_to_user_id',
            [
                'attribute' => 'summary',
                'format' => 'raw',
                'value' => $summaryText,
            ],
        ],
    ]); ?>
</div>
