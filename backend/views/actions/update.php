<?php

use yii\helpers\Inflector;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var yii\db\ActiveRecord $model
 */

$this->title = $model->id . ' \ ' . $modelLabel;

if ($model->tableName() !== 'post' || (time() - strtotime($model->updated)) / 60 / 60 > 14) {
?>
<p class="btn-group pull-right">
	<?= Html::a('Удалить',  ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'data-confirm' => 'Вы действительно хотите удалить?']) ?>
</p>
<?php } ?>
<h1>Изменение <?= $model->id; ?></h1>

<?php echo $this->render('/' . Inflector::underscore($model->formName()) . '/_form', [
	'model' => $model,
]); ?>
