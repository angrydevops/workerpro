<?php

use yii\helpers\Inflector;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var yii\db\ActiveRecord $model
 */

$this->title = 'Добавление \ ' . $modelLabel;
?>
<h1>Добавление</h1>

<?php echo $this->render('/' . Inflector::underscore($model->formName()) . '/_form', [
	'model' => $model,
]); ?>
