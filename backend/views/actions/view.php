<?php

use yii\helpers\Inflector;

/**
 * @var yii\web\View $this
 * @var yii\db\ActiveRecord $model
 */

$this->title = $model->id . ' \ ' . $modelLabel;
?>

<h1>Просмотр <?= $model->id; ?></h1>

<?php echo $this->render('/' . Inflector::underscore($model->formName()) . '/_view', [
    'model' => $model,
]); ?>
