<?php

use common\helpers\Url;
use execut\widget\TreeView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var yii\db\ActiveRecord $items
 */

$this->title = 'Все \ ' . $modelLabel;
?>
<div class="items-tree">

	<?php if ($withCreate) { ?>
	<p class="pull-right">
		<?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php } ?>
	<h1>Список всех</h1>

	<?php
	$items_nav = [];
	foreach ($items as $item) {
		$item_nav = [
			'text' => $item->name,
			'href' => Url::toRoute(['update', 'id' => $item->id]),
		];

		$path_pos = strpos($item->path, '.');
		if ($path_pos === false) {
			$items_nav[$item->slug] = $item_nav;
		} else {
			$items_nav[substr($item->path, 0, $path_pos)]['nodes'][] = $item_nav;
		}
	}

	echo TreeView::widget([
		'data' => array_values($items_nav),
		'template' => '{tree}',
		'clientOptions' => [
			'enableLinks' => true,
			'levels' => 2,
		],
	]);
?>

</div>
