<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var array $filters
 * @var string|boolean $filter_active
 * @var common\models\AbstractSearch $filterModel
 * @var array $configGridView
 */
?>
<div class="btn-toolbar pull-right">
	<?php
	if ($withCreate) {
		echo Html::a('Добавить', ['create'], ['class' => 'btn btn-success pull-right']);
	}
	if ($filters != []) {
		?><div class="btn-group pull-right"><?php
		foreach ($filters as $name => $value) {
			$class = 'btn btn-info';
			if ($name == $filter_active) {
				$class .= ' active';
			}

			echo Html::a($value, Url::current(['filter' => $name]), ['class' => $class]);
		}
		?></div><?php
	}
	if ($filterModel->isAttributeActive('search')) {
		$form = ActiveForm::begin([
			'action' => ['index'],
			'method' => 'get',
			'options' => ['class' => 'pull-right form-inline'],
		]);
		echo $form->field($filterModel, 'search', [
			'options' => ['class' => 'input-group'],
			'template' => '{input}{button}',
			'parts' => ['{button}' => '<span class="input-group-btn">' . Html::submitButton('Поиск', ['class' => 'btn btn-primary']) . '</span>'],
		]);
		ActiveForm::end();
	}
	?>
</div>
<h1>Список <?php
	if ($filter_active) {
		$this->title = $filters[$filter_active];
		echo mb_strtolower(mb_substr($this->title, 0, 1)) . mb_substr($this->title, 1);
	} else {
		$this->title = 'Все';
		echo ' всех';
	}
	$this->title .= ' \ ' . $modelLabel;
?></h1>

<?php
Pjax::begin();
echo GridView::widget($configGridView);
Pjax::end();