<?php

namespace frontend\components;

use yii\web\UrlManager;

/**
 * FrontendUrlManager is UrlManager for Frontend.
 */
class FrontendUrlManager extends UrlManager
{
	/**
	 * @inheritdoc
	 */
	public $enablePrettyUrl = true;

	/**
	 * @inheritdoc
	 */
	public $enableStrictParsing = true;

	/**
	 * @inheritdoc
	 */
	public $showScriptName = false;

	/**
	 * @inheritdoc
	 */
	public $cache = 'cacheCommon';

	/**
	 * @inheritdoc
	 */
	public $rules = [
		'' => 'site/index',
		['pattern' => '<action:(download-worker|support|order)>', 'route' => 'site/<action>'],
		['pattern' => 'pay', 'route' => 'purchase/index', 'suffix' => '/'],
		['pattern' => 'pay/<action:(success|fail)>', 'route' => 'purchase/<action>', 'suffix' => '/'],
		['pattern' => 'pay/<action>', 'route' => 'purchase/<action>'],
		['pattern' => '<folder_path:[a-z0-9-\/]+>/<post_slug:[a-z0-9-]+>-<post_id:\d+>', 'route' => 'post/view', 'encodeParams' => false, 'suffix' => '.html'],
		['pattern' => '<folder_path:[a-z0-9-\/]+>', 'route' => 'post/folder', 'encodeParams' => false, 'suffix' => '/'],
		['pattern' => 'media/<type:user-avatar>/<name:[a-zA-Z0-9_\.-]+>-<size:(50x50|100x100)>.<extension:(jpe?g|png|gif)>', 'route' => 'thumbnail/view'],
		['pattern' => 'media/<name:[a-zA-Z0-9_\.-]+>-<size:(\d+x\d+)>.<extension:(jpe?g|png|gif)>',   'route' => 'thumbnail/view'],
		['pattern' => '<slug:[a-z0-9_-]+>', 'route' => 'post/view-by-slug'],
	];

}
