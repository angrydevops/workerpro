<?php

namespace frontend\components;

use \Yii;
use \common\helpers\Html;

/**
 * Walletone is component for https://www.walletone.com/
 */
class Walletone
{
	/**
	 * Makes form with signature from the fields
	 * @param array $fields
	 * @param string $submitText
	 * @param array $submitOptions
	 * @return string
	 */
	public static function getForm($fields = [], $submitText = 'Send', $submitOptions = []) {
		if (empty($fields)) {
			return '';
		}

		$fields = static::prepareFields($fields);

		$form = Html::beginTag('form', ['action' => 'https://wl.walletone.com/checkout/checkout/Index', 'method' => 'post']);

		foreach ($fields as $name => $value) {
			$form .= Html::input('hidden', $name, $value);
		}

		$form .= Html::submitButton($submitText, $submitOptions);

		$form .= Html::endTag('form');

		return $form;
	}

	/**
	 * @param array $fields
	 * @return array
	 */
	public static function prepareFields($fields) {
		$request = Yii::$app->getRequest();
		$fields['token'] = $request->getCsrfToken();

		foreach ($fields as $name => $val) {
			if (is_array($val)) {
				usort($val, "strcasecmp");
				$fields[$name] = $val;
			}
		}

		uksort($fields, "strcasecmp");

		$fields[$request->csrfParam] = $fields['token'];

		$fieldValues = '';

		foreach ($fields as $value) {
			if (is_array($value)) {
				foreach ($value as $v) {
					$fieldValues .= iconv('utf-8', 'windows-1251', $v);
				}
			} else {
				$fieldValues .= iconv('utf-8', 'windows-1251', $value);
			}
		}

		$fields['WMI_SIGNATURE'] = static::getSignature($fieldValues);

		return $fields;
	}

	/**
	 * @return string
	 */
	public static function getSignatureFromRequest() {
		$request = Yii::$app->getRequest();
		$post = $request->post();
		$fields = [];

		foreach ($post as $name => $value) {
			if ($name !== 'WMI_SIGNATURE' && $name !== $request->csrfParam) {
				$fields[$name] = $value;
			}
		}

		uksort($fields, "strcasecmp");

		$fieldValues = '';

		foreach ($fields as $name => $value) {
			$value = iconv('utf-8', 'windows-1251', $value);
			$fieldValues .= $value;
		}

		$fieldValues .= iconv('utf-8', 'windows-1251', $request->post('token'));

		return static::getSignature($fieldValues);
	}

	/**
	 * @param string $valuesString
	 * @return string
	 */
	public static function getSignature($valuesString) {
		return base64_encode(pack('H*', sha1($valuesString . self::getSecret())));
	}

	/**
	 * Gets key from params
	 * @return string
	 */
	public static function getSecret() {
		return Yii::$app->params['walletoneSecret'];
	}
}
