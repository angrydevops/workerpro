jQuery(document).ready(function () {
    var licenceSubmit = $('#licence .button-submit');

    if (!$('#licence-agree').prop('checked')) {
        licenceSubmit.addClass('disabled');
    }

    licenceSubmit.on('click', function (e) {
        if ($(this).hasClass('disabled')) {
            return false;
        }

        if (!$('#licence-agree').prop('checked')) {
            $(this).closest('.modal').hide();

            return false;
        }

        window.modalOpened.hide();
    });

    $('#licence input[type="radio"]').on('change', function() {
        if (!$('#licence-agree').prop('checked')) {
            licenceSubmit.addClass('disabled');
        } else {
            licenceSubmit.removeClass('disabled');
        }
    });
});