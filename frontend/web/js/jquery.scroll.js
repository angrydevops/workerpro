(function ($) {
	$.fn.scrollToTop = function (element, complete_function) {
		if (typeof element == 'string') {
			element = $(element);
		}
		if (typeof(complete_function) == 'undefined') {
			complete_function = function() {};
		}

		$('html,body').animate({
			scrollTop: element.offset().top - 80
		}, 'fast', complete_function);
	};

	$.fn.scrollToBottom = function (element, complete_function) {
		if (typeof element == 'string') {
			element = $(element);
		}
		if (typeof(complete_function) == 'undefined') {
			complete_function = function() {};
		}

		var scrollTop = element.offset().top - 80;
		if (scrollTop > $(document).scrollTop()) {
			$('html,body').animate({scrollTop: scrollTop}, 'fast', complete_function);
		} else {
			complete_function();
		}
	}
})(jQuery);
