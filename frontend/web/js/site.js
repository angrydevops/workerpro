jQuery(document).ready(function () {
	$('.logo').on('click', function (e) {
		var mainPageTopBlock = $('#worker-main');

		if (mainPageTopBlock.length > 0) {
			$.fn.scrollToTop(mainPageTopBlock);
			e.preventDefault();
		}
	});

	$('.modal').on('modal-show', function () {
		var modal = $(this);
		if (modal.find('.narrow').height() >= ($(window).height() - 80)) {
			modal.addClass('small-screen-modal');
		}

		window.modalOpened = modal;
	});

	$('#support').on('modal-close', function () {
		$('#new-support-message').trigger('click');
	});

	$('#order-modal').on('modal-close', function () {
		$('#new-order-message').trigger('click');
	});

	$('#licence-modal').on('modal-show', function () {
		if (typeof window.licenceTextFrameSrc !== 'undefined') {
			var src = $('#licence-text-frame').attr('src');

			if (typeof src === 'undefined' || src === false) {
				$('#licence-text-frame').attr('src', window.licenceTextFrameSrc);
			}
		}
	});

	$('#download').on('click', function (e) {
		if ($('#download-page').length > 0) {
			return true;
		}

		e.preventDefault();

		$('#licence-modal').show().trigger('modal-show');
	});

	$('.support-open').on('click', function (e) {
		if ($('#support-page').length > 0) {
			return true;
		}
		e.preventDefault();

		if ($(this).hasClass('question-ask')) {
			$('#supportform-type').val('help');
		}

		$('#support').show().trigger('modal-show');
	});

	if (window.location.hash == '#support') {
		$('#support').show().trigger('modal-show');
	}

	if (window.location.hash == '#order') {
		$('#order-modal').show().trigger('modal-show');
	}

	$('.open-order').on('click', function (e) {
		e.preventDefault();

		$('#order-computers_count').val($(this).data('value'));
		$('#order-modal').show().trigger('modal-show');
		window.recalculatePrice();
	});

	$('.close-modal').on('click', function (e) {
		e.preventDefault();

		$(this).closest('.modal').hide().removeClass('small-screen-modal').trigger('modal-close');

		window.modalOpened = null;
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27 && window.modalOpened != null) {
			window.modalOpened.hide();
		}
	});

	$('.order-scroll').on('click', function (e) {
		if ($('#order-page').length > 0) {
			return true;
		}

		e.preventDefault();

		if ($('#worker-order').length == 0) {
			$('#order-modal').show().trigger('modal-show');
			window.recalculatePrice();

			return false;
		}

		$.fn.scrollToBottom('#worker-order');
	});

	var pageNavLinks = $('.page-nav a');

	pageNavLinks.on('click', function (e) {
		e.preventDefault();

		var link = $(this),
		    list = pageNavLinks,
		    active = list.filter('.active');

		if (link.hasClass('active')) {
			return true;
		}

		active.removeClass('active');

		if (list.index(link) < list.index(active)) {
			$.fn.scrollToTop(link.attr('href'), function () {
				link.addClass('active');
			});
		} else {
			$.fn.scrollToBottom(link.attr('href'), function () {
				link.addClass('active');
			});
		}
	});

	window.pageOffsets = [];

	$(window).scroll(function() {
		if (typeof window.scrollTimer != 'undefined') {
			clearTimeout(window.scrollTimer);
		}

		window.scrollTimer = setTimeout(checkCurrentPage, 100);
	});

	window.checkCurrentPage = function () {
		var windowScroll = $(window).scrollTop() + 100,
		    active = $('.page-nav .active'),
		    currentId;

		if (pageOffsets.length == 0) {
			$.each(pageNavLinks, function (i, item) {
				item = $(item);

				var href = item.attr('href'),
					block = $(href);

				window.pageOffsets.push({top: block.offset().top, id:href});
			});
		}

		$.each(pageOffsets, function (i, item) {
			if (windowScroll >= item.top) {
				currentId = item.id;
			} else {
				return false;
			}
		});

		if (active.attr('href') !== currentId) {
			active.removeClass('active');
			$('a[href="' + currentId + '"]').addClass('active');
		}
	};

	$('.tab-nav a').on('click', function (e) {
		e.preventDefault();

		var tab = $(this);

		$('.tabs .tab').removeClass('active');
		$('.tab-nav .active').removeClass('active');

		$(tab.attr('href')).addClass('active');
		tab.addClass('active');

		$('#order-licence_type').val(tab.attr('href').replace('#', '')).trigger('change');
	});

	$('.subscription a').on('click', function (e) {
		e.preventDefault();

		$('.subscription .active').removeClass('active');
		$(this).addClass('active');

		var newVal = $(this).data('value'),
			subscription = window.prices['cloud'].subscribe[newVal],
			newSubscr = $('.new-subscription .val'),
			continueSubscr = $('.continue-subscription .val');

		$('#order-subscription_type').val(newVal);

		if (typeof subscription != 'undefined') {
			for (var i = 0; i < subscription.prices.length; i++) {
				$(newSubscr[i]).text(subscription.prices[i].price);
				$(continueSubscr[i]).text(subscription.pricesContinue[i]);
			}
		}
	});

	var gallery = $("#gallery");
	gallery.owlCarousel({
		items : 4,
		itemsDesktop: [1000,3],
		itemsTablet: [820,2],
		itemsMobile: [615,1],
		slideSpeed: 400,
		pagination: false,
		addClassActive: true,
		autoPlay: false,
		autoHeight: false
	});

	$(".gallery-controls .arrow-right").click(function(e){
		e.preventDefault();
		gallery.trigger('owl.next');
	});

	$(".gallery-controls .arrow-left").click(function(e){
		e.preventDefault();
		gallery.trigger('owl.prev');
	});

	$(document).ready(function() {
		gallery.lightGallery({
			selector: 'a',
			autoplay: false,
			autoplayControls: false,
			download: false
		});
	});

	var advantages = $('.advantages-wrapper');
	advantages.owlCarousel({
		singleItem: true,
		slideSpeed: 400,
		pagination: false,
		addClassActive: true,
		autoPlay: false,
		autoHeight: true,
		mouseDrag: false,
		touchDrag: false
	});

	var advantagesSlider = advantages.data('owlCarousel');

	$("#goto-advantages").click(function(e){
		e.preventDefault();

		advantagesSlider.goTo(0);

		$('.advantages-link').removeClass('active');
		$(this).addClass('active');
	});

	$("#goto-capabilities").click(function(e){
		e.preventDefault();
		advantagesSlider.goTo(1);

		$('.advantages-link').removeClass('active');
		$(this).addClass('active');
	});

	window.localStorageGetItem = function (key, returnValue) {
		if (typeof window['localStorage'] !== 'undefined') {
			return localStorage.getItem(key);
		} else if (typeof returnValue !== 'undefined') {
			return returnValue;
		}
	};

	window.localStorageSetItem = function (key, value) {
		if (typeof window['localStorage'] !== 'undefined') {
			localStorage.setItem(key, value);
		}
	};
});