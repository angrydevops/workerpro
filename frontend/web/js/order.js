jQuery(document).ready(function () {
    var orderMessageSent = $('#order-message-sent');
    if (orderMessageSent.length > 0) {
        orderMessageSent.append('<a href="#order-form" class="button button-submit" id="new-order-message">\u041e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u0435\u0449\u0451 \u043e\u0434\u043d\u0443</a>'); // Оставить ещё одну
        $('#order-form-wrapper').addClass('hidden');
    }

    $('#new-order-message').on('click', function (e) {
        e.preventDefault();

        $('#order-message-sent').addClass('hidden');
        $('#order-form-wrapper').removeClass('hidden');
    });

    $('#order-licence_type,#order-licence_type input:radio').on('change', function (e) {
        var subscription = $('.field-order-subscription_type');

        if ($(this).val() == 'cloud') {
            subscription.show();
            $('#order-subscription_type').val(window.defaultCloudSubscription);
        } else {
            subscription.hide();
            $('#order-subscription_type').val('');
        }

        window.recalculatePrice();
    });

    $('#order-computers_count').on('change', function (e) {
        window.recalculatePrice();
    });

    $('#order-subscription_type').on('change', function (e) {
        window.recalculatePrice();
    });

    window.recalculatePrice = function () {
        var licence_type = $('#order-licence_type').val();
        if(!licence_type){
            licence_type = $("#order-licence_type input:radio:checked").val();
        }
        var licence = window.prices[licence_type],
            computers = $('#order-computers_count'),
            count = computers.val(),
            subscription = $('#order-subscription_type').val(),
            price = 1,
            prices;
        if (typeof licence.subscribe != 'undefined') {
            if (typeof licence.subscribe[subscription] != 'undefined') {
                price *= licence.subscribe[subscription].count;
                prices = licence.subscribe[subscription].prices;
            }
        } else {
                prices = licence.prices;
        }
        
        var minCount = prices[0].min;

        computers.attr('min', minCount);
        
        if (count < minCount) {
            computers.val(minCount);
            count = minCount;
        }

        price *= count;

        for (var i = 0; i < prices.length; i++) {
            var isCurrent = true;
            isCurrent = isCurrent && prices[i].min <= count;
            isCurrent = isCurrent && (prices[i].max === null || prices[i].max >= count);

            if (isCurrent) {
                price *= prices[i].price;
                break;
            }
        }
        if($('#order-price').is('input'))
        {
            $('#order-price').val(price);
        }
        else {
            $('.order-price').text(price);
            $('#order-price').text(price);
        }

    };

    recalculatePrice();
});