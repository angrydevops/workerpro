jQuery(document).ready(function () {
    var supportMessageSent = $('#support-message-sent');
    if (supportMessageSent.length > 0) {
        supportMessageSent.append('<a href="#support-form" class="button button-submit" id="new-support-message">\u041d\u0430\u043f\u0438\u0441\u0430\u0442\u044c \u0435\u0449\u0435</a>'); // Написать еще
        $('#support-form').addClass('hidden');
    }

    $('#new-support-message').on('click', function (e) {
        e.preventDefault();

        $('#support-message-sent').addClass('hidden');
        $('#support-form').removeClass('hidden');
    });
});