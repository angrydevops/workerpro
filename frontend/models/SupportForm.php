<?php

namespace frontend\models;

use yii\base\Model;

/**
 * Support form
 */
class SupportForm extends Model
{
	/**
	 * @var string username
	 */
	public $username;

	/**
	 * @var string message type
	 */
	public $type;

	/**
	 * @var string email
	 */
	public $email;

	/**
	 * @var string text of message
	 */
	public $text;

	/**
	 * @var array messages' types
	 */
	public $typesLabels = [
		'common'  => 'Общие вопросы',
		'help'    => 'Помощь с программой',
		'order'   => 'Финансовые вопросы',
		'partner' => 'Партнерство',
		'bug'     => 'Жалоба руководству',
	];

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['username', 'email', 'text'], 'filter', 'filter' => 'trim'],
			[['email'], 'filter', 'filter' => 'mb_strtolower'],
			[['username', 'text', 'type', 'email'], 'required'],
			['email', 'email', 'enableIDN' => true],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'username'  => 'Контактное лицо',
			'email' => 'E-mail',
			'type'  => 'Тип',
			'text'  => 'Сообщение',
		];
	}

	/**
	 * @return array
	 */
	public static function getTypesEmails() {
		$params = \Yii::$app->params;

		return [
			'common'  => $params['supportEmail'],
			'help'    => $params['supportEmail'],
			'order'   => $params['orderEmail'],
			'partner' => $params['partnerEmail'],
			'bug'     => $params['bugEmail'],
		];
	}
}
