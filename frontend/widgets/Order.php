<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use frontend\assets\OrderAsset;
use common\helpers\Url;

/**
 * Order widget renders order form
 */
class Order extends Widget
{
    /**
     * @var boolean widget is in modal
     */
    public $isModal = true;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $view = $this->getView();
        $model = new \common\models\Order();
        $user = Yii::$app->getUser();
        $autocomplete = $user->getGuest();
        $forceOpenModal = false;
        $request = Yii::$app->getRequest();
        $post = $request->post();
        $attributesToLoad = [
            'organization',
            'username',
            'email',
            'computers_count',
            'licence_type',
            'subscription_type',
            'referal',
        ];

        if ($request->getIsPost() && array_key_exists('Order', $post)) {
            $post = $post['Order'];
            foreach ($attributesToLoad as $attribute) {
                $model->$attribute = $post[$attribute];
            }

            if ($model->validate()) {
                $model->calculatePrice();

                if ($model->save()) {
                    $model->refresh();

                    $user->setAutocomplete([
                        'organization' => $model->organization,
                        'username'     => $model->username,
                        'email'        => $model->email,
                    ]);

                    $mail = Yii::$app->getMailer()
                        ->compose(
                            [
                                'html' => 'order-html',
                                'text' => 'order-text',
                            ],
                            [
                                'model' => $model,
                            ]
                        )
                        ->setTo(Yii::$app->params['orderEmail'])
                        ->setFrom([$model->email => $model->username])
                        ->setCharset('utf-8')
                        ->setSubject('Запрос покупки')
                        ->send();

                    if ($mail) {
                        Yii::$app->getResponse()->redirect(Url::to(Url::current(['order' => 'sended', '#' => 'order'])), 302);
                        Yii::$app->end();
                    }
                }

                $forceOpenModal = true;
                Yii::$app->getResponse()->setStatusCode(400);
            } else {
                $forceOpenModal = true;
                Yii::$app->getResponse()->setStatusCode(422, 'Validation Failed');
            }
        } else if (!empty($autocomplete)) {
            if (array_key_exists('organization', $autocomplete)) {
                $model->organization = $autocomplete['organization'];
            }

            if (array_key_exists('username', $autocomplete)) {
                $model->username = $autocomplete['username'];
            }

            if (array_key_exists('email', $autocomplete)) {
                $model->email = $autocomplete['email'];
            }

            if (array_key_exists('referal', $autocomplete)) {
                $model->referal = $autocomplete['referal'];
            }
        }

        $params = Yii::$app->params;
        $prices = \yii\helpers\Json::encode($params['programPrices']);
        $defaultCloudSubscription = $params['defaultCloudSubscription'];

        $view->registerJs(<<<JS
window.prices = {$prices};
window.defaultCloudSubscription = '{$defaultCloudSubscription}';
JS
            , $view::POS_END, 'prices');

        OrderAsset::register($view);

        return $view->render('/widgets/order', [
            'model' => $model,
            'isModal' => $this->isModal,
            'forceOpenModal' => $forceOpenModal,
            'mailSent' => Yii::$app->getRequest()->get('order') === 'sended',
        ]);
    }
}
