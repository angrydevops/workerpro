<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use frontend\assets\DownloadAsset;

/**
 * Download widget renders download link with license text
 */
class Download extends Widget
{

    /**
     * @var boolean widget is in modal
     */
    public $isModal = true;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $view = $this->getView();

        DownloadAsset::register($view);

        return $view->render('/widgets/download', [
            'isModal' => $this->isModal,
            'licencePostId' => Yii::$app->params['licencePostId'],
        ]);
    }
}
