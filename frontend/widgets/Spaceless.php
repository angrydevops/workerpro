<?php

namespace frontend\widgets;

/**
 * Returns spaceless content
 */
class Spaceless extends \yii\widgets\Spaceless
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$content = ob_get_clean();

		if (strpos($content, 'nospaceless') !== false) {
			$parts = preg_split('/<!--\/?nospaceless-->/', $content);

			$noSpaceless = false;
			foreach ($parts as $part) {
				if ($noSpaceless) {
					echo $part;
				} else {
					echo trim(preg_replace('/>\s+</', '><', $part));
				}
				$noSpaceless = !$noSpaceless;
			}
		} else {
			echo trim(preg_replace('/>\s+</', '><', $content));
		}
	}
}