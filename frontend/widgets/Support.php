<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\VarDumper;
use common\helpers\Url;
use frontend\assets\SupportAsset;
use frontend\models\SupportForm;

/**
 * Support widget renders support form
 */
class Support extends Widget
{

    /**
     * @var boolean widget is in modal
     */
    public $isModal = true;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $view = $this->getView();
        $model = new SupportForm();
        $user = Yii::$app->getUser();
        $autocomplete = $user->getGuest();
        $request = Yii::$app->getRequest();
        $forceOpenModal = false;

        if ($model->load($request->post())) {
            if ($model->validate()) {
                Yii::info(VarDumper::dumpAsString($model), 'notifications/support');

                $user->setAutocomplete([
                    'username' => $model->username,
                    'email'    => $model->email,
                ]);

                $mail = Yii::$app->getMailer()
                    ->compose(
                        [
                            'html' => 'support-html',
                            'text' => 'support-text',
                        ],
                        [
                            'model' => $model,
                        ]
                    )
                    ->setTo($model::getTypesEmails()[$model->type])
                    ->setSubject('Обращение с сайта')
                    ->setFrom([$model->email => $model->username])
                    ->send();

                if ($mail) {
                    Yii::$app->getResponse()->redirect(Url::to(Url::current(['support-sent' => 1, '#' => 'support'])), 302);
                } else {
                    $forceOpenModal = true;
                    Yii::$app->getResponse()->setStatusCode(400);
                }
            } else {
                $forceOpenModal = true;
                Yii::$app->getResponse()->setStatusCode(422, 'Validation Failed');
            }
        } else if (!empty($autocomplete)) {
            if (array_key_exists('username', $autocomplete)) {
                $model->username = $autocomplete['username'];
            }

            if (array_key_exists('email', $autocomplete)) {
                $model->email = $autocomplete['email'];
            }
        }

        SupportAsset::register($view);

        return $view->render('/widgets/support', [
            'model' => $model,
            'isModal' => $this->isModal,
            'forceOpenModal' => $forceOpenModal,
            'mailSent' => Yii::$app->getRequest()->get('support-sent', false) == 1,
        ]);
    }
}
