<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\helpers\Html;
use common\helpers\StringHelper;
use common\helpers\Url;
use common\models\Post;
use common\models\PostQuery;
use common\models\Folder;

/**
 * Renders tree with posts
 */
class FullTree extends Widget
{
    /**
     * @var string path of folder to search in
     */
    public $mainFolder = null;

    /**
     * @var integer id of active element
     */
    public $activeId = null;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $tree = $this->getTree();

        echo $this->getList($tree[0]['nodes'], true);
    }

    /**
     * @param array $tree
     * @param boolean $expanded
     * @return string
     */
    protected function getList($tree, $expanded)
    {
        $listOptions = [];
        if ($expanded) {
            $listOptions['class'] = 'expanded';
        }

        if (empty($tree)) {
            return '';
        }

        $list = Html::beginTag('ul', $listOptions);

        foreach ($tree as $node) {
            $nodeOptions = [
                'class' => $node['type'],
            ];

            if (array_key_exists('active', $node) && $node['active']) {
                $nodeOptions['class'] .= ' active';
            }

            $content = Html::a($node['text'], $node['href']);
            if (array_key_exists('nodes', $node)) {
                $content .= $this->getList($node['nodes'], array_key_exists('expanded', $node) && $node['expanded']);
            }

            $list .= Html::tag('li', $content, $nodeOptions);
        }

        $list .= Html::endTag('ul');

        return $list;
    }

    /**
     * @return array|\common\models\Folder[]
     */
    public function getData()
    {
        $query = Folder::find()
            ->forTree()
            ->with([
                'posts' => function ($q) {
                    /** @var PostQuery $q */
                    $q->select(['id', 'folder_id', 'name', 'slug', 'sort']);
                    $q->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC]);
                }
            ]);

        if (!empty($this->mainFolder)) {
            $query->sameParent($this->mainFolder);
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function getTree()
    {
        $items_nav = [];

        foreach ($this->getData() as $item) {
            $posts = [];
            $expanded = false;

            foreach ($item['posts'] as $post) {
                /** @var Post $post */
                $postNode = [
                    'type' => 'post',
                    'text' => $post->name,
                    'href' => $post->getUrl(),
                ];

                if ($post->id == $this->activeId) {
                    $postNode['active'] = true;
                }

                $posts[] = $postNode;
            }

            $item_nav = [
                'type' => 'folder',
                'text' => $item->name,
                'href' => Url::toRoute([
                    'post/folder',
                    'folder_path' => StringHelper::mb_str_replace('.', '/', $item->path),
                ]),
                'nodes' => $posts,
            ];

            if ($item->path == $this->mainFolder) {
                $expanded = true;
                $item_nav['expanded'] = true;
                $item_nav['active'] = true;
            }

            $path_pos = strpos($item->path, '.');

            if ($path_pos === false) {
                $items_nav[$item->slug] = $item_nav;
            } else {
                $items_nav[substr($item->path, 0, $path_pos)]['nodes'][] = $item_nav;
                if ($expanded) {
                    $items_nav[substr($item->path, 0, $path_pos)]['expanded'] = true;
                }
            }
        }

        return array_values($items_nav);
    }

}
