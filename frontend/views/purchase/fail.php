<?php

use \yii\helpers\Url;
use \common\models\Purchase;

/**
 * @var yii\web\View $this
 * @var Purchase|null $purchase
 */

$params = Yii::$app->params;

$this->registerMetaTag([
	'property' => 'og:type',
	'content' => 'website',
]);

$url = Url::toRoute(['purchase/fail'], true);

$this->registerMetaTag([
	'property' => 'og:url',
	'content' => $url,
]);

$this->registerLinkTag([
	'rel' => 'canonical',
	'href' => $url,
]);

$this->registerMetaTag([
	'property' => 'og:image',
	'content' => Url::to('@web/styles/worker-logo.png', true),
]);

$this->registerMetaTag([
	'property' => 'og:title',
	'content' => $params['mainOgTitle'],
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => $params['mainMetaDescription'],
]);

$this->registerMetaTag([
	'property' => 'og:description',
	'content' => $params['mainOgDescription'],
]);

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $params['mainKeywords'],
]);

$this->title = 'Заказ не оплачен';
?>
<div class="pay-page site-page" id="pay-page">
	<div class="narrow">
		<h1><?= $this->title ?></h1>
		<?php if (empty($purchase)) { ?>
		<p>Заказ не найден.</p>
		<?php } else if (!empty($purchase->guid)) { ?>
			<p>Заказ не оплачен. <a href="<?= Url::toRoute(['purchase/index', 'order' => $purchase->guid])?>">Перейти к оплате</a></p>
		<?php } else { ?>
		<?php } ?>
	</div>
</div>
