<?php

use \yii\helpers\Url;
use \common\helpers\Html;
use \common\models\Purchase;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var Purchase|null $purchase
 * @var string|null $orderParam
 * @var string|null $clientParam
 */

$params = Yii::$app->params;
\frontend\assets\OrderAsset::register($this);

$this->registerMetaTag([
	'property' => 'og:type',
	'content' => 'website',
]);

$url = Url::toRoute(['purchase/index'], true);

$this->registerMetaTag([
	'property' => 'og:url',
	'content' => $url,
]);

$this->registerLinkTag([
	'rel' => 'canonical',
	'href' => $url,
]);

$this->registerMetaTag([
	'property' => 'og:image',
	'content' => Url::to('@web/styles/worker-logo.png', true),
]);

$this->registerMetaTag([
	'property' => 'og:title',
	'content' => $params['mainOgTitle'],
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => $params['mainMetaDescription'],
]);

$this->registerMetaTag([
	'property' => 'og:description',
	'content' => $params['mainOgDescription'],
]);

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $params['mainKeywords'],
]);

$this->title = 'Оформление продления подписки';

$all = 0;
?>
<div class="pay-page site-page" id="pay-page">
	<div class="narrow">
		<h1><?= $this->title ?></h1>
		<?php if (!$isValid) { ?>
			<p>Ссылка не действительна или устарела.</p>
			<p><a href="<?= Url::toRoute(['site/support']); ?>" rel="nofollow">Обратиться в поддержку</a>.</p>
			<?php
		}elseif(Yii::$app->session->hasFlash('order-success')){?>
				<div class="message-sent" id="order-message-sent">
					<h3><?=Yii::$app->session->getFlash('order-success',null,true)?></h3>
				</div>
			<?php
		} else { ?>
			<p class="subtitle"><?= 'Для клиента: ' . $client['name']; ?></p>

			<div id="order-form">
				<?php $form = ActiveForm::begin([]) ?>
				<h3>Выберите тип подписки и количество сотрудников</h3>
				<div class="version" style="width: 30%;border-right: 1px solid #000;float:left;">
					<?php /*=$form->field($model,'licence_type')->radioList(\common\models\Order::$licenceTypes)->label('Версия:')*/?>
					<?= $form->field($model,'licence_type')->hiddenInput()->label(false); ?>
					Версия: <br>
					<?= \common\models\Order::$licenceTypes[$model->licence_type] ?>
				</div>
				<div class="params" style="width: 68%;float:left;">
					<table style="width: 100%;margin: 0 10px;">
						<tr>
							<td>Срок действия</td><td>Кол-во</td><td>Сумма</td>
						</tr>
						<?php foreach($licenses as $license):?>
							<?php $all+=$model->calculatePrice();?>
						<tr>
							<td>
								<?= $license['subscription_type'] ? \common\models\Order::$subscriptionTypes[$license['subscription_type']] : ''?>
							</td>
							<td>
								<?=Html::activeTextInput($model,'computers_count')?>
							</td>
							<td>
								<span class="order-price"><?=$model->calculatePrice()?></span>
							</td>
						</tr>
						<?php endforeach;;?>
					</table>
				</div>
				<div class="meta" style="clear:both;border-top:1px solid #000;border-bottom: 1px solid #000;padding: 20px; margin: 0 10px;">
					<? /*=Html::activeTextInput($model,'referal',['placeholder'=>'Промокод'])*/ ?>
					<p style="text-align: right;">ИТОГО: <span  id="order-price"><?=$all?></span></p>
				</div>
				<div style="padding: 10px;">
					<span style="font-weight: 700;float: left;">ВЫ: </span>
					<div style="width: 300px;float: left;">
					<?php /*= Html::activeRadioList($model,'user_type',\common\models\Order::$userTypes,['separator'=>'<br/>'])*/ ?>
					<?= Html::activeHiddenInput($model, 'user_type') ?>
						 <?= \common\models\Order::$userTypes[$model->user_type]; ?>
					</div>
					<div class="actions">
						<div class="buttons">
							<?= Html::submitButton('Оплатить он-лайн', ['class' => 'button button-submit '.($model->user_type != 'fiz' ? 'hidden':''),'id'=>'fiz-but']); ?>
							<?= Html::submitButton('Получить счёт', ['class' => 'button button-submit '.($model->user_type != 'ur' ? 'hidden':''),'id'=>'ur-but']); ?>
						</div>
					</div>
				</div>
				<?=Html::activeHiddenInput($model,'subscription_type')?>
				<?php ActiveForm::end()?>
			</div>
		<?php } ?>
	</div>
</div>
<?php
$params = Yii::$app->params;
$prices = \yii\helpers\Json::encode($params['programPrices']);
$defaultCloudSubscription = $params['defaultCloudSubscription'];

$this->registerJs(<<<JS
window.prices = {$prices};
window.defaultCloudSubscription = '{$defaultCloudSubscription}';
JS
	, \yii\web\View::POS_END, 'prices');

?>