<?php

use \yii\helpers\Url;
use \common\helpers\Html;
use \common\models\Purchase;

/**
 * @var yii\web\View $this
 * @var Purchase|null $purchase
 * @var string|null $orderParam
 * @var string|null $clientParam
 */

$params = Yii::$app->params;

$this->registerMetaTag([
	'property' => 'og:type',
	'content' => 'website',
]);

$url = Url::toRoute(['purchase/index'], true);

$this->registerMetaTag([
	'property' => 'og:url',
	'content' => $url,
]);

$this->registerLinkTag([
	'rel' => 'canonical',
	'href' => $url,
]);

$this->registerMetaTag([
	'property' => 'og:image',
	'content' => Url::to('@web/styles/worker-logo.png', true),
]);

$this->registerMetaTag([
	'property' => 'og:title',
	'content' => $params['mainOgTitle'],
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => $params['mainMetaDescription'],
]);

$this->registerMetaTag([
	'property' => 'og:description',
	'content' => $params['mainOgDescription'],
]);

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $params['mainKeywords'],
]);

$this->title = 'Оплата';

$purchaseModel = new Purchase();
?>
<div class="pay-page site-page" id="pay-page">
	<div class="narrow">
		<h1><?= $this->title ?></h1>
		<?php if ($purchase === null) { ?>
			<p>Ссылка не действительна или устарела.</p>
			<?php
			$noOrderLink = Html::a('Создайте новую заявку', ['site/order'], ['rel' => 'nofollow']);
			$noClientLink = Html::a('Воспользуйтесь инструкцией', [
				'post/view',
				'folder_path' => 'help/general',
				'post_slug' => 'subscription',
				'post_id' => 9
			]);

			$links = $noOrderLink;

			if ($orderParam === null) {
				if ($clientParam === null) {
					$links .= ' или ' . $noClientLink;
				} else {
					$links = $noClientLink;
				}
			}

			?>
			<p><?= $links; ?>, чтобы оплатить.</p>
			<p>Также, вы всегда можете <a href="<?= Url::toRoute(['site/support']); ?>" rel="nofollow">обратиться в поддержку</a>.</p>
		<?php } else { ?>
			<?php
			if ($purchase['licence_type'] === 'cloud') {
				$type = 'облачной';
				$subscriptionType = $purchaseModel::$subscriptionTypes[$purchase['subscription_type']];
				$subscription = ' ' . $subscriptionType;
			} else {
				$type = 'локальной';
				$subscription = '';
			}

			$description = $type . ' лицензии Worker' . $subscription . ' для ' . $purchase['computers_count'] . ' ' . Yii::t('app', '{n, plural, one{компьютера} few{компьютеров} many{компьютеров} other{компьютеров}}', ['n' => $purchase['computers_count']]) . '.';
			$price = number_format($purchase['price'], 2, '.', '');
			?>
			<p><?= 'Заказ ' . $description; ?></p>
			<p>Контактная информация:</p>
			<ul>
				<li><?= $purchaseModel->getAttributeLabel('organization'); ?>: <?= $purchase['organization']; ?></li>
				<li><?= $purchaseModel->getAttributeLabel('username'); ?>: <?= $purchase['organization']; ?></li>
				<li><?= $purchaseModel->getAttributeLabel('email'); ?>: <?= $purchase['email']; ?></li>
			</ul>
			<p><strong><?= $purchaseModel->getAttributeLabel('price'); ?>: <?= $price; ?> руб.</strong></p>
			<?= \frontend\components\Walletone::getForm([
				'WMI_MERCHANT_ID' => Yii::$app->params['wmiMerchantId'],
				'WMI_PAYMENT_AMOUNT' => $price,
				'WMI_PAYMENT_NO' => $purchase['guid'],
				'WMI_CURRENCY_ID' => '643',
				'WMI_DESCRIPTION' => "BASE64:" . base64_encode('Оплата ' . $description),
				'WMI_SUCCESS_URL' => Url::toRoute(['purchase/success'], true),
				'WMI_FAIL_URL'    => Url::toRoute(['purchase/fail'], true),
			], 'Оплатить', ['class' => 'button button-submit']); ?>
		<?php } ?>
	</div>
</div>
