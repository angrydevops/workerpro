<?php

use yii\helpers\Url;
use frontend\widgets\Order;

/**
 * @var yii\web\View $this
 */

$params = Yii::$app->params;

$this->registerMetaTag([
	'property' => 'og:type',
	'content' => 'website',
]);

$url = Url::toRoute(['site/order'], true);

$this->registerMetaTag([
	'property' => 'og:url',
	'content' => $url,
]);

$this->registerLinkTag([
	'rel' => 'canonical',
	'href' => $url,
]);

$this->registerMetaTag([
	'property' => 'og:image',
	'content' => Url::to('@web/styles/worker-logo.png', true),
]);

$this->registerMetaTag([
	'property' => 'og:title',
	'content' => $params['mainOgTitle'],
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => $params['mainMetaDescription'],
]);

$this->registerMetaTag([
	'property' => 'og:description',
	'content' => $params['mainOgDescription'],
]);

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $params['mainKeywords'],
]);

$this->title = 'Купить';
?>
<div class="order-page site-page" id="order-page">
	<div class="narrow">
		<h1><?= $this->title ?></h1>
		<?= Order::widget([
			'isModal' => false,
		]); ?>
	</div>
</div>
