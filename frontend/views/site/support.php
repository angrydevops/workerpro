<?php

use yii\helpers\Url;
use frontend\widgets\Support;

/**
 * @var yii\web\View $this
 */

$params = Yii::$app->params;

$this->registerMetaTag([
	'property' => 'og:type',
	'content' => 'website',
]);

$url = Url::toRoute(['site/support'], true);

$this->registerMetaTag([
	'property' => 'og:url',
	'content' => $url,
]);

$this->registerLinkTag([
	'rel' => 'canonical',
	'href' => $url,
]);

$this->registerMetaTag([
	'property' => 'og:image',
	'content' => Url::to('@web/styles/worker-logo.png', true),
]);

$this->registerMetaTag([
	'property' => 'og:title',
	'content' => $params['mainOgTitle'],
]);

$this->registerMetaTag([
	'name' => 'description',
	'content' => $params['mainMetaDescription'],
]);

$this->registerMetaTag([
	'property' => 'og:description',
	'content' => $params['mainOgDescription'],
]);

$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $params['mainKeywords'],
]);

$this->title = 'Поддержка';
?>
<div class="support-page site-page" id="support-page">
	<div class="narrow">
		<h1><?= $this->title ?></h1>
		<?= Support::widget([
			'isModal' => false,
		]); ?>
	</div>
</div>
