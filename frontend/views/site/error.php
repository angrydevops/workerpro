<?php

use \common\helpers\Html;
use \yii\web\NotFoundHttpException;
use \yii\web\ForbiddenHttpException;

/**
 * @var $this yii\web\View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 */

$this->title = $message;
?>
<div class="site-page error-page">
    <div class="narrow">
        <h1><?= Html::encode($this->title); ?></h1>
        <p>
            <?php
            if ($exception instanceof NotFoundHttpException) {
                ?>Вы можете перейти на главную или найти её самостоятельно.<?php
            } elseif ($exception instanceof ForbiddenHttpException) {
                ?>Войдите под аккаунтом, имеющим такие права.<?php
            } else {
                ?>Мы уже работаем над исправлением ошибки.<?php
            }
            ?>
        </p>
    </div>
</div>
