<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use frontend\assets\SiteAsset;
use frontend\widgets\Spaceless;
use common\helpers\Url;

SiteAsset::register($this);

$params = Yii::$app->params;
$supportEmail = $params['supportEmail'];

Spaceless::begin();
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta property="og:locale" content="ru_RU"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php
$this->beginBody(); ?>
<div class="content">
	<header class="header">
		<div class="narrow">
			<a href="<?= Url::to('@web/', true); ?>" class="logo">
				<img src="/styles/worker-logo.png" alt="Воркер" width="280" height="55">
				<img src="/styles/worker-logo-mobile.png" alt="Воркер" width="135" height="55" class="logo-mobile">
				<span class="description">Контроль<br>рабочего времени</span>
			</a>
			<nav class="top-menu">
				<ul>
					<li>
						<a href="<?= Url::toRoute([
							'post/folder',
							'folder_path' => 'help'
						])?>">
							<span>Поддержка</span>
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12">
								<use xlink:href="#worker-help-icon"/>
							</svg>
						</a>
					</li>
					<li>
						<a href="<?= Url::toRoute(['site/download-worker'])?>" id="download">
							<span>Скачать</span>
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12">
								<use xlink:href="#worker-download-icon"/>
							</svg>
						</a>
					</li>
					<li class="order">
						<a href="<?= Url::toRoute(['site/order']); ?>" class="order-scroll" rel="nofollow">
							<span>Купить</span>
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12">
								<use xlink:href="#worker-order-icon"/>
							</svg>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
		<?php  foreach (Yii::$app->session->getAllFlashes(true) as $key => $messages): ?>
			<div class="col-sm-12 flash-message">
				<?php $messages = (array)$messages; ?>
				<?php foreach ($messages as $message): ?>
					<div class="alert alert-<?= $key ?>">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<?= $message ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
		<?= $content; ?>
	</main>
</div>
<footer class="footer">
	<div class="narrow">
		<div><img src="/styles/worker-logo-white.png" alt="Воркер" width="187" height="38"></div>
		<div class="contacts">©2013‒<?= date('Y'); ?> Воркер<br>
			<a href="mailto:<?= $supportEmail . '?subject=%D0%9E%D0%B1%D1%80%D0%B0%D1%89%D0%B5%D0%BD%D0%B8%D0%B5%20%D1%81%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0'; ?>" class="support-open">
				<span class="email"><?= $supportEmail; ?></span>
			</a>
		</div>
	</div>
</footer>
<div class="modals">
	<?php
	$isSiteController = Yii::$app->controller->id !== 'site';
	$actionId = Yii::$app->controller->action->id;
	?>
	<?php if ($isSiteController || $actionId !== 'download-worker') { ?>
	<div class="licence modal" id="licence-modal">
		<div class="narrow">
			<header class="modal-header">Лицензионное соглашение<a href="#licence" class="close-modal">×</a></header>
			<div class="modal-text">
				<?= \frontend\widgets\Download::widget(); ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if ($isSiteController || $actionId !== 'support') { ?>
	<div class="support modal" id="support">
		<div class="narrow">
			<header class="modal-header">Поддержка<a href="#support" class="close-modal">×</a></header>
			<div class="modal-text">
				<?= \frontend\widgets\Support::widget(); ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if (($isSiteController || $actionId !== 'order') && Yii::$app->controller->id != 'purchase') { ?>
	<div class="order-modal modal" id="order-modal">
		<div class="narrow">
			<header class="modal-header">Купить<a href="#order-modal" class="close-modal">×</a></header>
			<div class="modal-text">
				<?= \frontend\widgets\Order::widget(); ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<svg version="1.1" class="svg-container" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">
	<symbol id="worker-help-icon" viewBox="0 0 28.306 28.306">
		<path d="M12.486,8.162c0.598-2.139,0.055-4.53-1.623-6.213C9.189,0.283,6.824-0.263,4.699,0.314l3.602,3.603 L7.355,7.443L3.824,8.387l-3.604-3.6c-0.574,2.127-0.027,4.49,1.645,6.162c1.748,1.751,4.271,2.266,6.477,1.547l0.021,0.02 l14.818,14.818c0.586,0.585,1.355,0.881,2.123,0.881c0.77,0,1.535-0.296,2.123-0.881c1.172-1.17,1.172-3.069,0-4.246L12.486,8.162z M25.562,26.831c-0.631,0-1.141-0.513-1.141-1.147s0.51-1.146,1.141-1.146c0.637,0,1.152,0.512,1.152,1.146 S26.199,26.831,25.562,26.831z"/>
	</symbol>
	<symbol id="worker-download-icon" viewBox="0 0 29.978 29.978">
		<path d="M25.462,19.105v6.848H4.515v-6.848H0.489v8.861c0,1.111,0.9,2.012,2.016,2.012h24.967c1.115,0,2.016-0.9,2.016-2.012 v-8.861H25.462z"/>
		<path d="M14.62,18.426l-5.764-6.965c0,0-0.877-0.828,0.074-0.828s3.248,0,3.248,0s0-0.557,0-1.416c0-2.449,0-6.906,0-8.723 c0,0-0.129-0.494,0.615-0.494c0.75,0,4.035,0,4.572,0c0.536,0,0.524,0.416,0.524,0.416c0,1.762,0,6.373,0,8.742 c0,0.768,0,1.266,0,1.266s1.842,0,2.998,0c1.154,0,0.285,0.867,0.285,0.867s-4.904,6.51-5.588,7.193 C15.092,18.979,14.62,18.426,14.62,18.426z"/>
	</symbol>
	<symbol id="worker-order-icon" viewBox="0 0 12 12">
		<path d="M11.934,4.911C10.003,9,9.375,9,9,9H5.308C4.014,9,3.024,8.239,2.783,7.062C2.776,7.026,2.045,3.437,1.858,2.435 C1.766,1.943,1.532,1.615,0.692,1.615C0.31,1.615,0,1.306,0,0.923s0.31-0.692,0.692-0.692c1.394,0,2.291,0.692,2.526,1.95 C3.404,3.172,4.133,6.749,4.14,6.785c0.147,0.723,0.795,0.831,1.168,0.831h3.51c0.377-0.405,1.236-1.967,1.864-3.296 c0.163-0.346,0.576-0.493,0.922-0.33C11.949,4.152,12.097,4.565,11.934,4.911z M4.846,9.462c-0.637,0-1.154,0.516-1.154,1.154 c0,0.637,0.516,1.154,1.154,1.154S6,11.253,6,10.615C6,9.978,5.484,9.462,4.846,9.462z M9,9.462c-0.637,0-1.154,0.516-1.154,1.154 c0,0.637,0.516,1.154,1.154,1.154c0.637,0,1.154-0.516,1.154-1.154C10.154,9.978,9.637,9.462,9,9.462z"/>
		<path d="M7.84,3.01h1.03c0.09,0,0.18,0.05,0.22,0.14c0.05,0.08,0.04,0.18-0.01,0.26C8.39,4.43,7.13,5.66,7.09,5.7 C7.04,5.73,6.98,5.75,6.93,5.75c-0.06,0-0.12-0.02-0.16-0.05c-0.05-0.04-1.32-1.27-2-2.29C4.71,3.33,4.71,3.23,4.75,3.15 C4.8,3.06,4.89,3.01,4.98,3.01h1.04c0,0,0.09-1.98,0.15-2.07c0.1-0.14,0.5-0.23,0.76-0.23c0.25,0,0.65,0.09,0.74,0.23 C7.74,1.03,7.84,3.01,7.84,3.01z"/>
	</symbol>
</svg>
<?php
if (array_key_exists('yaCounterId', $params)) {
	$id = $params['yaCounterId'];

	$this->registerJs(<<<JS
		window.yaCounterName = 'yaCounter{$id}';
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter{$id} = new Ya.Metrika({
						id:{$id},
						accurateTrackBounce:true,
						params:window.yamParams||{}
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
JS
		,
		$this::POS_END,
		'yandex-metrika'
	);
}
?>
<?php $this->endBody(); ?>
</body>
</html>
<?php
$this->endPage();
Spaceless::end();
?>
