<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use frontend\assets\SiteAsset;
use frontend\widgets\Spaceless;

SiteAsset::register($this);

$params = Yii::$app->params;
$supportEmail = $params['supportEmail'];

Spaceless::begin();
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta property="og:locale" content="ru_RU"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php
$this->beginBody(); ?>
<div class="blank-content">
	<?= $content; ?>
</div>
<?php
if (array_key_exists('yaCounterId', $params)) {
	$id = $params['yaCounterId'];

	$this->registerJs(<<<JS
		window.yaCounterName = 'yaCounter{$id}';
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter{$id} = new Ya.Metrika({
						id:{$id},
						accurateTrackBounce:true,
						params:window.yamParams||{}
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
JS
		,
		$this::POS_END,
		'yandex-metrika'
	);
}
?>
<?php $this->endBody(); ?>
</body>
</html>
<?php
$this->endPage();
Spaceless::end();
?>
