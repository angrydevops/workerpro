<?php

use common\helpers\Html;
use common\helpers\Url;

/**
 * @var \common\models\Post $post
 * @var string $postUrl
 * @var boolean $contentOnly
 */

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'article',
]);

$postUrl = Url::to($postUrl, true);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => $postUrl,
]);
$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => $postUrl,
]);

$iconUrl = Url::imageDomain($post['icon']);
$postImage = $iconUrl ? $iconUrl . Url::thumbnail($post['icon'], 1920, 1080) : Url::to('@web/styles/worker-logo.png', true);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $postImage,
]);

$this->title = ($post['title_seo'] == '' ? $post['name'] : $post['title_seo']);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $post['name'],
]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $post['description'],
]);

$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $post['description'],
]);
?>
<?php if ($contentOnly) { ?>
    <!--nospaceless--><?= $post['content']; ?><!--/nospaceless-->
<?php } else { ?>
    <div class="article-page site-page">
        <div class="narrow">
            <div class="article-area" itemscope itemtype="http://schema.org/Article">
                <h1 itemprop="headline"><?= Html::encode($post['name']); ?></h1>
                <?= Html::tag('meta', '', [
                    'itemprop' => 'description',
                    'content' => $post['description'],
                ]); ?>
                <meta itemprop="image" content="<?= $postImage; ?>"/>
                <?php
                $thisMonth = date('m');
                $date = mktime(15, $thisMonth, $thisMonth, $thisMonth - 1, 16, date('Y'))
                ?>
                <meta itemprop="datePublished" content="<?= date('Y-m-d H:i:s', $date); ?>"/>
                <div class="article-text" itemprop="articleBody">
                    <!--nospaceless--><?= $post['content']; ?><!--/nospaceless-->
                </div>
            </div>
            <a href="<?= Url::toRoute(['site/support']); ?>" class="button question-ask support-open" rel="nofollow">Задать вопрос</a>
            <nav class="articles-tree">
                <?= \frontend\widgets\FullTree::widget([
                    'mainFolder' => $post['folderMain']['path'],
                    'activeId' => $post['id'],
                ]); ?>
            </nav>
        </div>
    </div>
<?php } ?>
