<?php


use common\helpers\Url;
use common\models\Post;

/**
 * @var integer $licencePostId
 * @var boolean $isModal
 */
?>
<div id="licence">
    <p>Пожалуйста, внимательно прочтите следующее лицензионное соглашение</p>
    <div class="licence-text">
        <?php if ($isModal) { ?>
            <iframe id="licence-text-frame"></iframe>
            <?php
            $post = Post::find()
                ->select(['id', 'folder_id', 'slug'])
                ->with(['folderMain' => function($query) {
                    /** @var \common\models\FolderQuery $query */
                    $query->select(['id', 'path']);
                }])
                ->where(['id' => $licencePostId])
                ->one();

            $route = Post::getUrlParams($post);
            $route['content-only'] = 1;
            $url = Url::toRoute($route);

            $this->registerJs(<<<JS
window.licenceTextFrameSrc = '{$url}';
JS
                , $this::POS_END, 'licence_text_frame_src');
            ?>
        <?php } else { ?>
            <?= Post::getContentById($licencePostId); ?>
        <?php } ?>
    </div>
    <div class="actions">
        <div>
            <input type="radio" value="1" name="licence" id="licence-agree"><label for="licence-agree">Принимаю условия</label>
            <input type="radio" value="2" name="licence" id="licence-disagree" checked><label for="licence-disagree">Отказываюсь от условий</label>
        </div>
        <p>Для продолжения примите соглашение и нажмите “Далее”</p>
        <div class="buttons">
            <a href="/download/" class="button button-submit" rel="nofollow">Далее</a>
            <?php if ($isModal) { ?>
                <a href="#licence" class="button close-modal">Отмена</a>
            <?php } ?>
        </div>
    </div>
</div>