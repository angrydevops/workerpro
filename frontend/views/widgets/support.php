<?php

use yii\widgets\ActiveForm;
use common\helpers\Html;
use common\helpers\Url;

/**
 * @var \frontend\models\SupportForm $model
 * @var boolean $mailSent
 * @var boolean $isModal
 * @var boolean $forceOpenModal
 */

$route = Yii::$app->getUrlManager()->parseRequest(Yii::$app->getRequest());
$action = is_array($route) ? array_merge([$route[0]], $route[1]) : Url::current();
?>
<div id="support-block">
    <?php if ($mailSent) { ?>
        <div class="message-sent" id="support-message-sent">
            <h3>Вы отправили сообщение в поддержку.</h3>
        </div>
    <?php } ?>
    <?php $form = ActiveForm::begin([
        'id' => 'support-form',
        'action' => $action,
        'options' => [
            'class'      => 'support-form',
            'novalidate' => 'novalidate',
        ],
    ]); ?>
    <?= $form->field($model, 'username', ['template' => '{input}'])->textInput([
        'placeholder' => $model->getAttributeLabel('username'),
    ]); ?>
    <?= $form->field($model, 'email', ['template' => '{input}'])->textInput([
        'placeholder' => $model->getAttributeLabel('email'),
        'type'        => 'email',
    ]); ?>
    <?= $form->field($model, 'type', ['template' => '{input}'])->dropDownList($model->typesLabels); ?>
    <?= $form->field($model, 'text', ['template' => '{input}'])->textarea([
        'rows'        => 6,
        'placeholder' => $model->getAttributeLabel('text'),
    ]); ?>
    <?= $form->errorSummary($model); ?>
    <div class="actions">
        <div class="buttons">
            <?= Html::submitButton('Отправить', ['class' => 'button button-submit']); ?>
            <?php if ($isModal) { ?>
              <a href="#support-form" class="button close-modal">Отмена</a>
            <?php } ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
if ($isModal && $forceOpenModal) {
    $this->registerJs(<<<JS
window.location.hash = '#support'
JS
        , $this::POS_END, 'add-support-modal-hash');
}
?>
