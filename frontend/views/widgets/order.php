<?php

use yii\widgets\ActiveForm;
use common\helpers\Html;
use common\helpers\Url;

/**
 * @var \common\models\Order $model
 * @var boolean $mailSent
 * @var boolean $isModal
 * @var boolean $forceOpenModal
 */

$route = Yii::$app->getUrlManager()->parseRequest(Yii::$app->getRequest());
$action = is_array($route) ? array_merge([$route[0]], $route[1]) : Url::current();
?>
<div id="order-block">
    <?php if ($mailSent) { ?>
        <div class="message-sent" id="order-message-sent">
            <h3>Ваша заявка принята, дождитесь ответного email.</h3>
        </div>
    <?php } ?>
    <div class="order-form-wrapper" id="order-form-wrapper">
        <p>Пожалуйста, введите данные</p>
        <?php $form = ActiveForm::begin([
            'id' => 'order-form',
            'action' => $action,
            'options' => [
                'class'      => 'order-form',
                'novalidate' => 'novalidate',
            ],
        ]); ?>
        <?= $form->field($model, 'organization', ['template' => '{label}{input}'])->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'username', ['template' => '{label}{input}'])->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'email', ['template' => '{label}{input}'])->textInput(['type' => 'email', 'maxlength' => true]); ?>
        <?= $form->field($model, 'computers_count', ['template' => '{label}{input}'])->textInput([
            'type'  => 'number',
            'min'   => 1,
            'value' => 1.
        ]); ?>
        <?= $form->field($model, 'licence_type')->dropDownList($model::$licenceTypes); ?>
        <?php
        if (empty($model->subscription_type)) {
            $model->subscription_type = Yii::$app->params['defaultCloudSubscription'];
        }
        ?>
        <?= $form->field($model, 'subscription_type')->dropDownList($model::$subscriptionTypes, ['prompt' => 'Без подписки']); ?>
        <?= $form->field($model, 'referal', ['template' => '{label}{input}'])->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'price')->textInput(['disabled' => 'disabled']); ?>
        <?= $form->errorSummary($model); ?>
        <div class="actions">
            <div class="buttons">
                <?= Html::submitButton('Купить', ['class' => 'button button-submit']); ?>
                <?php if ($isModal) { ?>
                    <a href="#order-form" class="button close-modal">Отмена</a>
                <?php } ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
if ($isModal && $forceOpenModal) {
    $this->registerJs(<<<JS
window.location.hash = '#order'
JS
, $this::POS_END, 'add-order-modal-hash');
}
?>
