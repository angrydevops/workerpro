<?php
namespace frontend\assets;

class FormSimpleAsset extends AppAssetBundle
{
	public $depends = [
		'frontend\assets\SiteAsset',
		'yii\widgets\ActiveFormAsset',
		'yii\validators\ValidationAsset',
		'yii\validators\PunycodeAsset',
	];
}
