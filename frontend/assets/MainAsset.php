<?php
namespace frontend\assets;

class MainAsset extends AppAssetBundle
{
	public $css = [
	];
	public $js = [
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];
}
