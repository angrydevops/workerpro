<?php

namespace frontend\assets;

class OrderAsset extends AppAssetBundle
{
	public $js = [
		'js/order.js',
	];
	public $depends = [
		'frontend\assets\MainAsset',
	];
}
