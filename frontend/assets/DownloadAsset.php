<?php

namespace frontend\assets;

class DownloadAsset extends AppAssetBundle
{
	public $js = [
		'js/download.js',
	];
	public $depends = [
		'frontend\assets\MainAsset',
	];
}
