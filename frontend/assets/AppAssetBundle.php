<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAssetBundle extends AssetBundle
{
	public $baseUrl = '';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->basePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'web';

		parent::init();
	}
}