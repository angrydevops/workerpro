<?php

namespace frontend\assets;

class SupportAsset extends AppAssetBundle
{
	public $js = [
		'js/support.js',
	];
	public $depends = [
		'frontend\assets\MainAsset',
	];
}
