<?php
namespace frontend\assets;

class SiteAsset extends AppAssetBundle
{
	public $css = [
		'styles/normalize.css',
		'styles/common.css',
		'styles/lightgallery.css',
	];
	public $js = [
		'js/site.js',
		'js/jquery.scroll.js',
		'js/owl.carousel.js',
		'js/lightgallery.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'frontend\assets\MainAsset',
	];
}
