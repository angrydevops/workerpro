<?php
/**
 * Configuration file for the "yii asset" console command.
 * Note that in the console environment, some path aliases like '@webroot' and '@web' may not exist.
 * Please define these missing path aliases.
 */
$basePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'web';
return [
	 'jsCompressor' => 'yii optimize/js  {from} {to}',
	'cssCompressor' => 'yii optimize/css {from} {to}',
	'bundles' => [
		'frontend\assets\SiteAsset',
		'frontend\assets\FormSimpleAsset',
		'frontend\assets\OrderAsset',
		'frontend\assets\SupportAsset',
		'frontend\assets\DownloadAsset',
	],
	'targets' => [
		'frontend\assets\MainCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/main-{hash}.js',
			'css' => 'assets/main-{hash}.css',
			'depends' => [
				'yii\web\JqueryAsset',
				'frontend\assets\MainAsset',
			],
		],
		'frontend\assets\SiteCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/site-{hash}.js',
			'css' => 'assets/site-{hash}.css',
			'depends' => [
				'yii\web\YiiAsset',
				'frontend\assets\SiteAsset',
			],
		],
		'frontend\assets\FormSimpleCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/form-simple-{hash}.js',
			'css' => 'assets/form-simple-{hash}.css',
			'depends' => [
				'yii\widgets\ActiveFormAsset',
				'yii\validators\ValidationAsset',
				'yii\validators\PunycodeAsset',
				'frontend\assets\FormSimpleAsset',
			],
		],
		'frontend\assets\OrderCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/order-{hash}.js',
			'css' => 'assets/order-{hash}.css',
			'depends' => [
				'frontend\assets\OrderAsset',
			],
		],
		'frontend\assets\SupportCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/support-{hash}.js',
			'css' => 'assets/support-{hash}.css',
			'depends' => [
				'frontend\assets\SupportAsset',
			],
		],
		'frontend\assets\DownloadCompressedAsset' => [
			'basePath' => $basePath,
			'baseUrl' => '',
			'js' => 'assets/download-{hash}.js',
			'css' => 'assets/download-{hash}.css',
			'depends' => [
				'frontend\assets\DownloadAsset',
			],
		],
	],
	'assetManager' => [
		'basePath' => $basePath . DIRECTORY_SEPARATOR . 'assets',
		'baseUrl' => '/assets',
		'linkAssets' => strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN',
	],
];