<?php
return [
	'urlMedia' => '/media',
	'mainTitle'           => 'Воркер – Контроль рабочего времени сотрудников',
	'mainOgTitle'         => 'Воркер – Контроль рабочего времени сотрудников',
	'mainMetaDescription' => 'Система учета рабочего времени офисных сотрудников, повышающая их продуктивность за счет соблюдения рабочего расписания, блокировки запуска развлекательных приложений и просмотра сайтов.',
	'mainOgDescription'   => 'Система учета рабочего времени офисных сотрудников, повышающая их продуктивность за счет соблюдения рабочего расписания, блокировки запуска развлекательных приложений и просмотра сайтов.',
	'mainKeywords'        => 'учет рабочего времени, эффективность персонала, проверка сотрудников, блокировка программ и сайтов, программа слежения, наблюдение за сотрудниками, продуктивность сотрудников, следить за компьютером, контроль работников.',
	'licencePostId' => 34,
	'yaCounterId' => '33574169',
	'wmiMerchantId' => '152839970279',
	'walletoneSecret' => '59316f456f6245413850506434335d44625743677576724e514e43',
	/*'programPrices' => [
		'cloud' => [
			'countLabels' => [
				'менее 6',
				'от 6 до 20',
				'более 20',
			],
			'subscribe' => [
				'threeMonths' => [
					'count' => 3,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 290],
						['min' => 6,  'max' => 20,   'price' => 280],
						['min' => 21, 'max' => null, 'price' => 270],
					],
					'pricesContinue' => [260, 250, 240],
				],
				'sixMonths' => [
					'count' => 6,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 280],
						['min' => 6,  'max' => 20,   'price' => 270],
						['min' => 21, 'max' => null, 'price' => 260],
					],
					'pricesContinue' => [250, 240, 230],
				],
				'year' => [
					'count' => 12,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 260],
						['min' => 6,  'max' => 20,   'price' => 250],
						['min' => 21, 'max' => null, 'price' => 240],
					],
					'pricesContinue' => [230, 220, 210],
				],
			],
		],
		'local' => [
			'prices' => [
				['min' => 20,   'max' => 49,   'price' => 2500, 'countLabel' => 'от 20 до 49', 'discountLabel' => 'Без скидки'],
				['min' => 50,   'max' => 99,   'price' => 2000, 'countLabel' => 'от 50 до 99', 'discountLabel' => '20%'],
				['min' => 100,  'max' => null, 'price' => 1750, 'countLabel' => 'от 100…',    'discountLabel' => '30%']
			],
		],
	],*/
	'programPrices' => [
		'cloud' => [
			'countLabels' => [
				'менее 6',
				'от 6 до 20',
				'более 20',
			],
			'subscribe' => [
				'threeMonths' => [
					'count' => 3,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 1],
						['min' => 6,  'max' => 20,   'price' => 1],
						['min' => 21, 'max' => null, 'price' => 1],
					],
					'pricesContinue' => [260, 250, 240],
				],
				'sixMonths' => [
					'count' => 6,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 1],
						['min' => 6,  'max' => 20,   'price' => 1],
						['min' => 21, 'max' => null, 'price' => 1],
					],
					'pricesContinue' => [250, 240, 230],
				],
				'year' => [
					'count' => 12,
					'prices' => [
						['min' => 1,  'max' => 5,    'price' => 1],
						['min' => 6,  'max' => 20,   'price' => 1],
						['min' => 21, 'max' => null, 'price' => 1],
					],
					'pricesContinue' => [230, 220, 210],
				],
			],
		],
		'local' => [
			'prices' => [
				['min' => 20,   'max' => 49,   'price' => 1, 'countLabel' => 'от 20 до 49', 'discountLabel' => 'Без скидки'],
				['min' => 50,   'max' => 99,   'price' => 1, 'countLabel' => 'от 50 до 99', 'discountLabel' => '20%'],
				['min' => 100,  'max' => null, 'price' => 1, 'countLabel' => 'от 100…',    'discountLabel' => '30%']
			],
		],
	],
	'defaultCloudSubscription' => 'year',
	'hashSalt' => 'f13salt',
];
