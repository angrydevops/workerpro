<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);
$isWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';

$assetsConfigDir = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR;
$assetManagerCompressedFile = $assetsConfigDir . 'assets_compressed.php';

$assets = is_file($assetManagerCompressedFile) ? require($assetManagerCompressedFile) : [];

return [
	'id' => 'worker-frontend',
	'name' => 'Site Worker.Pro',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'frontend\controllers',
	'defaultRoute' => 'post',
	'bootstrap' => ['log'],
	'components' => [
		'urlManager' => [
			'class' => 'frontend\components\FrontendUrlManager',
			'enableStrictParsing' => !defined('YII_DEBUG'),
		],
		'log' => [
			'targets' => [
				'notFound' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/404.log',
					'rotateByCopy' => $isWin,
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:404'],
					'logVars' => [],
				],
				'notificationsSupport' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/notifications-support.log',
					'rotateByCopy' => $isWin,
					'levels' => ['info'],
					'categories' => ['notifications/support'],
					'logVars' => [],
				],
				'purchaseLinkError' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/purchase-link-error.log',
					'rotateByCopy' => $isWin,
					'levels' => ['warning'],
					'categories' => ['purchase-fail/no-order'],
					'logVars' => ['_GET', '_SERVER'],
				],
				'purchaseError' => [
					'class' => 'yii\log\FileTarget',
					'logFile' => '@runtime/logs/purchase-error.log',
					'rotateByCopy' => $isWin,
					'levels' => ['warning'],
					'categories' => ['purchase-fail/confirmation', 'purchase-fail/payment'],
					'logVars' => ['_POST', '_SERVER'],
				],
				'emailPurchaseLinkError' => [
					'class' => 'common\log\EmailTarget',
					'schemaCache' => 'cacheCommon',
					'levels' => ['warning'],
					'categories' => ['purchase-fail/no-order'],
					'logVars' => [],
					'message' => [
						'to' => ['bug@worker.pro'],
						'subject' => 'Worker: неверные ссылки на оплату',
					],
				],
				'emailPurchaseError' => [
					'class' => 'common\log\EmailTarget',
					'schemaCache' => 'cacheCommon',
					'levels' => ['warning'],
					'categories' => ['purchase-fail/confirmation', 'purchase-fail/payment'],
					'logVars' => [],
					'message' => [
						'to' => ['bug@worker.pro'],
						'subject' => 'Worker: ошибки при оплате',
					],
				],
				'errorCommon' => [
					'except' => [
						'yii\web\HttpException:404',
						'notifications/support',
						'purchase-fail/no-order',
						'purchase-fail/confirmation',
						'purchase-fail/payment'
					],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'session' => [
			'class' => 'common\components\Session',
			'name' => 'WORKER',
			'useTransparentSessionID' => false,
			'useCookies' => true,
			'cookieParams' => [
				'lifetime' => 31536000, // 60*60*24*365
				'path' => '/',
				'domain' => $_SERVER['HTTP_HOST'],
				'secure' => false,
			],
			'gCProbability' => 0,
			'savePath' => '@runtime/session',
		],
		'user' => [
			'class' => \common\components\User::className(),
			'identityClass' => 'common\models\User',
		],
		'assetManager' => [
			'linkAssets' => strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN',
			'bundles' => $assets,
		],
	],
	'params' => $params,
];
