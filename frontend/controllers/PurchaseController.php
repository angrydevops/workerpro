<?php

namespace frontend\controllers;

use \Yii;
use \yii\filters\AccessControl;
use \yii\helpers\Json;
use \yii\base\InvalidParamException;
use \yii\web\Controller;
use \yii\web\ForbiddenHttpException;
use \yii\web\NotFoundHttpException;
use \common\models\Order;
use \common\models\Purchase;
use \frontend\components\Walletone;
use common\helpers\Url;

/**
 * Purchase controller
 */
class PurchaseController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['confirmation', 'success', 'fail'],
						'verbs' => ['POST'],
					],
					[
						'allow' => true,
						'actions' => ['index'],
					],
				],
				'denyCallback' => function($rule, $action) {
					throw new NotFoundHttpException('Страница не найдена');
				},
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		foreach ($_POST as &$item) {
			if(is_string($item)) {
				$item = iconv('windows-1251', 'utf-8', urldecode($item));
			}
		}

		return parent::beforeAction($action);
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Displays purchase page.
	 *
	 * @return mixed
	 * @throws InvalidParamException
	 */
	public function actionIndex()
	{
		$purchase = $orderParam = $clientParam = $hashParam = null;
		$get = Yii::$app->getRequest()->get();

		if (array_key_exists('order', $get) && !empty($get['order'])) {
			$orderParam = $get['order'];
			$order = Order::find()->byGuid($orderParam)->createdInLastDay(3)->asArray()->one();

			if ($order) {
				$purchase = Purchase::findOne(['order_id' => $order['id']]);
				if (!$purchase) {
					$purchase = new Purchase();

					$purchase->order_id = $order['id'];
					foreach ($purchase::$attributesFromOrder as $attribute) {
						$purchase->{$attribute} = $order[$attribute];
					}
				}

				$summary = Json::decode($purchase->summary);
				$summary[time()] = 'opened';
				$purchase->summary = Json::encode($summary);

				$purchase->save();
			}
			if ($purchase === null) {
				Yii::warning('Заказ для оплаты не найден', 'purchase-fail/no-order');
			}

			return $this->render('order', [
				'purchase'    => $purchase,
				'orderParam'  => $orderParam,
				'clientParam' => $clientParam,
			]);
		} else if (array_key_exists('client', $get) && array_key_exists('hash', $get) && !empty($get['client']) && !empty($get['hash'])) {
			$clientParam = $get['client'];
			$hashParam = $get['hash'];
			$hashSalt = Yii::$app->params['hashSalt'];
			$isValid = false;
			$client = '';
			foreach ([time(), strtotime('yesterday'), strtotime('tomorrow')] as $date) {
				if (mb_strtoupper(md5($hashSalt . date('Y-m-d', $date) . $clientParam)) === $hashParam) {
					$isValid = true;
					break;
				}
			}
			$model = new \common\models\Order();

			$licenses = [];
			if($isValid) {
				$licenses = Yii::$app->license->getAll($clientParam);
				$client = ['name' => $licenses[0]['client_name'], 'email' => /*$licenses[0]['email'].*/'i@yar.pw'];
				$model->email = $client['email'];
				$model->username = $client['name'];
				$model->organization = $client['name'];
				if ($model->load(Yii::$app->request->post()) && $model->validate()) {
					$model->calculatePrice();
					if ($model->save()) {
						$model->refresh();
						if ($model->user_type == 'ur') {



							$mail = Yii::$app->getMailer()
								->compose(
									[
										'html' => 'order-html',
										'text' => 'order-text',
									],
									[
										'model' => $model,
									]
								)
								->setTo(Yii::$app->params['orderEmail'])
								->setFrom([$model->email => $model->username])
								->setSubject('Запрос продления подписки')
								->send();

							if ($mail) {
								Yii::$app->session->setFlash('order-success', 'Ваша заявка принята, дождитесь ответного email.');
								return $this->refresh();
							}
						} else {
							return $this->redirect(['index','order' => $model->guid]);
						}
					}
				} else {
					$model->licence_type = $licenses[0]['is_cloud'] ? 'cloud' : 'local';
					$model->user_type = $licenses[0]['is_jur'] ? 'ur' : 'fiz';
					$model->subscription_type = $licenses[0]['subscription_type'];
					$model->computers_count = $licenses[0]['cnt'] ? $licenses[0]['cnt'] : 1;
				}
			}
			return $this->render('client', [
				'isValid'=>$isValid,
				'model'=>$model,
				'licenses'=>$licenses,
				'client'    => $client,
				'clientParam' => $clientParam,
			]);
		}
	}

	/**
	 * Displays purchase confirm page.
	 *
	 * @return mixed
	 * @throws InvalidParamException
	 */
	public function actionConfirmation()
	{
		$result = $this->checkAndUpdatePurchase('confirmation');

		if ($result['result'] === 'retry') {
			Yii::warning($result['answer'], 'purchase-fail/confirmation');
		}

		return $this->confirmationAnswer($result['result'], $result['answer']);
	}

	/**
	 * Displays purchase success page.
	 *
	 * @return mixed
	 * @throws InvalidParamException
	 * @throws ForbiddenHttpException
	 */
	public function actionSuccess()
	{
		$result = $this->checkAndUpdatePurchase();
		$purchase = array_key_exists('purchase', $result) ? $result['purchase'] : null;

		if ($result['result'] !== 'ok') {
			Yii::warning($result['answer'], 'purchase-fail/payment');

			return $this->render('fail', [
				'purchase' => $purchase,
			]);
		}

		Yii::$app->getMailer()
			->compose(
				[
					'html' => 'purchase-html',
					'text' => 'purchase-text',
				],
				[
					'model' => $purchase,
				]
			)
			->setTo(Yii::$app->params['orderEmail'])
			->setFrom([$purchase->email => $purchase->username])
			->setSubject('Оплата Worker')
			->send();

		return $this->render('success', [
			'purchase' => $purchase,
		]);
	}

	/**
	 * Displays purchase fail page.
	 *
	 * @return mixed
	 * @throws InvalidParamException
	 * @throws ForbiddenHttpException
	 */
	public function actionFail()
	{
		$result = $this->checkAndUpdatePurchase();

		Yii::warning($result['answer'], 'purchase-fail/payment');

		return $this->render('fail', [
			'purchase' => array_key_exists('purchase', $result) ? $result['purchase'] : null,
		]);
	}

	/**
	 * @param string $result
	 * @param string $description
	 * @return string
	 */
	protected function confirmationAnswer($result, $description = '')
	{
		$result = 'WMI_RESULT=' . strtoupper($result);

		if (!empty($description)) {
			$description = '&' . 'WMI_DESCRIPTION=' . $description;
		}

		return $result . $description;
	}

	/**
	 * @param string $statusPrefix
	 * @return array
	 */
	protected function checkAndUpdatePurchase($statusPrefix = 'pay') {
		$request = Yii::$app->getRequest();

		if (!$request->post('WMI_PAYMENT_NO')) {
			return ['answer' => 'Отсутствует параметр WMI_PAYMENT_NO', 'result' => 'retry'];
		}

		$purchase = Purchase::find()->where(['guid' => $request->post('WMI_PAYMENT_NO')])->one();

		if (!$purchase) {
			return ['answer' => 'Заказ не найден', 'result' => 'retry'];
		}

		$now = time();
		$summary = Json::decode($purchase->summary);
		$summary[$now] = $statusPrefix . '-fail';

		if (!$request->post('WMI_ORDER_STATE')) {
			$answer = ['answer' => 'Отсутствует параметр WMI_ORDER_STATE', 'result' => 'retry'];
		} else if (!$request->post('WMI_SIGNATURE')) {
			$answer = ['answer' => 'Отсутствует параметр WMI_SIGNATURE', 'result' => 'retry'];
		} else {
			if (Walletone::getSignatureFromRequest() === $request->post('WMI_SIGNATURE')) {
				if (strtoupper($request->post('WMI_ORDER_STATE')) === 'ACCEPTED') {
					$summary[$now] = $statusPrefix . '-success';
					if ($request->post('WMI_ORDER_ID')) {
						$purchase->wmi_order_id = $request->post('WMI_ORDER_ID');
					}

					if ($request->post('WMI_TO_USER_ID')) {
						$purchase->wmi_to_user_id = $request->post('WMI_TO_USER_ID');
					}

					$answer = ['answer' => 'Заказ ' . $request->post('WMI_PAYMENT_NO') . ' оплачен', 'result' => 'ok'];
				} else {
					$answer = ['answer' => 'Неверное состояние ' . $request->post('WMI_ORDER_STATE'), 'result' => 'retry'];
				}
			} else {
				$answer = ['answer' => 'Неверная подпись ' . $request->post('WMI_SIGNATURE'), 'result' => 'retry'];
			}
		}

		$purchase->summary = Json::encode($summary);
		$purchase->save();
		$answer['purchase'] = $purchase;

		return $answer;
	}
}
