<?php
namespace frontend\controllers;

use yii\web\Controller;
use common\models\Post;
use common\helpers\StringHelper;
use common\models\Folder;

class PostController extends Controller
{
	/**
	 * @param string $folder_path
	 * @param string $post_slug
	 * @param string $post_id
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
	public function actionView($folder_path, $post_slug, $post_id)
	{
		$post = Post::find()->forView()->andWhere(['id' => $post_id])->asArray()->one();
		if ($post === null) {
			throw new \yii\web\NotFoundHttpException('Запрошенный материал не найден.');
		}

		$postUrl = \common\helpers\Url::toRoute(Post::getUrlParams($post));
		if ($postUrl != parse_url($_SERVER['REQUEST_URI'])['path']) {
			$this->redirect($postUrl, 301);
		}

		return $this->renderPost($post, $postUrl);
	}

	/**
	 * @param string $slug
	 * @throws \yii\web\NotFoundHttpException
	 * @return mixed
	 */
	public function actionViewBySlug($slug)
	{
		$postsUrlReplace = \Yii::$app->params['postsUrlReplace'];
		array_filter($postsUrlReplace, function($route) {
			return $route[0] === 'post/view-by-slug';
		});

		$post = Post::find()->forView()->andWhere(['id' => array_keys($postsUrlReplace)])->andWhere(['slug' => $slug])->asArray()->one();
		if ($post === null) {
			throw new \yii\web\NotFoundHttpException('Запрошенный материал не найден.');
		}

		return $this->renderPost($post, \common\helpers\Url::toRoute(Post::getUrlParams($post)));
	}

	/**
	 * @param string $folder_path
	 * @return mixed
	 * @throws \yii\web\NotFoundHttpException
	 */
	public function actionFolder($folder_path)
	{
		$folder_path = StringHelper::mb_str_replace('/', '.', $folder_path);

		$folderIds = Folder::find()->select('id')->withChildren($folder_path)->orderTree()->column();

		if (empty($folderIds)) {
			throw new \yii\web\NotFoundHttpException('Запрошенная директория не найдена.');
		}

		$post = Post::find()->forFolderView($folderIds)->asArray()->one();

		if ($post === null) {
			throw new \yii\web\NotFoundHttpException('Материал в запрошенной директории не найден.');
		}

		$postUrl = \common\helpers\Url::toRoute(Post::getUrlParams($post));

		return $this->renderPost($post, $postUrl);
	}


	/**
	 * @param array $post
	 * @param string $postUrl
	 * @return mixed
	 */
	protected function renderPost($post, $postUrl)
	{
		$contentOnly = false;

		if (\Yii::$app->getRequest()->get('content-only', 0) == 1) {
			$this->layout = 'blank';
			$contentOnly = true;
		}

		return $this->render('view', [
			'post' => $post,
			'postUrl' => $postUrl,
			'contentOnly' => $contentOnly,
		]);
	}
}
