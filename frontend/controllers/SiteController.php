<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}

	/**
	 * Displays download page.
	 *
	 * @return mixed
	 */
	public function actionDownloadWorker() {
		return $this->render('download');
	}

	/**
	 * Displays support page.
	 *
	 * @return mixed
	 */
	public function actionSupport()
	{
		return $this->render('support');
	}

	/**
	 * Displays order page.
	 *
	 * @return mixed
	 */
	public function actionOrder() {
		return $this->render('order');
	}
}
