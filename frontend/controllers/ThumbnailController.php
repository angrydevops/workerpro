<?php
namespace frontend\controllers;

use Yii;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Imagine\Image\Box;
use Imagine\Image\Point;

class ThumbnailController extends Controller
{
	/**
	 * @throws NotFoundHttpException image doesn't exists
	 * @return mixed
	 */
	public function actionView()
	{
		$extension = $_GET['extension'];
		$name = $_GET['name'];
		$bigImageSubPath = DIRECTORY_SEPARATOR . (array_key_exists('type', $_GET) ? $_GET['type'] : '') . DIRECTORY_SEPARATOR . $name . '.' . $extension;
		$bigImage = Yii::getAlias('@media') . $bigImageSubPath;
		if (file_exists($bigImage)) {
			$thumbnailFile = Yii::getAlias('@webroot') . $_SERVER['REQUEST_URI'];
			if (file_exists($thumbnailFile)) {
				Image::getImagine()->open($thumbnailFile)->show($extension);
			} else {
				$thumbnailDir = dirname($thumbnailFile);
				if (!is_dir($thumbnailDir)) {
					mkdir($thumbnailDir, 0775);
				}

				$newThumbnail = Image::getImagine()->open($bigImage);
				$imgHeight = $newThumbnail->getSize()->getHeight();
				$imgWidth  = $newThumbnail->getSize()->getWidth();

				$size = $_GET['size'];
				$separatorPlace = strrpos($size, 'x');
				$thumbnailWidth = substr($size, 0, $separatorPlace);
				$thumbnailHeight = substr($size, $separatorPlace + 1);

				// если запрошенная высота на 10% меньше той, которая получилась бы при сохранении пропорции
				$imgHeightIfProportional = $thumbnailHeight * $imgWidth / $thumbnailWidth;
				if (1.1 * $imgHeightIfProportional < $imgHeight && strpos($name, '-noshift') === false) {
					// то отрезаем снизу половину излишка
					$newThumbnail->crop(new Point(0, 0), new Box($imgWidth, $imgHeight - ceil(($imgHeight - $imgHeightIfProportional) / 2)));
				}
				$newThumbnail->thumbnail(new Box($thumbnailWidth, $thumbnailHeight), 'outbound')->save($thumbnailFile)->show($extension);
			}
		} else {
			throw new NotFoundHttpException('The requested file does not exist.');
		}
	}
}
