#!/bin/sh
cd /var/www/worker/
export PATH=$PATH:/var/www/worker/

yii asset ./frontend/config/assets.php ./frontend/runtime/assets_compressed.php./
for file in `find ./frontend/web/assets/ -type f -name "*.js"`; do
  gzip -9 -c "$file" > "$file.gz";
done
for file in `find ./frontend/web/assets/ -type f -name "*.css"`; do
  gzip -9 -c "$file" > "$file.gz";
done
for file in `find ./frontend/web/ -type f -name "*.txt"`; do
  gzip -9 -c "$file" > "$file.gz";
done
for file in `find ./frontend/web/ -type f -name "*.xml"`; do
  gzip -9 -c "$file" > "$file.gz";
done
for file in `find ./frontend/web/ -type f -name "*.html"`; do
  gzip -9 -c "$file" > "$file.gz";
done
for file in `find ./frontend/web/ -type f -name "*.ico"`; do
  gzip -9 -c "$file" > "$file.gz";
done
