<?php
return [
    'components' => [
        'cacheCommon' => [
            'class' => '',
        ],
        'cache' => [
            'class' => '',
        ],
        'db' => [
            'dsn' => 'pgsql:host=localhost;dbname=worker',
            'username' => 'worker',
            'password' => '',
        ],
    ],
];
