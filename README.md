Worker Web Application
======================

Установка
---------

- `composer selfupdate`
- `composer global require "fxp/composer-asset-plugin:~1.1.1"`
- `composer update`
- `php requirements.php`
- `./init`
- директории сессий
    - `./console/migrations/mod_files ./frontend/runtime/session 2 5`
    - `chown -R apache:apache ./frontend/runtime/session`
    - `chmod -R 750 ./frontend/runtime/session`
- `./common/config/main-local.php`
- `./yii db/execute-sql --db=false "CREATE ROLE worker ENCRYPTED PASSWORD 'md5' || md5('password' || 'worker') LOGIN INHERIT"`
- `./yii db/execute-sql --db=false "CREATE DATABASE worker OWNER worker ENCODING 'UTF-8'"`
- `./common/media/` добавить в бекап

В случае Windows замените слеши на обратные.

Консольные команды
------------------

- `backup.sh`
- `yum update`           обновление бинарных пакетов
- `vendors_update.sh`    обновление веб-библиотек
- `deploy.sh`            обновление кода сайта
- `static_compressed.sh` пережимка ассетов и предварительно сжатых
- `yii`                  консольные методы приложения
